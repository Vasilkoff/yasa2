precision mediump float;

varying mediump vec2 textureCoordinate;

uniform sampler2D inputImageTexture;
uniform sampler2D inputImageTexture2;  //process
uniform sampler2D inputImageTexture3;  //blowout
uniform sampler2D inputImageTexture4;  //contrast
 
 
uniform float strength;
uniform float effect2;

varying mediump vec2 blurTexCoords[14];
vec4 doBlurNew(vec2 texCoord)
{
    vec4 blur = vec4(0.0);
    blur += texture2D(inputImageTexture, blurTexCoords[ 0])*0.0044299121055113265;
    blur += texture2D(inputImageTexture, blurTexCoords[ 1])*0.00895781211794;
    blur += texture2D(inputImageTexture, blurTexCoords[ 2])*0.0215963866053;
    blur += texture2D(inputImageTexture, blurTexCoords[ 3])*0.0443683338718;
    blur += texture2D(inputImageTexture, blurTexCoords[ 4])*0.0776744219933;
    blur += texture2D(inputImageTexture, blurTexCoords[ 5])*0.115876621105;
    blur += texture2D(inputImageTexture, blurTexCoords[ 6])*0.147308056121;
    blur += texture2D(inputImageTexture, texCoord         )*0.159576912161;
    blur += texture2D(inputImageTexture, blurTexCoords[ 7])*0.147308056121;
    blur += texture2D(inputImageTexture, blurTexCoords[ 8])*0.115876621105;
    blur += texture2D(inputImageTexture, blurTexCoords[ 9])*0.0776744219933;
    blur += texture2D(inputImageTexture, blurTexCoords[10])*0.0443683338718;
    blur += texture2D(inputImageTexture, blurTexCoords[11])*0.0215963866053;
    blur += texture2D(inputImageTexture, blurTexCoords[12])*0.00895781211794;
    blur += texture2D(inputImageTexture, blurTexCoords[13])*0.0044299121055113265;

	return blur;
}

void main()
{
  vec2 tempCoordinate = textureCoordinate;
  //vec2 shapePos = vec2(1.4*tempCoordinate.x, 1.2*tempCoordinate.y-0.1);
  vec2 shapePos = vec2(1.64*tempCoordinate.x-0.34, 1.2*tempCoordinate.y-0.1);

  vec4 originColor = texture2D(inputImageTexture, tempCoordinate);
  vec4 originColor2 = texture2D(inputImageTexture, vec2(1.2*tempCoordinate.x, 1.2*tempCoordinate.y) );
  vec4 vMask = texture2D(inputImageTexture3, shapePos );
  vec4 butterfly = texture2D(inputImageTexture4, shapePos );
  gl_FragColor = mix( doBlurNew(tempCoordinate), originColor2*butterfly, vMask.r );
  /*if (vMask.r > 0.5)
	  gl_FragColor = originColor2*butterfly;
  else
	  gl_FragColor = doBlurNew(tempCoordinate);//originColor;*/
  //gl_FragColor = originColor;

}