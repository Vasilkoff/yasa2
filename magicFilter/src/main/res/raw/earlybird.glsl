 precision mediump float;

 varying mediump vec2 textureCoordinate;

 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2; //earlyBirdCurves
 uniform sampler2D inputImageTexture3; //earlyBirdOverlay
 uniform sampler2D inputImageTexture4; //vig
 uniform sampler2D inputImageTexture5; //earlyBirdBlowout
 uniform sampler2D inputImageTexture6; //earlyBirdMap

 const mat3 saturate = mat3(
                            1.210300,
                            -0.089700,
                            -0.091000,
                            -0.176100,
                            1.123900,
                            -0.177400,
                            -0.034200,
                            -0.034200,
                            1.265800);
 const vec3 rgbPrime = vec3(0.25098, 0.14640522, 0.0);
 const vec3 desaturate = vec3(.3, .59, .11);

 uniform float effect2;

  vec2 getMirrorEffect_() {
     float mirrorType = effect2;
     vec2 newCoordinate = vec2(textureCoordinate.x, textureCoordinate.y);
     vec2 res = vec2(textureCoordinate.x, textureCoordinate.y);

     if(mirrorType==3.5) {
         if (newCoordinate.x > 0.5) {
             res = vec2(1.25 - newCoordinate.x, newCoordinate.y); }
         else {
             res = vec2(0.25 + newCoordinate.x, newCoordinate.y); }
     }

     else if(mirrorType==2.5) {
         if (newCoordinate.y > 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x),  1.5*(1.0 - newCoordinate.y) );}
         if (newCoordinate.y > 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*(1.0 - newCoordinate.y) );}
         if (newCoordinate.y < 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x), 1.5*newCoordinate.y );}
         if (newCoordinate.y < 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*newCoordinate.y );}
     }

     else if(mirrorType==1.5) {
         if (newCoordinate.y > 0.5) {
             res = vec2(newCoordinate.x, newCoordinate.y - 0.25); }
         else {
             res = vec2(newCoordinate.x, 0.75 - newCoordinate.y); }
     }

     else if(mirrorType==0.5) {
         if (newCoordinate.y > 0.5) {
             res = vec2(newCoordinate.x, 1.25 - newCoordinate.y); }
         else {
             res = vec2(newCoordinate.x, 0.25 + newCoordinate.y); }
     }

  	return res;
  }


 void main()
 {

     vec2 tempCoordinate = getMirrorEffect_();
     vec3 texel = texture2D(inputImageTexture, tempCoordinate).rgb;


     vec2 lookup;
     lookup.y = 0.5;

     lookup.x = texel.r;
     texel.r = texture2D(inputImageTexture2, lookup).r;

     lookup.x = texel.g;
     texel.g = texture2D(inputImageTexture2, lookup).g;

     lookup.x = texel.b;
     texel.b = texture2D(inputImageTexture2, lookup).b;

     float desaturatedColor;
     vec3 result;
     desaturatedColor = dot(desaturate, texel);


     lookup.x = desaturatedColor;
     result.r = texture2D(inputImageTexture3, lookup).r;
     lookup.x = desaturatedColor;
     result.g = texture2D(inputImageTexture3, lookup).g;
     lookup.x = desaturatedColor;
     result.b = texture2D(inputImageTexture3, lookup).b;

     texel = saturate * mix(texel, result, .5);

     vec2 tc = (2.0 * tempCoordinate) - 1.0;
     float d = dot(tc, tc);

     vec3 sampled;
     lookup.y = .5;

     /*
      lookup.x = texel.r;
      sampled.r = texture2D(inputImageTexture4, lookup).r;

      lookup.x = texel.g;
      sampled.g = texture2D(inputImageTexture4, lookup).g;

      lookup.x = texel.b;
      sampled.b = texture2D(inputImageTexture4, lookup).b;

      float value = smoothstep(0.0, 1.25, pow(d, 1.35)/1.65);
      texel = mix(texel, sampled, value);
      */

     //---

     lookup = vec2(d, texel.r);
     texel.r = texture2D(inputImageTexture4, lookup).r;
     lookup.y = texel.g;
     texel.g = texture2D(inputImageTexture4, lookup).g;
     lookup.y = texel.b;
     texel.b	= texture2D(inputImageTexture4, lookup).b;
     float value = smoothstep(0.0, 1.25, pow(d, 1.35)/1.65);

     //---

     lookup.x = texel.r;
     sampled.r = texture2D(inputImageTexture5, lookup).r;
     lookup.x = texel.g;
     sampled.g = texture2D(inputImageTexture5, lookup).g;
     lookup.x = texel.b;
     sampled.b = texture2D(inputImageTexture5, lookup).b;
     texel = mix(sampled, texel, value);


     lookup.x = texel.r;
     texel.r = texture2D(inputImageTexture6, lookup).r;
     lookup.x = texel.g;
     texel.g = texture2D(inputImageTexture6, lookup).g;
     lookup.x = texel.b;
     texel.b = texture2D(inputImageTexture6, lookup).b;

     gl_FragColor = vec4(texel, 1.0);
 }
