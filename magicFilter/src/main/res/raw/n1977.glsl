precision mediump float;

 varying mediump vec2 textureCoordinate;

 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 uniform float effect2;

 vec2 getMirrorEffect_() {
 	float mirrorType = effect2;
 	vec2 newCoordinate = vec2(textureCoordinate.x, textureCoordinate.y);
 	vec2 res = vec2(textureCoordinate.x, textureCoordinate.y);

 	if(mirrorType==3.5) {
 		if (newCoordinate.x > 0.5) {
 			res = vec2(1.25 - newCoordinate.x, newCoordinate.y); }
 		else {
 			res = vec2(0.25 + newCoordinate.x, newCoordinate.y); }
 	}

 	else if(mirrorType==2.5) {
 		if (newCoordinate.y > 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x),  1.5*(1.0 - newCoordinate.y) );}
 		if (newCoordinate.y > 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*(1.0 - newCoordinate.y) );}
 		if (newCoordinate.y < 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x), 1.5*newCoordinate.y );}
 		if (newCoordinate.y < 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*newCoordinate.y );}
 	}

 	else if(mirrorType==1.5) {
 		if (newCoordinate.y > 0.5) {
 			res = vec2(newCoordinate.x, newCoordinate.y - 0.25); }
 		else {
 			res = vec2(newCoordinate.x, 0.75 - newCoordinate.y); }
 	}

 	else if(mirrorType==0.5) {
 		if (newCoordinate.y > 0.5) {
 			res = vec2(newCoordinate.x, 1.25 - newCoordinate.y); }
 		else {
 			res = vec2(newCoordinate.x, 0.25 + newCoordinate.y); }
 	}

 	return res;
 }


 void main()
 {

     vec2 tempCoordinate = getMirrorEffect_();
     vec3 texel = texture2D(inputImageTexture, tempCoordinate).rgb;

     texel = vec3(
                  texture2D(inputImageTexture2, vec2(texel.r, .16666)).r,
                  texture2D(inputImageTexture2, vec2(texel.g, .5)).g,
                  texture2D(inputImageTexture2, vec2(texel.b, .83333)).b);

     gl_FragColor = vec4(texel, 1.0);
 }
