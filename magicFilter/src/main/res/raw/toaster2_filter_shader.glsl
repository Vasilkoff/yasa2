precision mediump float;

varying mediump vec2 textureCoordinate;

uniform sampler2D inputImageTexture;
uniform sampler2D inputImageTexture2; //toaster_metal
uniform sampler2D inputImageTexture3; //toaster_soft_light
uniform sampler2D inputImageTexture4; //toaster_curves
uniform sampler2D inputImageTexture5; //toaster_overlay_map_warm
uniform sampler2D inputImageTexture6; //toaster_color_shift
uniform float effect2;

vec2 getMirrorEffect_() {
	float mirrorType = effect2;
	vec2 newCoordinate = vec2(textureCoordinate.x, textureCoordinate.y);
	vec2 res = vec2(textureCoordinate.x, textureCoordinate.y);

	if(mirrorType==3.5) {
		if (newCoordinate.x > 0.5) {
			res = vec2(1.25 - newCoordinate.x, newCoordinate.y); }
		else {
			res = vec2(0.25 + newCoordinate.x, newCoordinate.y); }
	}

	else if(mirrorType==2.5) {
		if (newCoordinate.y > 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x),  1.5*(1.0 - newCoordinate.y) );}
		if (newCoordinate.y > 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*(1.0 - newCoordinate.y) );}
		if (newCoordinate.y < 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x), 1.5*newCoordinate.y );}
		if (newCoordinate.y < 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*newCoordinate.y );}
	}

	else if(mirrorType==1.5) {
		if (newCoordinate.y > 0.5) {
			res = vec2(newCoordinate.x, newCoordinate.y - 0.25); }
		else {
			res = vec2(newCoordinate.x, 0.75 - newCoordinate.y); }
	}

	else if(mirrorType==0.5) {
		if (newCoordinate.y > 0.5) {
			res = vec2(newCoordinate.x, 1.25 - newCoordinate.y); }
		else {
			res = vec2(newCoordinate.x, 0.25 + newCoordinate.y); }
	}

	return res;
}


void main()
{
    vec2 tempCoordinate = getMirrorEffect_();
    mediump vec3 texel;
    mediump vec2 lookup;
    vec2 blue;
    vec2 green;
    vec2 red;
    mediump vec4 tmpvar_1;
    tmpvar_1 = texture2D (inputImageTexture, tempCoordinate);
    texel = tmpvar_1.xyz;
    mediump vec4 tmpvar_2;
    tmpvar_2 = texture2D (inputImageTexture2, textureCoordinate);
    mediump vec2 tmpvar_3;
    tmpvar_3.x = tmpvar_2.x;
    tmpvar_3.y = tmpvar_1.x;
    texel.x = texture2D (inputImageTexture3, tmpvar_3).x;
    mediump vec2 tmpvar_4;
    tmpvar_4.x = tmpvar_2.y;
    tmpvar_4.y = tmpvar_1.y;
    texel.y = texture2D (inputImageTexture3, tmpvar_4).y;
    mediump vec2 tmpvar_5;
    tmpvar_5.x = tmpvar_2.z;
    tmpvar_5.y = tmpvar_1.z;
    texel.z = texture2D (inputImageTexture3, tmpvar_5).z;
    red.x = texel.x;
    red.y = 0.16666;
    green.x = texel.y;
    green.y = 0.5;
    blue.x = texel.z;
    blue.y = 0.833333;
    texel.x = texture2D (inputImageTexture4, red).x;
    texel.y = texture2D (inputImageTexture4, green).y;
    texel.z = texture2D (inputImageTexture4, blue).z;
    mediump vec2 tmpvar_6;
    tmpvar_6 = ((2.0 * textureCoordinate) - 1.0);
    mediump vec2 tmpvar_7;
    tmpvar_7.x = dot (tmpvar_6, tmpvar_6);
    tmpvar_7.y = texel.x;
    lookup = tmpvar_7;
    texel.x = texture2D (inputImageTexture5, tmpvar_7).x;
    lookup.y = texel.y;
    texel.y = texture2D (inputImageTexture5, lookup).y;
    lookup.y = texel.z;
    texel.z = texture2D (inputImageTexture5, lookup).z;
    red.x = texel.x;
    green.x = texel.y;
    blue.x = texel.z;
    texel.x = texture2D (inputImageTexture6, red).x;
    texel.y = texture2D (inputImageTexture6, green).y;
    texel.z = texture2D (inputImageTexture6, blue).z;
    mediump vec4 tmpvar_8;
    tmpvar_8.w = 1.0;
    tmpvar_8.xyz = texel;
    gl_FragColor = tmpvar_8;
}