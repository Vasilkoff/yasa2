varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
void main()
{
	if (textureCoordinate.y > 0.5) {
		gl_FragColor = texture2D(inputImageTexture, vec2(textureCoordinate.x, 1.25 - textureCoordinate.y) );
	}
	else {
		gl_FragColor = texture2D(inputImageTexture, vec2(textureCoordinate.x, 0.25 + textureCoordinate.y) );
	}
};