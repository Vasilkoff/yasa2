varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
uniform sampler2D inputImageTexture2; // lookup texture
uniform mediump float effect2;

mediump vec2 getMirrorEffect_() {
	mediump float mirrorType = effect2;
	mediump vec2 newCoordinate = vec2(textureCoordinate.x, textureCoordinate.y);
	mediump vec2 res = vec2(textureCoordinate.x, textureCoordinate.y);

	if(mirrorType==3.5) {
		if (newCoordinate.x > 0.5) {
			res = vec2(1.25 - newCoordinate.x, newCoordinate.y); }
		else {
			res = vec2(0.25 + newCoordinate.x, newCoordinate.y); }
	}

	else if(mirrorType==2.5) {
		if (newCoordinate.y > 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x),  1.5*(1.0 - newCoordinate.y) );}
		if (newCoordinate.y > 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*(1.0 - newCoordinate.y) );}
		if (newCoordinate.y < 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x), 1.5*newCoordinate.y );}
		if (newCoordinate.y < 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*newCoordinate.y );}
	}

	else if(mirrorType==1.5) {
		if (newCoordinate.y > 0.5) {
			res = vec2(newCoordinate.x, newCoordinate.y - 0.25); }
		else {
			res = vec2(newCoordinate.x, 0.75 - newCoordinate.y); }
	}

	else if(mirrorType==0.5) {
		if (newCoordinate.y > 0.5) {
			res = vec2(newCoordinate.x, 1.25 - newCoordinate.y); }
		else {
			res = vec2(newCoordinate.x, 0.25 + newCoordinate.y); }
	}

	return res;
}

void main()
{
	highp vec2 tempCoordinate = getMirrorEffect_();
	lowp vec4 textureColor = texture2D(inputImageTexture, tempCoordinate);

	mediump float blueColor = textureColor.b * 63.0;

	mediump vec2 quad1;
	quad1.y = floor(floor(blueColor) / 8.0);
	quad1.x = floor(blueColor) - (quad1.y * 8.0);

	mediump vec2 quad2;
	quad2.y = floor(ceil(blueColor) / 8.0);
	quad2.x = ceil(blueColor) - (quad2.y * 8.0);

	highp vec2 texPos1;
	texPos1.x = (quad1.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.r);
	texPos1.y = (quad1.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.g);

	highp vec2 texPos2;
	texPos2.x = (quad2.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.r);
	texPos2.y = (quad2.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.g);

	lowp vec4 newColor1 = texture2D(inputImageTexture2, texPos1);
	lowp vec4 newColor2 = texture2D(inputImageTexture2, texPos2);

	lowp vec4 newColor = mix(newColor1, newColor2, fract(blueColor));
	gl_FragColor = vec4(newColor.rgb, textureColor.w);
}