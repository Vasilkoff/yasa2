varying highp vec2 textureCoordinate; 
precision highp float; 

uniform sampler2D inputImageTexture; 
uniform sampler2D curve;
uniform float effect2;

vec2 getMirrorEffect_() {
	float mirrorType = effect2;
	vec2 newCoordinate = vec2(textureCoordinate.x, textureCoordinate.y);
	vec2 res = vec2(textureCoordinate.x, textureCoordinate.y);

	if(mirrorType==3.5) {
		if (newCoordinate.x > 0.5) {
			res = vec2(1.25 - newCoordinate.x, newCoordinate.y); }
		else {
			res = vec2(0.25 + newCoordinate.x, newCoordinate.y); }
	}

	else if(mirrorType==2.5) {
		if (newCoordinate.y > 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x),  1.5*(1.0 - newCoordinate.y) );}
		if (newCoordinate.y > 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*(1.0 - newCoordinate.y) );}
		if (newCoordinate.y < 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x), 1.5*newCoordinate.y );}
		if (newCoordinate.y < 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*newCoordinate.y );}
	}

	else if(mirrorType==1.5) {
		if (newCoordinate.y > 0.5) {
			res = vec2(newCoordinate.x, newCoordinate.y - 0.25); }
		else {
			res = vec2(newCoordinate.x, 0.75 - newCoordinate.y); }
	}

	else if(mirrorType==0.5) {
		if (newCoordinate.y > 0.5) {
			res = vec2(newCoordinate.x, 1.25 - newCoordinate.y); }
		else {
			res = vec2(newCoordinate.x, 0.25 + newCoordinate.y); }
	}

	return res;
}

void main()
{
	vec2 tempCoordinate = getMirrorEffect_();
	lowp vec4 textureColor;
	lowp vec4 textureColorRes; 
	lowp vec4 textureColorOri; 
	vec4 grey1Color; 
	vec4 layerColor; 
	mediump float satVal = 115.0 / 100.0; 

    float xCoordinate = tempCoordinate.x;
	float yCoordinate = tempCoordinate.y;
	
	highp float redCurveValue; 
	highp float greenCurveValue; 
	highp float blueCurveValue; 

	textureColor = texture2D( inputImageTexture, vec2(xCoordinate, yCoordinate)); 
    textureColorRes = textureColor; 
	textureColorOri = textureColor; 

	// step1. screen blending 
	textureColor = 1.0 - ((1.0 - textureColorOri) * (1.0 - textureColorOri)); 
	textureColor = (textureColor - textureColorOri) + textureColorOri; 

	// step2. curve 
	redCurveValue = texture2D(curve, vec2(textureColor.r, 0.0)).r; 
	greenCurveValue = texture2D(curve, vec2(textureColor.g, 0.0)).g; 
    blueCurveValue = texture2D(curve, vec2(textureColor.b, 0.0)).b; 

	// step3. saturation 
	highp float G = (redCurveValue + greenCurveValue + blueCurveValue); 
	G = G / 3.0; 

    redCurveValue = ((1.0 - satVal) * G + satVal * redCurveValue); 
	greenCurveValue = ((1.0 - satVal) * G + satVal * greenCurveValue); 
	blueCurveValue = ((1.0 - satVal) * G + satVal * blueCurveValue); 

	textureColor = vec4(redCurveValue, greenCurveValue, blueCurveValue, 1.0); 

    gl_FragColor = vec4(textureColor.r, textureColor.g, textureColor.b, 1.0); 
}
