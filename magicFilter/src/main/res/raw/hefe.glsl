 precision mediump float;
 
 varying mediump vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;  //edgeBurn
 uniform sampler2D inputImageTexture3;  //hefeMap
 uniform sampler2D inputImageTexture4;  //hefeGradientMap
 uniform sampler2D inputImageTexture5;  //hefeSoftLight
 uniform sampler2D inputImageTexture6;  //hefeMetal
 
 uniform float strength;
 uniform float effect2;

 vec2 getMirrorEffect_() {
 	float mirrorType = effect2;
 	vec2 newCoordinate = vec2(textureCoordinate.x, textureCoordinate.y);
 	vec2 res = vec2(textureCoordinate.x, textureCoordinate.y);

 	if(mirrorType==3.5) {
 		if (newCoordinate.x > 0.5) {
 			res = vec2(1.25 - newCoordinate.x, newCoordinate.y); }
 		else {
 			res = vec2(0.25 + newCoordinate.x, newCoordinate.y); }
 	}

 	else if(mirrorType==2.5) {
 		if (newCoordinate.y > 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x),  1.5*(1.0 - newCoordinate.y) );}
 		if (newCoordinate.y > 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*(1.0 - newCoordinate.y) );}
 		if (newCoordinate.y < 0.5 && newCoordinate.x > 0.5) { res = vec2( 1.5*(1.0 - newCoordinate.x), 1.5*newCoordinate.y );}
 		if (newCoordinate.y < 0.5 && newCoordinate.x < 0.5) { res = vec2( 1.5*newCoordinate.x, 1.5*newCoordinate.y );}
 	}

 	else if(mirrorType==1.5) {
 		if (newCoordinate.y > 0.5) {
 			res = vec2(newCoordinate.x, newCoordinate.y - 0.25); }
 		else {
 			res = vec2(newCoordinate.x, 0.75 - newCoordinate.y); }
 	}

 	else if(mirrorType==0.5) {
 		if (newCoordinate.y > 0.5) {
 			res = vec2(newCoordinate.x, 1.25 - newCoordinate.y); }
 		else {
 			res = vec2(newCoordinate.x, 0.25 + newCoordinate.y); }
 	}

 	return res;
 }


 void main()
{
    vec2 tempCoordinate = getMirrorEffect_();
    vec4 originColor = texture2D(inputImageTexture, tempCoordinate);
    vec3 texel = texture2D(inputImageTexture, tempCoordinate).rgb;
    vec3 edge = texture2D(inputImageTexture2, tempCoordinate).rgb;
    texel = texel * edge;
    
    texel = vec3(
                 texture2D(inputImageTexture3, vec2(texel.r, .16666)).r,
                 texture2D(inputImageTexture3, vec2(texel.g, .5)).g,
                 texture2D(inputImageTexture3, vec2(texel.b, .83333)).b);
    
    vec3 luma = vec3(.30, .59, .11);
    vec3 gradSample = texture2D(inputImageTexture4, vec2(dot(luma, texel), .5)).rgb;
    vec3 final = vec3(
                      texture2D(inputImageTexture5, vec2(gradSample.r, texel.r)).r,
                      texture2D(inputImageTexture5, vec2(gradSample.g, texel.g)).g,
                      texture2D(inputImageTexture5, vec2(gradSample.b, texel.b)).b
                      );
    
    vec3 metal = texture2D(inputImageTexture6, tempCoordinate).rgb;
    vec3 metaled = vec3(
                        texture2D(inputImageTexture5, vec2(metal.r, texel.r)).r,
                        texture2D(inputImageTexture5, vec2(metal.g, texel.g)).g,
                        texture2D(inputImageTexture5, vec2(metal.b, texel.b)).b
                        );
    
    metaled.rgb = mix(originColor.rgb, metaled.rgb, strength);

    gl_FragColor = vec4(metaled, 1.0);
}