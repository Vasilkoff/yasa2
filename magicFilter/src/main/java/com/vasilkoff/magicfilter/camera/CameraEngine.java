package com.vasilkoff.magicfilter.camera;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;

public class CameraEngine {
	private static Camera mCamera = null;
	private static int mCameraID = 0;
	
	public static Camera getCamera(){
		return mCamera;
	}
	
	public static boolean openCamera(){
		if(mCamera == null){
			try{
				mCamera = Camera.open(mCameraID);
				setDefaultParameters();
				return true;
			}catch(RuntimeException e){
				return false;
			}
		}
		return false;
	}

	public static boolean openCamera(int cameraId){
        mCameraID = cameraId;
		if(mCamera == null){
			try{
				mCamera = Camera.open(mCameraID);
				setDefaultParameters();
				return true;
			}catch(RuntimeException e){
				return false;
			}
		}
		return false;
	}
	
	public static void releaseCamera(){
		if(mCamera != null){
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
	public void resumeCamera(){
		openCamera();
	}
	
	public void setParameters(Parameters parameters){
		mCamera.setParameters(parameters);
	}
	
	public Parameters getParameters(){
		if(mCamera != null)
			mCamera.getParameters();
		return null;
	}

    public static void startFaceDetection() {
        mCamera.startFaceDetection();
    }

    public static void setFaceDetectionListener(Camera.FaceDetectionListener listener) {
        mCamera.setFaceDetectionListener(listener);
    }

    public static void stopFaceDetection() {
        mCamera.stopFaceDetection();
    }

    public static boolean checkFaceDetection() {
        boolean res = false;
		if (mCamera!=null) {
			Parameters parameters = mCamera.getParameters();
            res = parameters.getMaxNumDetectedFaces() > 0;
		}
        return res;
    }
    public static void cancelAutoFocus() {
        mCamera.cancelAutoFocus();
    }
    public static void setAutoFocus() {
        Parameters parameters = mCamera.getParameters();
        if (parameters.getSupportedFocusModes().contains(
                Parameters.FOCUS_MODE_AUTO)) {
            parameters.setFocusMode(Parameters.FOCUS_MODE_AUTO);
        }
        mCamera.setParameters(parameters);
    }
	public static void startAutoFocus() { mCamera.autoFocus(null); }
    public static void addZoom(int z) {
        Parameters parameters = mCamera.getParameters();
        int max = parameters.getMaxZoom();
        if (z>max) z=max;
        /*int current = parameters.getZoom();
        System.out.println("MAXZOON " + max + " ZOON " + current + "SUUP " + parameters.isZoomSupported());*/
        if (parameters.isZoomSupported()) {
            if (z>=0 && z<=max) {
                parameters.setZoom(z);
                mCamera.setParameters(parameters);
            }
        }
    }
	private static void setDefaultParameters(){
		Parameters parameters = mCamera.getParameters();
		if (parameters.getSupportedFocusModes().contains(
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

		//Size previewSize = getLargePreviewSize();
		Size previewSize = getLargePreviewSizeNew();
        /*System.out.println("tempPREVIE_SIZE_ " + previewSize.width + " " + previewSize.height);*/
		parameters.setPreviewSize(previewSize.width, previewSize.height);

		Size pictureSize = getLargePictureSize(previewSize);
		parameters.setPictureSize(pictureSize.width, pictureSize.height);
		mCamera.setParameters(parameters);
	}

	private static Size getLargePreviewSize(){
		if(mCamera != null){
			List<Size> sizes = mCamera.getParameters().getSupportedPreviewSizes();
			Size temp = sizes.get(0);
            //System.out.println("temp1111111a1_ " + temp.width + " " + temp.height);
				for(int i = 1;i < sizes.size();i ++){
                    //System.out.println("temp11111111_ " + sizes.get(i).width + " " + sizes.get(i).height);
					if(temp.width < sizes.get(i).width)
						temp = sizes.get(i);
			}
			//System.out.println("temp_ " + temp.width + " " + temp.height);
			return temp;
		}
		return null;
	}

    private static double ratioMY = 0.75f;
    private static Size getLargePreviewSizeNew(){
        if(mCamera != null){
            double ratio;
            List<Size> sizes = mCamera.getParameters().getSupportedPreviewSizes();

            Collections.sort(sizes, new Comparator<Size>() {
                @Override
                public int compare(Size lhs, Size rhs) {
                    return -lhs.width * lhs.height + rhs.width * rhs.height;
                }
            });

            Size temp = sizes.get(0);
            for(int i = 0;i < sizes.size();i ++){
                ratio = (double)sizes.get(i).height/ (double)sizes.get(i).width ;
                if( Math.abs(ratio - ratioMY) <= 0.02 ) {
                    System.out.println("fdfd_xy " + sizes.get(i).width + " " + sizes.get(i).height);
                    return sizes.get(i);
                }
            }
            return temp;
        }
        return null;
    }
	
	private static Size getLargePictureSize(Size previewSize){
		if(mCamera != null) {
            float ratio = previewSize.width/(float)previewSize.height;
			List<Size> sizes = mCamera.getParameters().getSupportedPictureSizes();
            Collections.sort(sizes, new Comparator<Size>() {
                @Override
                public int compare(Size lhs, Size rhs) {
                    return -lhs.width*lhs.height+rhs.width*rhs.height;
                }
            });
            int index = 0;
            float difference = Float.MAX_VALUE;
			for(int i = 0;i < sizes.size();i ++) {
                Size s1 = sizes.get(i);
                float scale = s1.width/(float)s1.height;
                if(Math.abs(scale-ratio)<difference) {
                    difference = Math.abs(scale-ratio);
                    index = i;
                }

			}
            /*System.out.println("temp_picture_size_ " + sizes.get(index).width + " " + sizes.get(index).height);*/
			return sizes.get(index);
		}
		return null;
	}
	
	public static Size getPreviewSize(){
		return mCamera.getParameters().getPreviewSize();	
	}
	
	public static void startPreview(SurfaceTexture surfaceTexture){
		try {
			mCamera.setPreviewTexture(surfaceTexture);
			mCamera.startPreview();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static void startPreview(){
		if(mCamera != null)
			mCamera.startPreview();
	}
	
	public static void stopPreview(){
		mCamera.stopPreview();
	}
	
	public static CameraInfo getCameraInfo(){
		CameraInfo cameraInfo = new CameraInfo();
		Camera.getCameraInfo(mCameraID, cameraInfo);
		return cameraInfo;
	}
	
	public static int getOrientation(){
		CameraInfo cameraInfo = new CameraInfo();
		Camera.getCameraInfo(mCameraID, cameraInfo);
		return cameraInfo.orientation;
	}
	
	public static boolean isFlipHorizontal(){
		return CameraEngine.getCameraInfo().facing == CameraInfo.CAMERA_FACING_FRONT ? true : false;
	}
	
	public static void setRotation(int rotation){
		Camera.Parameters params = mCamera.getParameters();
        params.setRotation(rotation);
        mCamera.setParameters(params);
	}

	public static int getNumberOfDeviceCameras(){
		int params = mCamera.getNumberOfCameras();
		return params;
	}

	public static void takePicture(Camera.ShutterCallback shutterCallback, Camera.PictureCallback rawCallback, 
			Camera.PictureCallback jpegCallback){
		mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
	}

	public static int getFacing(){
		CameraInfo cameraInfo = new CameraInfo();
		Camera.getCameraInfo(mCameraID, cameraInfo);
		return cameraInfo.facing;
	}

    public static final int FRONT_CAMERA_ROTATION = 270;
    public static final int BACK_CAMERA_ROTATION = 90;
    public static boolean BACK_CAMERA_IN_USE = true;

	//PHOTO
	public static Bitmap bitmap;
	//CAPTURE
	public static Bitmap captured;

	public static Bitmap nonfiltered;

	public static int current_mirror_type = 0;
}
