package com.vasilkoff.magicfilter.display;

import java.io.File;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.vasilkoff.magicfilter.BitmapHelper;
import com.vasilkoff.magicfilter.camera.CameraEngine;
import com.vasilkoff.magicfilter.filter.base.MagicCameraInputFilter;
import com.vasilkoff.magicfilter.filter.helper.MagicFilterParam;
import com.vasilkoff.magicfilter.utils.OpenGLUtils;
import com.vasilkoff.magicfilter.utils.Rotation;
import com.vasilkoff.magicfilter.utils.SaveTask;
import com.vasilkoff.magicfilter.utils.SaveTaskLight;
import com.vasilkoff.magicfilter.utils.TextureRotationUtil;
import com.vasilkoff.magicfilter.utils.SaveTask.onPictureSaveListener;
import android.app.Activity;
import android.view.Surface;

/**
 * MagicCameraDisplay is used for camera preview
 */
public class MagicCameraDisplay extends MagicDisplay{	

	private final MagicCameraInputFilter mCameraInputFilter;

	private SurfaceTexture mSurfaceTexture;

	private Activity context41;
    
	public MagicCameraDisplay(Context context, GLSurfaceView glSurfaceView){
		super(context, glSurfaceView);
		context41 = (Activity)context;
		mCameraInputFilter = new MagicCameraInputFilter();
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		GLES20.glDisable(GL10.GL_DITHER);
        GLES20.glClearColor(0,0,0,0);
        GLES20.glEnable(GL10.GL_CULL_FACE);
        GLES20.glEnable(GL10.GL_DEPTH_TEST);
        MagicFilterParam.initMagicFilterParam(gl);
        mCameraInputFilter.init();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		GLES20.glViewport(0, 0, width, height);
		mSurfaceWidth = width;
		mSurfaceHeight = height;
		onFilterChanged();
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);	
		mSurfaceTexture.updateTexImage();
		float[] mtx = new float[16];
		mSurfaceTexture.getTransformMatrix(mtx);
		mCameraInputFilter.setTextureTransformMatrix(mtx);
		if(mFilters == null){
			mCameraInputFilter.onDrawFrame(mTextureId, mGLCubeBuffer, mGLTextureBuffer);
		}else{
			int textureID = mCameraInputFilter.onDrawToTexture(mTextureId);	
			mFilters.onDrawFrame(textureID, mGLCubeBuffer, mGLTextureBuffer);
		}
	}
	
	private OnFrameAvailableListener mOnFrameAvailableListener = new OnFrameAvailableListener() {
		
		@Override
		public void onFrameAvailable(SurfaceTexture surfaceTexture) {
			// TODO Auto-generated method stub
			mGLSurfaceView.requestRender();
		}
	};

	private void setUpCamera(){
		mGLSurfaceView.queueEvent(new Runnable() {

            @Override
            public void run() {
                if (mTextureId == OpenGLUtils.NO_TEXTURE) {
                    mTextureId = OpenGLUtils.getExternalOESTextureID();
                    mSurfaceTexture = new SurfaceTexture(mTextureId);
                    mSurfaceTexture.setOnFrameAvailableListener(mOnFrameAvailableListener);
                }

                Size size = CameraEngine.getPreviewSize();
				/*System.out.println("size is "+size.width + " " + size.height);*/
                int orientation = CameraEngine.getOrientation();
                if (orientation == 90 || orientation == 270) {
                    mImageWidth = size.height;
                    mImageHeight = size.width;
                } else {
                    mImageWidth = size.width;
                    mImageHeight = size.height;
                }
                mCameraInputFilter.onOutputSizeChanged(mImageWidth, mImageHeight);
                CameraEngine.startPreview(mSurfaceTexture);
            }
        });
    }

    public void onReleaseCamera() {
        CameraEngine.releaseCamera();
    }

    public int getCameraNumber() {
        return CameraEngine.getNumberOfDeviceCameras();
    }
    public void onOpenCameraById(int id) {
        CameraEngine.openCamera(id);
    }

    public void onOpenCamera() {
        CameraEngine.openCamera();
    }

    public Camera onGetCamera() {
        return CameraEngine.getCamera();
    }

    public void onStopPreview() {
        CameraEngine.stopPreview();
    }
	
	protected void onFilterChanged(){
		super.onFilterChanged();
		mCameraInputFilter.onDisplaySizeChanged(mSurfaceWidth, mSurfaceHeight);
		if(mFilters != null)
			mCameraInputFilter.initCameraFrameBuffer(mImageWidth, mImageHeight);
		else
			mCameraInputFilter.destroyFramebuffers();
	}
	
	public void onResume(){
		super.onResume();
		if(CameraEngine.getCamera() == null)
        	CameraEngine.openCamera();
		if(CameraEngine.getCamera() != null){
			boolean flipHorizontal = CameraEngine.isFlipHorizontal();
			int betterOrientation = getCameraDisplayOrientation(context41);
			adjustPosition(/*CameraEngine.getOrientation()*/betterOrientation, flipHorizontal, !flipHorizontal);
		}
		if(CameraEngine.getCamera() != null) {
            CameraEngine.addZoom(0);//default zoom
            //CameraEngine.cancelAutoFocus();
        }
		setUpCamera();
	}
	public void onChangeCamera() {
        if(CameraEngine.getCamera() != null){
            boolean flipHorizontal = CameraEngine.isFlipHorizontal();
            //System.out.println("orienttion " + CameraEngine.getOrientation());
            int betterOrientation = getCameraDisplayOrientation(context41);
            adjustPosition(/*CameraEngine.getOrientation()*/betterOrientation, flipHorizontal, !flipHorizontal);
        }
        setUpCamera();
    }
	public void onPause(){
		super.onPause();
		CameraEngine.releaseCamera();
	}

	public void onDestroy(){
		super.onDestroy();
	}

    private onPictureSaveListener collageListener;
	public void onTakePictureSimple(onPictureSaveListener listener){
        collageListener = listener;
		CameraEngine.takePicture(null, null, mPictureCallback3);
	}
    public void onTakePicture(File file, onPictureSaveListener listener,ShutterCallback shutterCallback, boolean isSave){
        mSaveTask = new SaveTask(mContext, file, listener, isSave);
        CameraEngine.takePicture(shutterCallback, null, mPictureCallback);
    }

	public void onTakePicture(File file ,onPictureSaveListener listener,ShutterCallback shutterCallback){
		//bellow line rotates on 90degree; seems like original author used only back camera
		//CameraEngine.setRotation(90);
		mSaveTask = new SaveTask(mContext, file, listener);
		CameraEngine.takePicture(shutterCallback, null, mPictureCallback);
	}

    protected SaveTaskLight mSaveTaskLight;
	public void onTakePicture(File file, SaveTaskLight.onPictureSaveListenerLight listener,ShutterCallback shutterCallback, boolean isSave){
        mSaveTaskLight = new SaveTaskLight(listener);
		CameraEngine.takePicture(shutterCallback, null, mPictureCallback2);
	}
	
	private PictureCallback mPictureCallback = new PictureCallback() {
		
		@Override
		public void onPictureTaken(final byte[] data,Camera camera) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            if(CameraEngine.BACK_CAMERA_IN_USE) {
                Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(bitmap, CameraEngine.BACK_CAMERA_ROTATION);
                bitmap = null;
                bitmap = rotatedBitmap;
            }
            else {
                Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(bitmap, CameraEngine.FRONT_CAMERA_ROTATION);
                rotatedBitmap = BitmapHelper.flip(rotatedBitmap);
                bitmap = null;
                bitmap = rotatedBitmap;
            }
			if(mFilters != null){
				getBitmapFromGL(bitmap, true);
			}else{
                //skip image saving here
				mSaveTask.execute(bitmap);
			}
		}
	};

    private PictureCallback mPictureCallback2 = new PictureCallback() {

        @Override
        public void onPictureTaken(final byte[] data,Camera camera) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            Bitmap bitmap2 = bitmap.copy(bitmap.getConfig(), true);;
            /*System.out.println("bitmap_si_ " + bitmap.getWidth() + " " + bitmap.getHeight());*/
            //bitmap = rotateTakenPhoto(bitmap);

            //CameraEngine.nonfiltered = bitmap.copy(bitmap.getConfig(), true);
            if(mFilters != null){
                bitmap2 = rotateTakenPhoto(bitmap2);
                CameraEngine.nonfiltered = bitmap2.copy(bitmap.getConfig(), true);
                bitmap2.recycle();
                getBitmapFromGL2(bitmap, true);
            }else{
                bitmap = rotateTakenPhoto(bitmap);
                CameraEngine.nonfiltered = bitmap.copy(bitmap.getConfig(), true);
                mSaveTaskLight.execute(bitmap);
            }
        }
    };

    private PictureCallback mPictureCallback3 = new PictureCallback() {

        @Override
        public void onPictureTaken(final byte[] data,Camera camera) {
            collageListener.onSaved("saved");
        }
    };
	
	protected void onGetBitmapFromGL(Bitmap bitmap){
		mSaveTask.execute(bitmap);
	}

    protected void onGetBitmapFromGL2(Bitmap bitmap){
        //mSaveTaskLight.execute(bitmap);
        mSaveTaskLight.execute( rotateTakenPhoto(bitmap) );
    }

    private onFilterReadyListener filterApplied;
	public void applyFilterToImageShuttered(Bitmap bitmap, onFilterReadyListener listener){
		if(mFilters!=null) {
            filterApplied = listener;
            getBitmapFromGL3(bitmap, true);
        }
	}

    protected void onGetBitmapFromGL3(Bitmap bitmap){
        /*CameraEngine.filtered = bitmap;*/
        filterApplied.onFilterReady(bitmap);
    }

	private Bitmap rotateTakenPhoto(Bitmap b) {
		if(CameraEngine.BACK_CAMERA_IN_USE) {
			Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(b, CameraEngine.BACK_CAMERA_ROTATION);
			b = null;
			b = rotatedBitmap;
		}
		else {
			Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(b, CameraEngine.FRONT_CAMERA_ROTATION);
			rotatedBitmap = BitmapHelper.flip(rotatedBitmap);
			b = null;
			b = rotatedBitmap;
		}
		return b;
	}

	private void adjustPosition(int orientation, boolean flipHorizontal,boolean flipVertical) {
        Rotation mRotation = Rotation.fromInt(orientation);
        float[] textureCords = TextureRotationUtil.getRotation(mRotation, flipHorizontal, flipVertical);
        mGLTextureBuffer.clear();
        mGLTextureBuffer.put(textureCords).position(0);
    }

	public int getCameraDisplayOrientation(final Activity activity) {
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
		}

		int result;
		int orientation = CameraEngine.getOrientation();
		int facing = CameraEngine.getFacing();
		//CameraInfo2 info = new CameraInfo2();
		if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (orientation + degrees) % 360;
		} else { // back-facing
			result = (orientation - degrees + 360) % 360;
		}
		return result;
	}

	public int getCameraFacing() {
		return CameraEngine.getFacing();
	}

    public interface onFilterReadyListener{
        void onFilterReady(Bitmap saved);
    }
}
