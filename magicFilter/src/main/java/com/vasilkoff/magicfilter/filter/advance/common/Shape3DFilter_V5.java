package com.vasilkoff.magicfilter.filter.advance.common;

import android.content.Context;
import android.opengl.GLES20;

import com.vasilkoff.magicfilter.R;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageFilter;
import com.vasilkoff.magicfilter.utils.OpenGLUtils;

/**
 * Created by Vasilkoff on 11.05.2016.
 */
public class Shape3DFilter_V5 extends GPUImageFilter {
    private int[] inputTextureHandles = {-1,-1,-1};
    private int[] inputTextureUniformLocations = {-1,-1,-1};
    private Context mContext;

    public Shape3DFilter_V5(Context context){
        //super(NO_FILTER_VERTEX_SHADER,OpenGLUtils.readShaderFromRawResource(context, R.raw.brannan));
        super(
                OpenGLUtils.readShaderFromRawResource(context, R.raw.vertex_shader_blur_h),
                OpenGLUtils.readShaderFromRawResource(context, R.raw.shape3d_v1));
        mContext = context;
    }

    protected void onDestroy() {
        super.onDestroy();
        GLES20.glDeleteTextures(inputTextureHandles.length, inputTextureHandles, 0);
        for(int i = 0; i < inputTextureHandles.length; i++)
            inputTextureHandles[i] = -1;
    }

    protected void onDrawArraysAfter(){
        for(int i = 0; i < inputTextureHandles.length
                && inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+3));
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        }
    }

    protected void onDrawArraysPre(){
        for(int i = 0; i < inputTextureHandles.length
                && inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+3) );
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, inputTextureHandles[i]);
            GLES20.glUniform1i(inputTextureUniformLocations[i], (i+3));
        }
    }

    protected void onInit(){
        super.onInit();
        for(int i=0; i < inputTextureUniformLocations.length; i++){
            inputTextureUniformLocations[i] = GLES20.glGetUniformLocation(getProgram(), "inputImageTexture"+(2+i));
        }
    }

    protected void onInitialized(){
        super.onInitialized();
        runOnDraw(new Runnable(){
            public void run(){
                inputTextureHandles[0] = OpenGLUtils.loadTexture(mContext, "filter/brannan_process.png");
                inputTextureHandles[1] = OpenGLUtils.loadTexture(mContext, "filter/shp5_mask.png");
                inputTextureHandles[2] = OpenGLUtils.loadTexture(mContext, "filter/shp5_.png");
                //inputTextureHandles[3] = OpenGLUtils.loadTexture(mContext, "filter/brannan_luma.png");
                //inputTextureHandles[4] = OpenGLUtils.loadTexture(mContext, "filter/brannan_screen.png");
            }
        });
    }
}
