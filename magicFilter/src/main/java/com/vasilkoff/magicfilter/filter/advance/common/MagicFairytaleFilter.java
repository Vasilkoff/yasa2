package com.vasilkoff.magicfilter.filter.advance.common;

import android.content.Context;

import com.vasilkoff.magicfilter.filter.base.MagicLookupFilter;

public class MagicFairytaleFilter extends MagicLookupFilter{

	public MagicFairytaleFilter(Context context) {
		super(context, "filter/fairy_tale.png");
	}
}
