package com.vasilkoff.magicfilter.filter.factory;

import android.content.Context;

import com.vasilkoff.magicfilter.filter.advance.common.MagicAmaroFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicAntiqueFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicBeautyFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicBlackCatFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicBrannanFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicBrooklynFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicCalmFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicCoolFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicEarlyBirdFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicEmeraldFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicEvergreenFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicFairytaleFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicFreudFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicHealthyFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicHefeFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicHudsonFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicInkwellFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicKevinFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicLatteFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicMirrorFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicMirrorFilter2;
import com.vasilkoff.magicfilter.filter.advance.common.MagicMirrorFilter3;
import com.vasilkoff.magicfilter.filter.advance.common.MagicMirrorFilter4;
import com.vasilkoff.magicfilter.filter.advance.common.MagicN1977Filter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicNashvilleFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicNostalgiaFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicPixarFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicRiseFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicRomanceFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSakuraFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSierraFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSkinWhitenFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSunriseFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSunsetFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSutroFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicSweetsFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicTenderFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicToasterFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicWaldenFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicWarmFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicWhiteCatFilter;
import com.vasilkoff.magicfilter.filter.advance.common.MagicXproIIFilter;
import com.vasilkoff.magicfilter.filter.advance.common.Shape3DFilter_V1;
import com.vasilkoff.magicfilter.filter.advance.common.Shape3DFilter_V2;
import com.vasilkoff.magicfilter.filter.advance.common.Shape3DFilter_V3;
import com.vasilkoff.magicfilter.filter.advance.common.Shape3DFilter_V4;
import com.vasilkoff.magicfilter.filter.advance.common.Shape3DFilter_V5;
import com.vasilkoff.magicfilter.filter.advance.image.MagicImageAdjustFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageBrightnessFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageContrastFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageExposureFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageHueFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageSaturationFilter;
import com.vasilkoff.magicfilter.filter.base.gpuimage.GPUImageSharpenFilter;
import com.vasilkoff.magicfilter.filter.helper.MagicFilterType;

public class MagicFilterFactory{
	
	private static int mFilterType = MagicFilterType.NONE;	
	
	public static GPUImageFilter getFilters(int type, Context mContext){
		mFilterType = type;
		switch (type) {
		case MagicFilterType.WHITECAT:
			return new MagicWhiteCatFilter(mContext);
		case MagicFilterType.BLACKCAT:
			return new MagicBlackCatFilter(mContext);
		case MagicFilterType.BEAUTY:
			return new MagicBeautyFilter(mContext);
		case MagicFilterType.SKINWHITEN:
			//List<GPUImageFilter> filters = new ArrayList<GPUImageFilter>();	
			//filters.add(new MagicBilateralFilter(mContext));
			//filters.add(new MagicSkinWhitenFilter(mContext));
			//return new MagicBaseGroupFilter(filters);
			return new MagicSkinWhitenFilter(mContext);
		case MagicFilterType.ROMANCE:
			return new MagicRomanceFilter(mContext);
		case MagicFilterType.SAKURA:
			return new MagicSakuraFilter(mContext);
		case MagicFilterType.AMARO:
			return new MagicAmaroFilter(mContext);
		case MagicFilterType.WALDEN:
			return new MagicWaldenFilter(mContext);
		case MagicFilterType.ANTIQUE:
			return new MagicAntiqueFilter(mContext);
		case MagicFilterType.CALM:
			return new MagicCalmFilter(mContext);
		case MagicFilterType.BRANNAN:
			return new MagicBrannanFilter(mContext);
		case MagicFilterType.BROOKLYN:
			return new MagicBrooklynFilter(mContext);
		case MagicFilterType.EARLYBIRD:
			return new MagicEarlyBirdFilter(mContext);
		case MagicFilterType.FREUD:
			return new MagicFreudFilter(mContext);
		case MagicFilterType.HEFE:
			return new MagicHefeFilter(mContext);
		case MagicFilterType.HUDSON:
			return new MagicHudsonFilter(mContext);
		case MagicFilterType.INKWELL:
			return new MagicInkwellFilter(mContext);
		case MagicFilterType.KEVIN:
			return new MagicKevinFilter(mContext);
		/*case MagicFilterType.LOMO:
			return new MagicLomoFilter(mContext);*/
		case MagicFilterType.N1977:
			return new MagicN1977Filter(mContext);
		case MagicFilterType.NASHVILLE:
			return new MagicNashvilleFilter(mContext);
		case MagicFilterType.PIXAR:
			return new MagicPixarFilter(mContext);
		case MagicFilterType.RISE:
			return new MagicRiseFilter(mContext);
		case MagicFilterType.SIERRA:
			return new MagicSierraFilter(mContext);
		case MagicFilterType.SUTRO:
			return new MagicSutroFilter(mContext);
		case MagicFilterType.TOASTER2:
			return new MagicToasterFilter(mContext);
		/*case MagicFilterType.VALENCIA:
			return new MagicValenciaFilter(mContext);*/
		case MagicFilterType.XPROII:
			return new MagicXproIIFilter(mContext);
		case MagicFilterType.EVERGREEN:
			return new MagicEvergreenFilter(mContext);
		case MagicFilterType.HEALTHY:
			return new MagicHealthyFilter(mContext);
		case MagicFilterType.COOL:
			return new MagicCoolFilter(mContext);
		case MagicFilterType.EMERALD:
			return new MagicEmeraldFilter(mContext);
		case MagicFilterType.LATTE:
			return new MagicLatteFilter(mContext);
		case MagicFilterType.WARM:
			return new MagicWarmFilter(mContext);
		case MagicFilterType.TENDER:
			return new MagicTenderFilter(mContext);
		case MagicFilterType.SWEETS:
			return new MagicSweetsFilter(mContext);
		case MagicFilterType.NOSTALGIA:
			return new MagicNostalgiaFilter(mContext);
		case MagicFilterType.FAIRYTALE:
			return new MagicFairytaleFilter(mContext);
		case MagicFilterType.SUNRISE:
			return new MagicSunriseFilter(mContext);
		case MagicFilterType.SUNSET:
			return new MagicSunsetFilter(mContext);
		case MagicFilterType.BRIGHTNESS:
			return new GPUImageBrightnessFilter();
		case MagicFilterType.CONTRAST:
			return new GPUImageContrastFilter();
		case MagicFilterType.EXPOSURE:
			return new GPUImageExposureFilter();
		case MagicFilterType.HUE:
			return new GPUImageHueFilter();
		case MagicFilterType.SATURATION:
			return new GPUImageSaturationFilter();
		case MagicFilterType.SHARPEN:
			return new GPUImageSharpenFilter();
		case MagicFilterType.IMAGE_ADJUST:
			return new MagicImageAdjustFilter();
		default:
			return null;
		}
	}

	public static GPUImageFilter getMEffects(int type, Context mContext){
        switch (type) {
            case 1:
                return new MagicMirrorFilter2(mContext);
            case 2:
                return new MagicMirrorFilter(mContext);
            case 3:
                return new MagicMirrorFilter3(mContext);
            case 4:
                return new MagicMirrorFilter4(mContext);
            /*case 5:
                return null;*/
            /*case 6:
                return null;*/
            default:
                return null;
        }
	}

	public static GPUImageFilter get3DEffects(int type, Context mContext){
		switch (type) {
			case 1:
				return new Shape3DFilter_V1(mContext);
			case 2:
				return new Shape3DFilter_V2(mContext);
			case 3:
				return new Shape3DFilter_V3(mContext);
			case 4:
				return new Shape3DFilter_V4(mContext);
            case 5:
				return new Shape3DFilter_V5(mContext);
			default:
				return null;
		}
	}
	
	public int getFilterType(){
		return mFilterType;
	}
}
