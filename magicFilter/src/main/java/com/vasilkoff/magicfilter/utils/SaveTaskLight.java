package com.vasilkoff.magicfilter.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.vasilkoff.magicfilter.camera.CameraEngine;

/**
 * Created by Vasilkoff on 30.03.2016.
 */
public class SaveTaskLight extends AsyncTask<Bitmap, Integer, String> {

    private onPictureSaveListenerLight mListener;


    public SaveTaskLight(onPictureSaveListenerLight listener){
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(final String result) {
        mListener.onSaved(true);
        super.onPostExecute(result);
    }

    @Override
    protected String doInBackground(Bitmap... params) {
        CameraEngine.bitmap = params[0];
        //mListener.onSaved(true);
        return "";
    }


    public interface onPictureSaveListenerLight{
        void onSaved(boolean saved);
    }
}
