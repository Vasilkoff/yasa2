package com.vasilkoff.magicfilter.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;

import com.vasilkoff.magicfilter.camera.CameraEngine;

public class SaveTask extends AsyncTask<Bitmap, Integer, String>{

    private onPictureSaveListener mListener;
    private Context mContext;
    private File mFile;
    private boolean save;

    public SaveTask(Context context, File file, onPictureSaveListener listener){
        this.mContext = context;
        this.mListener = listener;
        this.mFile = file;
        this.save = true;
    }

    public SaveTask(Context context, File file, onPictureSaveListener listener, boolean saveToFile){
        this.mContext = context;
        this.mListener = listener;
        this.mFile = file;
        this.save = saveToFile;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(final String result) {
        // TODO Auto-generated method stub
        if(result != null) {
            if (mListener != null) mListener.onSaved(result);
            if (save)
            MediaScannerConnection.scanFile(mContext,
                    new String[]{result}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(final String path, final Uri uri) {
							/*if (mListener != null)
								mListener.onSaved(result);*/
                        }
                    });
        }
    }

    @Override
    protected String doInBackground(Bitmap... params) {
        // TODO Auto-generated method stub
        if(mFile == null)
            return null;
        return saveBitmap(params[0]);
    }

    private String saveBitmap(Bitmap bitmap) {
        /*if (mFile.exists()) {
            mFile.delete();
        }*/
        if (!save) {
            CameraEngine.bitmap = bitmap;
            return "";//mFile.toString();
        }
        else {
            if (mFile.exists()) {
                mFile.delete();
            }
            try {
                FileOutputStream out = new FileOutputStream(mFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                CameraEngine.bitmap = bitmap;
                out.flush();
                out.close();
                //bitmap.recycle();
                return mFile.toString();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    public interface onPictureSaveListener{
        void onSaved(String result);
	}

    /*public interface onPictureSaveListenerLight{
        void onSaved(boolean saved);
    }*/
}
