package com.vasilkoff.maxycamera.common.bean;

public class FilterInfo {
	private boolean bSelected = false;
	private int filterType;
	private boolean bFavourite = false;

	/*Added by Vitali*/
	public int getMirror_type() {
		return mirror_type;
	}

	public void setMirror_type(int mirror_type) {
		this.mirror_type = mirror_type;
	}

	public int getShape_type() {
		return shape_type;
	}

	public void setShape_type(int shape_type) {
		this.shape_type = shape_type;
	}
	private int mirror_type;
	private int shape_type;

	public void setFilterType(int id){
		this.filterType = id;
	}
	
	public int getFilterType(){
		return this.filterType;
	}
	
	public boolean isSelected(){
		return bSelected;
	}
	
	public void setSelected(boolean bSelected){
		this.bSelected = bSelected;
	}
	
	public boolean isFavourite(){
		return bFavourite;
	}
	
	public void setFavourite(boolean bFavourite){
		this.bFavourite = bFavourite;
	}
}