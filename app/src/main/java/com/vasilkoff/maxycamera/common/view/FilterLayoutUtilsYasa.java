package com.vasilkoff.maxycamera.common.view;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.getyasa.R;
import com.getyasa.activities.YasaStickers;
import com.getyasa.activities.camera.utils.Constant;
import com.vasilkoff.magicfilter.camera.CameraEngine;
import com.vasilkoff.magicfilter.display.MagicDisplay;
import com.vasilkoff.magicfilter.filter.helper.MagicFilterType;
import com.vasilkoff.maxycamera.common.adapter.FilterAdapter;
import com.vasilkoff.maxycamera.common.bean.FilterInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasilkoff on 18.02.2016.
 */
public class FilterLayoutUtilsYasa {
    private Context mContext;
    private MagicDisplay mMagicDisplay;
    private FilterAdapter mAdapter;
    //private ImageView btn_Favourite;

    private int position;
    private List<FilterInfo> filterInfos;
    private List<FilterInfo> favouriteFilterInfos;

    private int mFilterType = MagicFilterType.NONE;


    /**
     * we have to handle programmatically onClick
     */
    RecyclerView mFilterListView;
    OnMirrorEffectCallback callback;

    public FilterLayoutUtilsYasa(Context context,MagicDisplay magicDisplay, OnMirrorEffectCallback c) {
        mContext = context;
        mMagicDisplay = magicDisplay;
        callback = c;
    }

    public void init(){
        //btn_Favourite = (ImageView) ((Activity) mContext).findViewById(R.id.btn_camera_favourite);
        //btn_Favourite.setOnClickListener(btn_Favourite_listener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        /*RecyclerView*/ mFilterListView = (RecyclerView)((Activity) mContext).findViewById(R.id.filterRecyclerView);
        mFilterListView.setLayoutManager(linearLayoutManager);

        mAdapter = new FilterAdapter(mContext);
        mFilterListView.setAdapter(mAdapter);
        initFilterInfos();
        //System.out.println("size " + filterInfos.size());
        mAdapter.setFilterInfos(filterInfos);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);

        /*Scroll to Walden*/
        //mFilterListView.getLayoutManager().scrollToPosition(filterInfos.size() - 4 /*- YasaStickers.getM_effects().length*/);
        /*mFilterListView.getLayoutManager().scrollToPosition(2);*/
    }

    public void init(View view){
        //btn_Favourite = (ImageView) view.findViewById(R.id.btn_camera_favourite);
        //btn_Favourite.setOnClickListener(btn_Favourite_listener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        RecyclerView mFilterListView = (RecyclerView) view.findViewById(R.id.filterRecyclerView);
        mFilterListView.setLayoutManager(linearLayoutManager);

        mAdapter = new FilterAdapter(mContext);
        mFilterListView.setAdapter(mAdapter);
        initFilterInfos();
        mAdapter.setFilterInfos(filterInfos);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);
    }

    public void changeSelected(int pos) {
        FilterAdapter mAdapter2 = new FilterAdapter(mContext, pos);
        initFilterInfosWithSelected(pos);
        //mAdapter2.setLastSelected(pos);
        mAdapter2.setFilterInfos(filterInfos);
        mAdapter = mAdapter2;
        mFilterListView.swapAdapter(mAdapter, true);
        mAdapter.setOnFilterChangeListener(null);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);
        //mAdapter2.setOnFilterChangeListener(onFilterChangeListener);
        /*mAdapter = mAdapter2;*/
        mFilterListView.getLayoutManager().scrollToPosition( pos );
    }

    /**
     * MirrorEffect
     * */
    public static interface OnMirrorEffectCallback {
        void onMirrorCallback(String t, int position, int tp_);
    }

    private FilterAdapter.onFilterChangeListener onFilterChangeListener = new FilterAdapter.onFilterChangeListener(){

        @Override
        public void onFilterChanged(int filterType, int position) {
            //if (filterType==-2) {
                //callback.onMirrorCallback("mirror", position);
            //}
            //else {
            int Type = filterInfos.get(position).getFilterType();
            FilterLayoutUtilsYasa.this.position = position;
            Constant.isFilterApllying = filterType!=0;

            if (filterType==0 && Constant.isMirrorApllying) {mMagicDisplay.setMEffect2(CameraEngine.current_mirror_type);}
            else if (filterType==0 && !Constant.isMirrorApllying) {mMagicDisplay.setFilter(filterType);}
            else { mMagicDisplay.setFilter(filterType); }
                mFilterType = filterType;
                /*if(position != 0)
                    btn_Favourite.setVisibility(View.VISIBLE);
                else
                    btn_Favourite.setVisibility(View.INVISIBLE);*/
                //btn_Favourite.setSelected(filterInfos.get(position).isFavourite());
                if (position <= favouriteFilterInfos.size()) {
                    for (int i = favouriteFilterInfos.size() + 2; i < filterInfos.size(); i++) {
                        if (filterInfos.get(i).getFilterType() == Type) {
                            filterInfos.get(i).setSelected(true);
                            mAdapter.setLastSelected(i);
                            FilterLayoutUtilsYasa.this.position = i;
                            mAdapter.notifyItemChanged(i);
                        } else if (filterInfos.get(i).isSelected()) {
                            filterInfos.get(i).setSelected(false);
                            mAdapter.notifyItemChanged(i);
                        }
                    }
                }
                for (int i = 1; i < favouriteFilterInfos.size() + 1; i++) {
                    if (filterInfos.get(i).getFilterType() == Type) {
                        filterInfos.get(i).setSelected(true);
                        mAdapter.notifyItemChanged(i);
                    } else if (filterInfos.get(i).isSelected()) {
                        filterInfos.get(i).setSelected(false);
                        mAdapter.notifyItemChanged(i);
                    }
                }
            callback.onMirrorCallback("filter", position, Type);
            //}
        }

    };

    private void initFilterInfos(){
        filterInfos = new ArrayList<FilterInfo>();
        //add original
        FilterInfo filterInfo = new FilterInfo();
        filterInfo.setFilterType(MagicFilterType.NONE);
        //filterInfo.setSelected(true);
        filterInfo.setSelected(false);
        filterInfos.add(filterInfo);

        //add Favourite
        loadFavourite();
        /*for(int i = 0;i < favouriteFilterInfos.size(); i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(favouriteFilterInfos.get(i).getFilterType());
            filterInfo.setFavourite(true);
            filterInfos.add(filterInfo);
        }*/
        //add Divider
        filterInfo = new FilterInfo();
        filterInfo.setFilterType(-1);
        filterInfos.add(filterInfo);

        //addAll
        for(int i = 1;i < MagicFilterType.FILTER_COUNT; i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(MagicFilterType.NONE + i);

            //Added by Vitali
            if (MagicFilterType.NONE + i == MagicFilterType.WALDEN )
                filterInfo.setSelected(true);
            /*Changed by Vitali*/
            /*for(int j = 0;j < favouriteFilterInfos.size(); j++){
                if(MagicFilterType.NONE + i == favouriteFilterInfos.get(j).getFilterType()){
                    filterInfo.setFavourite(true);
                    break;
                }
            }*/

            //TO BE FIXED
            //if (i!=MagicFilterType.LOMO && i!=MagicFilterType.VALENCIA)
            filterInfos.add(filterInfo);
        }

        /*for(int i = 0;i < YasaStickers.getM_effects().length; i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(-2);
            filterInfo.setMirror_type(i);
            filterInfos.add(filterInfo);
        }*/
    }

    private void initFilterInfosWithSelected(int selected){
        filterInfos = new ArrayList<FilterInfo>();
        FilterInfo filterInfo = new FilterInfo();
        filterInfo.setFilterType(MagicFilterType.NONE);
        filterInfos.add(filterInfo);

        loadFavourite();
        filterInfo = new FilterInfo();
        filterInfo.setFilterType(-1);
        filterInfos.add(filterInfo);

        for(int i = 1;i < MagicFilterType.FILTER_COUNT; i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(MagicFilterType.NONE + i);
            for(int j = 0;j < favouriteFilterInfos.size(); j++){
                if(MagicFilterType.NONE + i == favouriteFilterInfos.get(j).getFilterType()){
                    filterInfo.setFavourite(true);
                    break;
                }
            }
            if (i==selected) filterInfo.setSelected(true);
            //else filterInfo.setSelected(false);
            //else {filterInfo.setSelected(false);}
            //TO BE FIXED
            //if (i!=MagicFilterType.LOMO && i!=MagicFilterType.VALENCIA)
                filterInfos.add(filterInfo);
        }

        /*for(int i = 0;i < YasaStickers.getM_effects().length; i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(-2);
            filterInfo.setMirror_type(i);
            filterInfos.add(filterInfo);
        }*/
    }

    private View.OnClickListener btn_Favourite_listener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if(position != 0 && filterInfos.get(position).getFilterType() != -1){
                int Type = filterInfos.get(position).getFilterType();
                if(filterInfos.get(position).isFavourite()){
                    //ȡ��Favourite------------------------------------
                    //btn_Favourite.setSelected(false);
                    filterInfos.get(position).setFavourite(false);
                    mAdapter.notifyItemChanged(position);
                    int i = 0;
                    for(i = 0; i < favouriteFilterInfos.size(); i++){
                        if(Type == favouriteFilterInfos.get(i).getFilterType()){
                            favouriteFilterInfos.remove(i);
                            filterInfos.remove(i+1);
                            mAdapter.notifyItemRemoved(i+1);
                            mAdapter.setLastSelected(mAdapter.getLastSelected() - 1);
                            break;
                        }
                    }
                    position --;
                    mAdapter.notifyItemRangeChanged(i + 1, filterInfos.size() - i - 1);
                }else{
                    //btn_Favourite.setSelected(true);
                    filterInfos.get(position).setFavourite(true);
                    mAdapter.notifyItemChanged(position);

                    FilterInfo filterInfo = new FilterInfo();
                    filterInfo.setFilterType(Type);
                    filterInfo.setSelected(true);
                    filterInfo.setFavourite(true);
                    filterInfos.add(favouriteFilterInfos.size()+1 ,filterInfo);
                    position ++;
                    mAdapter.notifyItemInserted(favouriteFilterInfos.size() + 1);
                    mAdapter.notifyItemRangeChanged(favouriteFilterInfos.size() + 1, filterInfos.size() - favouriteFilterInfos.size() - 1);
                    favouriteFilterInfos.add(filterInfo);
                    mAdapter.setLastSelected(mAdapter.getLastSelected() + 1);
                }
                saveFavourite();
            }
        }
    };

    private void loadFavourite(){
        favouriteFilterInfos = new ArrayList<FilterInfo>();
        String[] typeList = ((Activity) mContext).getSharedPreferences("favourite_filter", Activity.MODE_PRIVATE)
                .getString("favourite_filter_list", "").split(",");
        for(int i = 0; i < typeList.length && typeList[i] != ""; i++){
            FilterInfo filterInfo = new FilterInfo();
            filterInfo.setFilterType(Integer.parseInt(typeList[i]));
            filterInfo.setFavourite(true);
            favouriteFilterInfos.add(filterInfo);
        }
    }

    private void saveFavourite(){
        SharedPreferences shared = ((Activity) mContext).getSharedPreferences("favourite_filter", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.remove("favourite_filter_list");
        editor.commit();
        String str = "";
        for(int i = 0; i < favouriteFilterInfos.size(); i++){
            str += favouriteFilterInfos.get(i).getFilterType() + ",";
        }
        editor.putString("favourite_filter_list", str);
        editor.commit();
    }

    public List<FilterInfo> getFilters() {
        return filterInfos;
    }

}
