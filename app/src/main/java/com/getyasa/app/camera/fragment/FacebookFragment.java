package com.getyasa.app.camera.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getyasa.R;

/**
 * Created by Admin on 01.02.16.
 */
public class FacebookFragment extends Fragment {

    public FacebookFragment() {
        super();
    }

    public static Fragment newInstance() {
        Fragment fragment = new FacebookFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.facebook_fragment, null);
    }

}
