package com.getyasa.app.camera.ui;

import android.view.View;

public interface MyListener {
    // you can define any parameter as per your requirement
    public void callback(View view, String result);
}
