package com.getyasa.app.bitmap;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.view.View;

import com.getyasa.Blur;
import com.vasilkoff.maxycamera.common.utils.Constants;

/**
 * Created by Vasilkoff on 14.03.2016.
 */
public class BitmapProcessor {

    public BitmapProcessor() {
    }

    public Bitmap cropBitmap(View v, Bitmap b) {
        Bitmap cropped = Bitmap.createBitmap(b, (int)v.getX(), (int)v.getY(), v.getWidth(), v.getHeight());
        return cropped;
    }

    public Bitmap cropBitmap2(View v, int x, int y, Bitmap b) {
        int startX = x;
        int startY = y;
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, v.getWidth(), v.getHeight());
        return cropped;
    }

    public static Bitmap cropBitmap3(Bitmap b, int startX, int startY, int w, int h) {
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, w, h);
        return cropped;
    }

    public static Bitmap loadBitmapFromView_(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width , height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap bitmap;
        v.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public Bitmap mirrorEffect1(Bitmap b) {
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();

        m.postTranslate((bmOverlay.getWidth() / -2), 0);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(-1, 1);
        m.postTranslate(bmOverlay.getWidth() * 1.5f, 0);
        canvas.drawBitmap(b, m, paint);

        return bmOverlay;
    }

    public Bitmap mirrorEffect2(Bitmap b){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();

        m.setScale(-1, 1);
        m.postTranslate((bmOverlay.getWidth() / 2), 0);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.postTranslate((bmOverlay.getWidth() / 2), 0);
        canvas.drawBitmap(b, m, paint);

        return bmOverlay;
    }

    public Bitmap mirrorEffect3(Bitmap b){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();
        //just do it small
        m.postScale(0.5f, 0.5f);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(-1, 1);
        m.postScale(0.5f, 0.5f);
        m.postTranslate(bmOverlay.getWidth(), 0);
        //m.postScale(0.5f, 0.5f);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(1, -1);
        m.postScale(0.5f, 0.5f);
        m.postTranslate(0, bmOverlay.getHeight());
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(-1, -1);
        m.postScale(0.5f, 0.5f);
        m.postTranslate(bmOverlay.getWidth(), bmOverlay.getHeight());
        canvas.drawBitmap(b, m, paint);

        return bmOverlay;
    }

    public Bitmap mirrorEffect4(Bitmap b){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();

        m.postTranslate(0, bmOverlay.getHeight() / -2);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(1, -1);
        m.postTranslate(0, bmOverlay.getHeight() * 1.5f);
        canvas.drawBitmap(b, m, paint);

        return bmOverlay;
    }

    public Bitmap mirrorEffect5(Bitmap b, int SquareCoverTopHeight){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();

        b = cropBitmap3(b, 0, SquareCoverTopHeight/*getSquareCoverTopHeight()*/, Constants.mScreenWidth, Constants.mScreenWidth);

        Bitmap t_ = b;
        Path myPath  = addBorderPath(0,0,t_.getWidth(),t_.getHeight());
        b = new Blur().fastblur(b, 1.0f, 20);
        m.postTranslate(0, SquareCoverTopHeight/*getSquareCoverTopHeight()*/);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.postScale(0.45f, 0.45f);
        float tY = (float)b.getHeight() - (float)b.getHeight()*0.45f;
        m.postTranslate(0 + 10 / 2, tY + SquareCoverTopHeight/*getSquareCoverTopHeight()*/ - 10 / 2);
        canvas.drawBitmap(t_, m, paint);

        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(10);
        paint.setAntiAlias(true);
        m.reset();
        m.postScale(0.45f, 0.45f);
        m.postTranslate(0 + 10 / 2, tY + SquareCoverTopHeight/*getSquareCoverTopHeight()*/ - 10 / 2);
        myPath.transform(m);
        canvas.drawPath(myPath, paint);

        return  bmOverlay;
    }

    public Bitmap mirrorEffect6(Bitmap b, int SquareCoverTopHeight){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();

        b = cropBitmap3(b, 0, SquareCoverTopHeight/*getSquareCoverTopHeight()*/, Constants.mScreenWidth, Constants.mScreenWidth);
        /*canvas.drawBitmap(b, m, paint);*/

        int frmW = b.getWidth();
        int frmH = b.getHeight()/3*2;
        int frmX = 0;
        int frmY = (b.getHeight() - frmH)/2;
        Bitmap t_ = cropBitmap3(b,frmX,frmY,frmW, frmH);
        Path myPath  = addBorderPath(frmX,frmY,frmW,frmH);

        b = new Blur().fastblur(b, 1.0f, 20);
        m.postTranslate(0, SquareCoverTopHeight/*getSquareCoverTopHeight()*/);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.postTranslate(frmX, frmY + SquareCoverTopHeight/*getSquareCoverTopHeight()*/);
        m.postScale(0.8f, 0.8f, b.getWidth() / 2, b.getHeight() / 2);
        canvas.drawBitmap(t_, m, paint);

        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(10);
        paint.setAntiAlias(true);
        m.reset();
        m.postTranslate(0, SquareCoverTopHeight/*getSquareCoverTopHeight()*/);
        m.postScale(0.8f, 0.8f, b.getWidth() / 2, b.getHeight() / 2);
        myPath.transform(m);
        canvas.drawPath(myPath, paint);

        return bmOverlay;
    }

    public Bitmap mirrorEffect1BasicVertical(Bitmap b){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Matrix m =  new Matrix();
        m.setScale( -1 , 1 );
        m.postTranslate(bmOverlay.getWidth(), 0);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(b, m, paint);
        return bmOverlay;
    }

    public Bitmap mirrorEffect1BasicHorizontal(Bitmap b){
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Matrix m = new Matrix();
        m.setScale(1, -1);
        m.postTranslate(0, bmOverlay.getHeight());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(b, m, paint);
        return bmOverlay;
    }

    public Path addBorderPath(int x, int y, int w, int h) {
        Path border = new Path();
        border.moveTo(x, y);
        border.lineTo(x + w, y);
        border.lineTo(x + w, y + h);
        border.lineTo(x, y + h);
        border.lineTo(x, y);
        border.lineTo(x + w, y);
        return border;
    }

    public Path addCircleBorderPath(int cntrX, int cntrY, int rds, int offset) {
        Path border = new Path();
        border.addCircle(cntrX, cntrY, rds - offset, Path.Direction.CCW);
        return border;
    }

    public void connectCollagePiece(Bitmap b, Canvas canvas, int posX, int posY, int wdth, int hght) {
        //Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        //Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();
        /*System.out.println("fffffffff1 " + wdth + " " + b.getWidth());
        System.out.println("fffffffff1 " + hght + " " + b.getHeight());*/
        float scX = (float)wdth / (float)b.getWidth();
        float scY = (float)hght / (float)b.getHeight();
        m.setScale(scX, scY);
        m.postTranslate(posX, posY);
        //just do it small
        //m.postScale(0.5f, 0.5f);
        canvas.drawBitmap(b, m, paint);

        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(4);
        paint.setAntiAlias(true);

        Path brdr_ = addBorderPath(posX, posY, wdth, hght);

        canvas.drawPath(brdr_, paint);

        /*m.reset();
        m.setScale(-1, 1);
        m.postScale(0.5f, 0.5f);
        m.postTranslate(bmOverlay.getWidth(), 0);
        //m.postScale(0.5f, 0.5f);
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(1, -1);
        m.postScale(0.5f, 0.5f);
        m.postTranslate(0, bmOverlay.getHeight());
        canvas.drawBitmap(b, m, paint);

        m.reset();
        m.setScale(-1, -1);
        m.postScale(0.5f, 0.5f);
        m.postTranslate(bmOverlay.getWidth(), bmOverlay.getHeight());
        canvas.drawBitmap(b, m, paint);

        //return bmOverlay;*/
    }

    public void addOutline(Canvas canvas) {
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(6);
        paint.setAntiAlias(true);

        Path brdr_ = addBorderPath(0, 0, canvas.getWidth(), canvas.getHeight());
        canvas.drawPath(brdr_, paint);
    }

    public void connectCollagePieceCircle(Bitmap b, Canvas canvas, int posX, int posY, int wdth, int hght, int rds) {
        //Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        //Canvas canvas = new Canvas(bmOverlay);
        Matrix m =  new Matrix();
        /*System.out.println("fffffffff1 " + wdth + " " + b.getWidth());
        System.out.println("fffffffff1 " + hght + " " + b.getHeight());*/
        float scX = (float)wdth / (float)b.getWidth();
        float scY = (float)hght / (float)b.getHeight();
        m.setScale(scX, scY);
        m.postTranslate(posX, posY);
        //just do it small
        //m.postScale(0.5f, 0.5f);
        Bitmap tmp = getCircularBitmap(b, 18);
        canvas.drawBitmap(tmp, m, paint);

        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(4);
        paint.setAntiAlias(true);

        Path brdr_ = addCircleBorderPath(b.getWidth()/2, b.getHeight()/2, b.getWidth()/2, 18);
        brdr_.transform(m);
        canvas.drawPath(brdr_, paint);

    }

    public static Bitmap getCircularBitmap(Bitmap bitmap, int radiusOffset) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        //final int color = R.color.red;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(r, r, r - radiusOffset, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap cropBitmapSquare(Bitmap b,/* int startX, int startY, */int w, int h) {
        int newWidth;
        int startX;
        int startY;
        if (w>h) {
            newWidth = h;
            startX = (w - h)/2;
            startY = 0;
        }
        else {
            newWidth = w;
            startX = 0;
            startY = (h - w)/2;
        }
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, newWidth, newWidth);
        return cropped;
    }

    public static Bitmap cropBitmapRectV(Bitmap b, int w, int h) {
        Point newWH = getRectVWidth(w,h,2,3);
        int newWidth = newWH.x;
        int newHeight = newWH.y;
        int startX = (w-newWidth)/2;
        int startY = (h-newHeight)/2;
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, newWidth, newHeight);
        return cropped;
    }

    public static Bitmap cropBitmapRectH(Bitmap b, int w, int h) {
        Point newWH = getRectVWidth(w,h,4,3);
        int newWidth = newWH.x;
        int newHeight = newWH.y;
        int startX = (w-newWidth)/2;
        int startY = (h-newHeight)/2;
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, newWidth, newHeight);
        return cropped;
    }


    public static Point getRectVWidth(int w, int h, int p1, int p2) {
        int width = w;
        int height = h;
        int step_ = 10;
        boolean c = true;
        while (c) {
            if (width/p1*p2 <= h) {
                c=false;
                height = width/p1*p2;
                return new Point(width, height);
            }
            else {
                width -= step_;
                if (width<step_) return new Point(w, h);
            }
        }
        return new Point(width, height);
    }

}
