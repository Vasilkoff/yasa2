package com.getyasa.collages;

import com.getyasa.R;

/**
 * Created by Vasilkoff on 07.03.2016.
 */
public class CollageManager {

    private static int currentType = 0;

    public static int getCurrentTypeId() { return currentType; }

    public static void setCurrentTypeId(int currentType) { CollageManager.currentType = currentType; }



    public static int getCurrentType() {
        return currentType;
    }

    class MyCollageItem {

    }

    public static int[] getCollage1states() {
        return collage1states;
    }

    public static int[] getCollage2states() {
        return collage2states;
    }

    public static int[] getCollage3states() {
        return collage3states;
    }

    public static int[] getCollage4states() {
        return collage4states;
    }

    public static int[] getCollage5states() {
        return collage5states;
    }

    private static int[] collage1states = {
            R.drawable.collage1_1,
            R.drawable.collage1_2,
            R.drawable.collage1_3,
            R.drawable.collage1_4,
    };
    private static int[] collage2states = {
            R.drawable.collage2_1,
            R.drawable.collage2_2,
            R.drawable.collage2_3,
            R.drawable.collage2_4,
    };
    private static int[] collage3states = {
            R.drawable.collage3_1,
            R.drawable.collage3_2,
            R.drawable.collage3_3,
            R.drawable.collage3_4
    };
    private static int[] collage4states = {
            R.drawable.collage4_1,
            R.drawable.collage4_2,
            R.drawable.collage4_3,
            R.drawable.collage4_4
    };
    private static int[] collage5states = {
            R.drawable.collage5_1,
            R.drawable.collage5_2,
            R.drawable.collage5_3,
            R.drawable.collage5_4,
            R.drawable.collage5_5,
            R.drawable.collage5_6,
            R.drawable.collage5_7,
            R.drawable.collage5_8,
            R.drawable.collage5_9
    };
}
