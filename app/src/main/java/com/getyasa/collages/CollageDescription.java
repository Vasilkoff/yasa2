package com.getyasa.collages;

import java.util.ArrayList;

/**
 * Created by Vasilkoff on 19.03.2016.
 */
public class CollageDescription {

    private ArrayList<CollageItemObject> currentCollage;
    private int width;
    private int height;
    private int type;
    /*private int[] items_width = {width/2,width/2,width/2,width/2};
    private int[] items_height = {height/2,height/2,height/2,height/2};
    private int[] items_posX = {0,0+width/2, 0,0+width/2};
    private int[] items_poxY = {0, 0, height/2, height/2};
    private int[] radius = {width/2,width/2,width/2,width/2};

    private int[] items_width2 = {width/3,width/3,width/3,width/3,width/3,width/3,width/3,width/3,width/3};
    private int[] items_height2 = {height/3,height/3,height/3,height/3,height/3,height/3,height/3,height/3,height/3};

    private int[] items_width3 = {width/4,width/4,width/4,width/4};
    private int[] items_height3 = {height/4,height/4,height/4,height/4};

    private int[] items_width4 = {width/3,width/3*2,width/3*2,width/3};
    private int[] items_height4 = {height/2,height/2,height/2,height/2};
    private int[] itemsX;
    private int[] itemsY;*/

    public CollageDescription(int w, int h, int t) {
        this.width = w;
        this.height = h;
        this.type = t;
        currentCollage = new ArrayList<>();
        initItemSize();
    }

    public void initItemSize() {
        int size;
        int w;
        int h;
        switch (type) {
            /*COLLAGE 1*/
            case 0:
                currentCollage.clear();
                size = 4;
                w = width/4;
                h = height/4;
                int[] items_posX = { (width-w)/2, (width-w)/2, (width-w)/2, (width-w)/2};
                int[] items_posY = {0, h, 2*h, 3*h};
                for (int i=0; i<size; i++) {
                    currentCollage.add( new CollageItemObject(
                            w, h,
                            items_posX[i], items_posY[i], "square" ) );
                }
                break;
            /*COLLAGE 2*/
            case 1:
                currentCollage.clear();
                size = 4;
                w = width/2;
                h = height/2;
                int radius_ = w/2;
                int[] items_posX1 = { 0, w, 0, w };
                int[] items_posY1 = { 0, 0, h, h };
                for (int i=0; i<size; i++) {
                    currentCollage.add( new CollageItemObject(
                            w, h,
                            items_posX1[i], items_posY1[i], radius_, "circle" ) );
                }
                break;
            /*COLLAGE 3*/
            case 2:
                currentCollage.clear();
                size = 9;
                w = width/3;
                h = height/3;
                int[] items_posX2 = { 0,  w,  2*w,  0,  w,  2*w,  0,   w,   2*w};
                int[] items_posY2 = { 0,  0,  0,    h,  h,  h,    2*h, 2*h, 2*h};
                for (int i=0; i<size; i++) {
                    currentCollage.add( new CollageItemObject(
                            w, h,
                            items_posX2[i], items_posY2[i], "square" ) );
                }
                break;
            /*COLLAGE 4*/
            case 3:
                currentCollage.clear();
                size = 4;
                int[] items_width3 = {width/3,width/3*2,width/3*2,width/3};
                int[] items_height3 = {height/2,height/2,height/2,height/2};
                String[] types = {"rectV", "rectH", "rectH", "rectV"};
                int[] items_posX3 = { 0, width/3, 0, width/3*2 };
                int[] items_posY3 = { 0, 0, height/2, height/2 };
                for (int i=0; i<size; i++) {
                    currentCollage.add( new CollageItemObject(
                            items_width3[i], items_height3[i],
                            items_posX3[i], items_posY3[i], types[i] ) );
                }
                break;
            /*COLLAGE 5*/
            case 4:
                currentCollage.clear();
                size = 4;
                w = width/2;
                h = height/2;
                int[] items_posX4 = { 0, w, 0, w };
                int[] items_posY4 = { 0, 0, h, h };
                for (int i=0; i<size; i++) {
                    currentCollage.add( new CollageItemObject(
                            w, h,
                            items_posX4[i], items_posY4[i], "square" ) );
                }
                break;
            default:
                /*COLLAGE 1 By Default*/
                currentCollage.clear();
                size = 4;
                w = width/4;
                h = height/4;
                int[] items_posXd = { (width-w)/2, (width-w)/2, (width-w)/2, (width-w)/2};
                int[] items_posYd = {0, h, 2*h, 3*h};
                for (int i=0; i<size; i++) {
                    currentCollage.add( new CollageItemObject(
                            w, h,
                            items_posXd[i], items_posYd[i], "square" ) );
                }
        }
    }

    public ArrayList<CollageItemObject> getCurrentCollage() {
        return currentCollage;
    }
}
