package com.getyasa.collages;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.ImageView;

import com.getyasa.R;
import com.getyasa.app.bitmap.BitmapProcessor;

import java.util.ArrayList;

/**
 * Created by Vasilkoff on 19.03.2016.
 */
public class CollageUsual {

    private int type = 0;
    //to show what state selected
    private int[] collage_items;
    private ArrayList<CollageItemObject> collage_views;
    private CollageDescription desc;
    private int currentSelected;
    private ImageView collageItemInUse;
    private Bitmap collage;
    private Canvas c;
    private boolean isFilled = false;
    private BitmapProcessor myProcessor;

    public CollageUsual(int w_, int h_, int t, ImageView view) {
        this.type = t;
        initCollageItems();
        desc = new CollageDescription(w_,h_,t);
        collage_views = desc.getCurrentCollage();
        currentSelected = 0;
        collageItemInUse = view;
        myProcessor = new BitmapProcessor();
        collageItemInUse.setImageResource(collage_items[currentSelected]);
        initBitmap(w_, h_);
    }

    public void initCollageItems() {
        switch (type) {
            case 0:
                collage_items = CollageManager.getCollage1states();
                break;
            case 1:
                collage_items = CollageManager.getCollage2states();
                break;
            case 2:
                collage_items = CollageManager.getCollage5states();
                break;
            case 3:
                collage_items = CollageManager.getCollage3states();
                break;
            case 4:
                collage_items = CollageManager.getCollage4states();
                break;
            default:
                collage_items = CollageManager.getCollage1states();
        }
    }

    public void fillNext(Bitmap b) {
        if (currentSelected<collage_views.size()) {
            //draw_bitmap
            if (getCurrentItemCollage().getTypeName().equals("circle")) {
                myProcessor.connectCollagePieceCircle(
                        b,
                        c,
                        getCurrentItemCollage().getPosX(),
                        getCurrentItemCollage().getPosY(),
                        getCurrentItemCollage().getWidth(),
                        getCurrentItemCollage().getHeight(),
                        getCurrentItemCollage().getRadius());
            }
            else {
                myProcessor.connectCollagePiece(
                        b,
                        c,
                        getCurrentItemCollage().getPosX(),
                        getCurrentItemCollage().getPosY(),
                        getCurrentItemCollage().getWidth(),
                        getCurrentItemCollage().getHeight());
            }
        }
        if (currentSelected==collage_views.size()-1) {
            isFilled = true;
            myProcessor.addOutline(c);
            collageItemInUse.setImageResource(R.color.transparent);
            collageItemInUse.setImageBitmap(null);
            return;
        }
        //set_current
        currentSelected++;
        //change_icon
        collageItemInUse.setImageResource(collage_items[currentSelected]);
    }

    public boolean checkFilled() {
        return isFilled;
    }

    public Bitmap getCollage() {
        return collage;
    }

    public ArrayList<CollageItemObject> getCollage_views() {
        return collage_views;
    }

    public CollageItemObject getCurrentItemCollage() {
        return collage_views.get(currentSelected);
    }

    public void initBitmap(int width, int height) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        collage = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
        c = new Canvas(collage);
        c.drawRect(new Rect(0, 0, width, height), paint);
    }

    public void destroyCollage() {
        collage_views.clear();
        collage.recycle();
        collage = null;
        c = null;
    }
}
