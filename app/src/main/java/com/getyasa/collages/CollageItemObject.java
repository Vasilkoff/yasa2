package com.getyasa.collages;

/**
 * Created by Vasilkoff on 19.03.2016.
 */
public class CollageItemObject {

    private int width;
    private int height;
    private int posX;
    private int poxY;
    private int radius;
    private float scale;
    private String typeName;

    public CollageItemObject(int width, int height, int posX, int poxY, String tn) {
        this.width = width;
        this.height = height;
        this.posX = posX;
        this.poxY = poxY;
        this.typeName = tn;
        this.scale = 0.0f;
    }

    public CollageItemObject(int width, int height, int posX, int poxY, int radius, String tn) {
        this.width = width;
        this.height = height;
        this.posX = posX;
        this.poxY = poxY;
        this.radius = radius;
        this.typeName = tn;
        this.scale = 0.0f;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return poxY;
    }

    public void setPoxY(int poxY) {
        this.poxY = poxY;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public float getScaleVerticalRect() {
        return scale;
    }

    public void setScaleVerticalRect(float scale) {
        this.scale = scale;
    }

}
