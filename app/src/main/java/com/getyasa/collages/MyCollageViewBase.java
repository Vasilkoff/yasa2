package com.getyasa.collages;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Vasilkoff on 07.03.2016.
 */
public class MyCollageViewBase {

    private Context activity;

    public MyCollageViewBase(Context con) {
        activity = con;
    }

    public void initCollage() {

    };

    public Context/*Activity*/ getMyActivity() {
        return activity;
    }

    public static interface CollageItemPressedCallback {
        void onCollageItemPressed(String t, int position);
    }
}
