package com.getyasa.collages;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;

/**
 * Created by Vasilkoff on 07.03.2016.
 */
public class CollageViewItem extends CollageViewItemBase {

    private Path clipPath;
    private Paint mpaint;

    public boolean getFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    private boolean filled = false;
    private boolean isPreview = true;
    private Paint stroke_paint;

    public CollageViewItem(Context context) {
        super(context);
        this.setDrawingCacheEnabled(true);
    }

    public CollageViewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setDrawingCacheEnabled(true);
    }

    public CollageViewItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setDrawingCacheEnabled(true);
    }

    public Bitmap getMyBitmap() {
        return myBitmap;
    }

    public void setMyBitmap(Bitmap myBitmap) {
        this.myBitmap = myBitmap;
    }

    public boolean isDrawCircleMode() {
        return drawCircleMode;
    }

    public void setDrawCircleMode(boolean drawCircleMode) {
        this.drawCircleMode = drawCircleMode;
    }

    private boolean drawCircleMode = false;
    private Bitmap myBitmap;
    public void initCircle(int w, /*int h,*/ int r) {
        initPath();
        initPaint();
        initPaintR();
        initPaintStroke();
        float radius = w/2;
        float centerX = w/2;
        float centerY = w/2;
        clipPath.addCircle(centerX, centerY, radius, Path.Direction.CW);
    }

    public void initCircle2(int w, /*int h,*/ int r) {
        initPath();
        initPaintR();
        initPaintStroke();
        float radius = r;
        float centerX = w/2;
        float centerY = w/2;
        clipPath.addCircle(centerX, centerY, radius, Path.Direction.CW);
    }

    public void initCircleWithRadius(int w, int r, boolean check) {
        initPath();
        if (check) initPaint();
        else initPaintR();
        initPaintStroke();
        float radius = r/2;
        float centerX = w/2;
        float centerY = w/2;
        clipPath.addCircle(centerX, centerY, radius, Path.Direction.CW);
    }

    public void initSquare(int w, int h) {
        initPath();
        initPaint();
        initPaintStroke();
        float lrg = w;
        clipPath.moveTo(0, 0);
        clipPath.lineTo(lrg, 0);
        clipPath.lineTo(lrg, lrg);
        clipPath.lineTo(0, lrg);
        clipPath.lineTo(0, 0);
    }

    public void initSquare2(int w, int h) {
        initPath();
        initPaint();
        initPaintStroke();
        clipPath.moveTo(0, 0);
        clipPath.lineTo(w, 0);
        clipPath.lineTo(w, h);
        clipPath.lineTo(0, h);
        clipPath.lineTo(0, 0);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (/*drawCircleMode*/true) {
            //canvas.drawBitmap(myBitmap,0,0,null);
        }
        if (isPreview && clipPath!=null) {
            if (drawCircleMode) canvas.drawPath(clipPath, mpaint2);
            else canvas.drawPath(clipPath, mpaint);
            canvas.clipPath(clipPath);
            canvas.drawPath(clipPath, stroke_paint);
        }
        else if (!isPreview) {
            canvas.clipPath(clipPath);
            if (myBitmap!=null && !drawCircleMode) canvas.drawBitmap(myBitmap,0, 0, null);
            canvas.drawPath(clipPath, stroke_paint);
        }
        super.onDraw(canvas);
    }

    public void initPaint(){
        if (mpaint!=null) mpaint.reset();
        mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setColor(Color.WHITE);
        mpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    private Paint mpaint2;
    public void initPaintR(){
        if (mpaint2!=null) mpaint2.reset();
        mpaint2 = new Paint();
        mpaint2.setAntiAlias(true);
        //mpaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mpaint2.setColor(Color.BLACK);
        mpaint2.setStyle(Paint.Style.FILL);
        //mpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    public void initPaintStroke() {
        if (stroke_paint !=null) stroke_paint.reset();
        //stroke_paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        stroke_paint = new Paint();
        stroke_paint.setAntiAlias(true);
        //stroke_paint.setStrokeJoin(Paint.Join.MITER);
        stroke_paint.setColor(Color.WHITE); // Change the boundary color
        stroke_paint.setStrokeWidth(15);
        stroke_paint.setStyle(Paint.Style.STROKE);
        //stroke_paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    public void initPath() {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
    }

    public boolean getPreviewMode() { return isPreview; }

    public void setPreviewMode(boolean isCamera) { this.isPreview = isCamera; }
}
