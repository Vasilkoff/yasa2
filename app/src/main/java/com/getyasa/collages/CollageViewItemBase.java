package com.getyasa.collages;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Vasilkoff on 07.03.2016.
 */
public class CollageViewItemBase extends ImageView {

    public CollageViewItemBase(Context context) {
        super(context);
        this.setDrawingCacheEnabled(true);
    }

    public CollageViewItemBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setDrawingCacheEnabled(true);
    }

    public CollageViewItemBase(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setDrawingCacheEnabled(true);
    }

    /*public void initPaint(){
        if (mpaint!=null) mpaint.reset();
        mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setColor(Color.WHITE);
        mpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    public void initPath() {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
    }*/

    @Override
    protected void onDraw(Canvas canvas) {
        /*if (clipPath!=null) {
            canvas.drawPath(clipPath, mpaint);
            canvas.clipPath(clipPath);}*/
        super.onDraw(canvas);
    }

}
