package com.getyasa;

import android.os.Environment;


public class YasaConstants {

    public static final String APP_DIR                    = Environment.getExternalStorageDirectory() + "/YASA";
    public static final String APP_TEMP                   = APP_DIR + "/temp";
    public static final String APP_IMAGE                  = APP_DIR + "/image";

    public static final int    POST_TYPE_POI              = 1;
    public static final int    POST_TYPE_TAG              = 0;
    public static final int    POST_TYPE_DEFAULT		  = 0;


    public static final float  DEFAULT_PIXEL              = 1024;
    public static final String PARAM_MAX_SIZE             = "PARAM_MAX_SIZE";
    public static final String PARAM_EDIT_TEXT            = "PARAM_EDIT_TEXT";
    public static final int    ACTION_EDIT_LABEL          = 8080;
    public static final int    ACTION_EDIT_LABEL_POI      = 9090;

    public static final String FEED_INFO                  = "FEED_INFO";

    public static final int PHOTO_SIZE = 1024;

    public static final int REQUEST_CROP = 6709;
    public static final int REQUEST_PICK = 9162;
    public static final int REQUEST_SHARE = 13332;
    public static final int RESULT_ERROR = 404;

    public static final String SKU_WOMANS_DAY_STICKERS = "stickers_womans_day";
    public static final String SKU_VALENTINE_DAY_STICKERS = "stickers_love";
    public static final String SKU_VINTAGE_STICKERS = "stickers_vintage_style";
    public static final String SKU_HAIRSTYLE_STICKERS = "stickers_hairstyles";

    public static final String MUSTACHE_STICKERS = "stickers_mustache";
    public static final String LOVE_STICKERS = "stickers_loveU";
    public static final String LIPS_STICKERS = "stickers_lips";
    public static final String HAIRSTYLE2_STICKERS = "stickers_hairstyles2";

    public static final String NEW_YEAR_STICKERS = "stickers_new_year";
    public static final String HAT_STICKERS = "stickers_hat";
    public static final String RIBON_STICKERS = "stickers_ribbon";

    public static final String STICKERS_WOMAN_DAY2 = "stickers_8marta";
    public static final String STICKERS_WMARK = "water_mark";
    public static final String _STICKERS_GLASSES = "stickers_glasses";

    public static final String SHAPES = "shapes";

    public static final String SKU_PHOTO_SHAPE = "stickers_photo";
    public static final String SKU_COLLAGE_CIRCULAR = "collage_circles";

    public static final String CLIENT_ID = "6e64ffdcde0d42b69073e44224fa87bf";
    public static final String CLIENT_SECRET = "b169c2a9d3a540ee9b4b8115015f4b18";
    public static final String CALLBACK_URL = "https://vk.com";

    public static final int REQUEST_PERMISSION_CAMERA = 10001;
    public static final int REQUEST_PERMISSION_SAVE = 10002;
    public static final int REQUEST_PERMISSION_SAVE_BEFORE_SHARE = 10003;

}
