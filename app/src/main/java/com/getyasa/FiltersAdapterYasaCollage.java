package com.getyasa;

import android.content.Context;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Vasilkoff on 22.02.2016.
 */
public class FiltersAdapterYasaCollage extends RecyclerView.Adapter<FiltersAdapterYasaCollage.ViewHolder>{

    private int[] mDataset;
    private boolean items_purchased;
    private AdapterCollageCallback myCallBack;
    private String type;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mImageView;
        public ImageView premiumView;
        public TextView textView;
        public View itemDivider;
        public ViewHolder(View v) {

            super(v);
            mImageView = (ImageView)v.findViewById(R.id.itemStickerImageView);
            premiumView = (ImageView)v.findViewById(R.id.premium_collage);
            textView = (TextView)v.findViewById(R.id.itemStickerTextView);
            itemDivider = v.findViewById(R.id.itemDivider);

        }
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageView iv = (ImageView)v;
            int resId = (int)iv.getTag(R.string.obj_id);
            int position = (int)iv.getTag(R.string.obj_position);
            /*Message msg = new Message();
            msg.arg1 = resId;
            msg.arg2 = position;*/
            myCallBack.onMethodCallbackCollage(type, position);
        }
    };


    // Provide a suitable constructor (depends on the kind of dataset)
    public FiltersAdapterYasaCollage(int[] resourcesDataset, boolean purchased,/*Context*/AdapterCollageCallback listener) {
        mDataset = resourcesDataset;
        this.myCallBack = (AdapterCollageCallback)listener;
        type = "";
        this.items_purchased = purchased;
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public FiltersAdapterYasaCollage(int[] resourcesDataset,
                                     AdapterCollageCallback listener,
                                     boolean purchased,
                                     String type) {
        mDataset = resourcesDataset;
        this.myCallBack = (AdapterCollageCallback)listener;
        this.type= type;
        this.items_purchased = purchased;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FiltersAdapterYasaCollage.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sticker_view_item_new, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mImageView.setImageResource(mDataset[position]);
        holder.mImageView.setTag(R.string.obj_id, mDataset[position]);
        holder.mImageView.setTag(R.string.obj_position, position);
        holder.mImageView.setOnClickListener(clickListener);
        holder.itemDivider.setVisibility(View.GONE);
        if (position==0 || position==1) {
            holder.textView.setVisibility(View.VISIBLE);
            if (position==0) holder.textView.setText(R.string.shape_sq);
            if (position==1) { holder.textView.setText(R.string.shape_re); holder.itemDivider.setVisibility(View.VISIBLE); }
        }
        else if (!items_purchased && position==3) { holder.premiumView.setVisibility(View.VISIBLE);}
        else {holder.textView.setVisibility(View.GONE);}
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static interface AdapterCollageCallback {
        void onMethodCallbackCollage(String t, int position);
    }
}
