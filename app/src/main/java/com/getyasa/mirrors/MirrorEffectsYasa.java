package com.getyasa.mirrors;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.getyasa.R;
import com.getyasa.activities.YasaStickers;
import com.getyasa.activities.camera.utils.Constant;
import com.vasilkoff.magicfilter.camera.CameraEngine;
import com.vasilkoff.magicfilter.display.MagicDisplay;
import com.vasilkoff.magicfilter.filter.helper.MagicFilterType;
import com.vasilkoff.maxycamera.common.adapter.FilterAdapter;
import com.vasilkoff.maxycamera.common.bean.FilterInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasilkoff on 18.04.2016.
 */
public class MirrorEffectsYasa {
    private Context mContext;
    private MagicDisplay mMagicDisplay;
    private FilterAdapter mAdapter;

    private int position;
    private List<FilterInfo> filterInfos;

    private int mFilterType = MagicFilterType.NONE;

    /**
     * we have to handle programmatically onClick
     */
    RecyclerView mFilterListView;
    OnNewMirrorEffectCallback callback;

    public MirrorEffectsYasa(Context context,MagicDisplay magicDisplay, OnNewMirrorEffectCallback c) {
        mContext = context;
        mMagicDisplay = magicDisplay;
        callback = c;
    }

    public void init(){

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFilterListView = (RecyclerView)((Activity) mContext).findViewById(R.id.filterRecyclerView_m);
        mFilterListView.setLayoutManager(linearLayoutManager);

        mAdapter = new FilterAdapter(mContext, 0);
        //mAdapter = new FilterAdapter(mContext);
        mFilterListView.setAdapter(mAdapter);
        initFilterInfos();
        mAdapter.setFilterInfos(filterInfos);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);
    }

    public void to_def_(){
        FilterAdapter mAdapter2 = new FilterAdapter(mContext, 0);
        mAdapter2.setFilterInfos(filterInfos);
        mAdapter2.setOnFilterChangeListener(onFilterChangeListener);
        mAdapter = mAdapter2;
        mFilterListView.swapAdapter(mAdapter, true);
        mAdapter.setOnFilterChangeListener(null);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);;
    }

    /**
     * MirrorEffect
     * */
    public static interface OnNewMirrorEffectCallback {
        void onMirrorCallback(String t, int position);
    }

    private FilterAdapter.onFilterChangeListener onFilterChangeListener = new FilterAdapter.onFilterChangeListener(){

        @Override
        public void onFilterChanged(int filterType, int position) {
            MirrorEffectsYasa.this.position = position;
            if (filterType==-2) {
                if (Constant.isFilterApllying) mMagicDisplay.setMEffect(position+1);
                else mMagicDisplay.setMEffect2(position + 1);
                CameraEngine.current_mirror_type = position + 1;
                Constant.isMirrorApllying = true;
            }
            else {
                Constant.isMirrorApllying = false;
                CameraEngine.current_mirror_type = 0;
                if (Constant.isFilterApllying) mMagicDisplay.setMEffect(0);
                else mMagicDisplay.setFilter(filterType);
            }
            mFilterType = filterType;

            callback.onMirrorCallback("mirror", (filterType==-2) ? position+1:position);
        }

    };

    private void initFilterInfos(){
        filterInfos = new ArrayList<FilterInfo>();
        //add original
        FilterInfo filterInfo = new FilterInfo();
        filterInfo.setFilterType(MagicFilterType.NONE);
        filterInfo.setSelected(true);
        filterInfos.add(filterInfo);

        //add Divider
        filterInfo = new FilterInfo();
        filterInfo.setFilterType(-1);
        filterInfos.add(filterInfo);


        for(int i = 0;i < YasaStickers.getM_effects().length; i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(-2);
            filterInfo.setMirror_type(i);
            filterInfos.add(filterInfo);
        }
    }


    public List<FilterInfo> getFilters() {
        return filterInfos;
    }

}
