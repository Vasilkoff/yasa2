package com.getyasa;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Vasilkoff on 22.02.2016.
 */
public class FiltersAdapterYasaShape extends RecyclerView.Adapter<FiltersAdapterYasaShape.ViewHolder>{

    private int[] mDataset;
    private AdapterCallback myCallBack;
    private String type;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mImageView;
        public TextView textView;
        public View itemDivider;
        public ViewHolder(View v) {

            super(v);
            mImageView = (ImageView)v.findViewById(R.id.itemStickerImageView);
            textView = (TextView)v.findViewById(R.id.itemStickerTextView);
            itemDivider = v.findViewById(R.id.itemDivider);

        }
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //
        }
    };


    // Provide a suitable constructor (depends on the kind of dataset)
    public FiltersAdapterYasaShape(int[] resourcesDataset, Context listener) {
        mDataset = resourcesDataset;
        this.myCallBack = (AdapterCallback)listener;
        type = "";
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public FiltersAdapterYasaShape(int[] resourcesDataset, Context listener,
                                   String type) {
        mDataset = resourcesDataset;
        this.myCallBack = (AdapterCallback)listener;
        this.type= type;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FiltersAdapterYasaShape.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sticker_view_item_new, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mImageView.setImageResource(mDataset[position]);
        holder.mImageView.setOnClickListener(clickListener);
        holder.itemDivider.setVisibility(View.GONE);
        if (position==0 || position==1) {
            holder.textView.setVisibility(View.VISIBLE);
            if (position==0) holder.textView.setText(R.string.shape_sq);
            if (position==1) { holder.textView.setText(R.string.shape_re); holder.itemDivider.setVisibility(View.VISIBLE); }
        }
        else {holder.textView.setVisibility(View.GONE);}
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static interface AdapterCallback {
        void onMethodCallback(String t, int position);
    }
}
