package com.getyasa.activities;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class MoveListener implements View.OnTouchListener  {

    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;
    private WindowManager.LayoutParams params;
    private WindowManager windowManager;
    private boolean inside;
    private RelativeLayout holderView;



    public MoveListener(WindowManager.LayoutParams p, WindowManager wm, RelativeLayout holder) {
        params = p;
        windowManager = wm;
        initialX = params.x;
        initialY = params.y;
        holderView = holder;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:{
                Rect myRect = new Rect();
                holderView.getHitRect(myRect);

                if (myRect.contains((int)event.getX(), (int)event.getY())) {
                    initialX = params.x;
                    initialY = params.y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();
                    inside = true;
                    return true;
                } else {
                    //
                }
            } return false;
            case MotionEvent.ACTION_UP:
                inside = false;
                return true;
            case MotionEvent.ACTION_MOVE:
                if (inside) {
                    params.x = initialX + (int) (event.getRawX() - initialTouchX);
                    params.y = initialY + (int) (event.getRawY() - initialTouchY);
                    windowManager.updateViewLayout(view, params);
                }
                return inside;
            case MotionEvent.ACTION_HOVER_MOVE:
                return false;
            case MotionEvent.ACTION_OUTSIDE: {
                params.x = initialX + (int) (event.getRawX() - initialTouchX);
                params.y = initialY + (int) (event.getRawY() - initialTouchY);
                windowManager.updateViewLayout(view, params);
                return false;
            }
        }
        return false;
    }
}