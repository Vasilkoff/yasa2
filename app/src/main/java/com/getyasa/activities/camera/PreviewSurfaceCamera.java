package com.getyasa.activities.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Vasilkoff on 20.02.2016.
 */
public class PreviewSurfaceCamera extends SurfaceView implements SurfaceHolder.Callback {

    private Context mContext;



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


    }

    public PreviewSurfaceCamera(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public PreviewSurfaceCamera(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public PreviewSurfaceCamera(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init(){
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
                               int arg3) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
