package com.getyasa.activities.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Vasilkoff on 19.03.2016.
 */
public class FaceDetectionBorder extends View {

    private Paint mPaint;

    public void setmDisplayOrientation(int mDisplayOrientation) {
        this.mDisplayOrientation = mDisplayOrientation;
        invalidate();
    }

    private int mDisplayOrientation;

    public void setmOrientation(int mOrientation) {
        this.mOrientation = mOrientation;
    }

    private int mOrientation;

    public void setFace(Rect mFace) {
        this.mFace = mFace;
        invalidate();
    }

    public void setFacing_(int facing_) {
        this.facing_ = facing_;
        invalidate();
    }

    private int facing_;
    private /*Camera.Face[]*/Rect mFace;



    private boolean isClear = false;

    public FaceDetectionBorder(Context context/*, int display_orientation, int facing*/) {

        super(context);
        initialize();
        /*mDisplayOrientation = display_orientation;
        facing_ = facing;*/
    }

    public FaceDetectionBorder(Context context, AttributeSet st) {
        super(context, st);
        initialize();
    }

    public FaceDetectionBorder(Context context, AttributeSet attrs, int defaultStyle){
        super(context, attrs, defaultStyle);
        initialize();
    }

    private void initialize() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(128);
        mPaint.setStrokeWidth(4);
        mPaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        float frame_width = 25f;
        float frame_height = 25f;
        super.onDraw(canvas);
        if (mFace != null /*&& mFaces.length > 0*/) {
            Matrix matrix = new Matrix();
            prepareMatrix(matrix, facing_, mDisplayOrientation, getWidth(), getHeight());
            canvas.save();

            //System.out.println(mDisplayOrientation + "  " + mOrientation + " " + facing_ + " fgfbgg");
            matrix.postRotate(mOrientation);
            canvas.rotate(-mOrientation);

            RectF rectF = new RectF();
            rectF.set(mFace);
            matrix.mapRect(rectF);
            frame_width = (rectF.right - rectF.left)/10f*3f;
            frame_height = (rectF.bottom - rectF.top)/10f*3f;
            //one
            canvas.drawLine(rectF.left, rectF.top, rectF.left+frame_width, rectF.top, mPaint);
            canvas.drawLine(rectF.left, rectF.top, rectF.left, rectF.top+frame_height, mPaint);
            //two
            canvas.drawLine(rectF.right-frame_width, rectF.top, rectF.right, rectF.top, mPaint);
            canvas.drawLine(rectF.right, rectF.top, rectF.right, rectF.top+frame_height, mPaint);
            //three
            canvas.drawLine(rectF.left, rectF.bottom, rectF.left, rectF.bottom-frame_height, mPaint);
            canvas.drawLine(rectF.left, rectF.bottom, rectF.left+frame_width, rectF.bottom, mPaint);
            //four
            canvas.drawLine(rectF.right-frame_width, rectF.bottom, rectF.right, rectF.bottom, mPaint);
            canvas.drawLine(rectF.right, rectF.bottom, rectF.right, rectF.bottom-frame_height, mPaint);

            //canvas.drawRect(rectF, mPaint);
            System.out.println(rectF.left + " " + rectF.top + " " + rectF.right + " " + rectF.bottom + " dfgfg " + rectF.centerX() + " " + rectF.centerY());
            /*for (Camera.Face face : mFaces) {
                rectF.set(face.rect);
                matrix.mapRect(rectF);
                canvas.drawRect(rectF, mPaint);
            }*/
            canvas.restore();
        }
        else {
            if (isClear) {canvas.drawColor(0, PorterDuff.Mode.CLEAR);}
        }
    }

    public static void prepareMatrix(Matrix matrix, int facing, int displayOrientation,
                                     int viewWidth, int viewHeight) {
        boolean mirror = (facing == Camera.CameraInfo.CAMERA_FACING_FRONT);
        //System.out.println("thisis_mirror " + mirror);
        /*matrix.setScale(mirror ? -1 : 1, 1);*/

        // Need mirror for front camera.
        //matrix.setScale(mirror ? -1 : 1, 1);

        if (!mirror)
            matrix.setScale(1, 1);
        else
            matrix.setScale(1, -1);

        // This is the value for android.hardware.Camera.setDisplayOrientation.
        matrix.postRotate(displayOrientation);
        // Camera driver coordinates range from (-1000, -1000) to (1000, 1000).
        // UI coordinates range from (0, 0) to (width, height).
        matrix.postScale(viewWidth / 2000f, viewHeight / 2000f);
        matrix.postTranslate(viewWidth / 2f, viewHeight / 2f);
    }

    public void setIsClear(boolean isClear) {
        this.isClear = isClear;
        invalidate();
    }

}
