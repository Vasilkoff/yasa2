package com.getyasa.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.net.Uri;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.appintop.init.AdToApp;
import com.appintop.interstitialads.DefaultInterstitialListener;
import com.common.util.DataUtils;
import com.customview.MyImageViewDrawableOverlay;
import com.getyasa.App;
import com.getyasa.FiltersAdapterYasa;
import com.getyasa.FiltersAdapterYasaCollage;
import com.getyasa.R;
import com.getyasa.YasaConstants;
import com.getyasa.activities.adapters.ShapesAdapterYasa;
import com.getyasa.activities.camera.FaceDetectionBorder;
import com.getyasa.activities.camera.utils.Constant;
import com.getyasa.app.bitmap.BitmapProcessor;
import com.getyasa.app.model.Addon;
import com.getyasa.base.YasaBaseActivity2;
import com.getyasa.collage.MultiTouchListener;
import com.getyasa.collages.CollageUsual;
import com.getyasa.mirrors.MirrorEffectsYasa;
import com.getyasa.shapes3d.Shapes3DYasa;
import com.getyasa.shapesview.CircleSurface;
import com.getyasa.shapesview.ShapeCameraDisplay;
import com.getyasa.stickerview.view.StickerView;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.inapppurchase.util.IabHelper;
import com.inapppurchase.util.IabResult;
import com.inapppurchase.util.Inventory;
import com.inapppurchase.util.Purchase;
import com.vasilkoff.magicfilter.camera.CameraEngine;
import com.vasilkoff.magicfilter.display.MagicCameraDisplay;
import com.vasilkoff.magicfilter.filter.helper.MagicFilterType;
import com.vasilkoff.magicfilter.utils.SaveTask;
import com.vasilkoff.magicfilter.utils.SaveTaskLight;
import com.vasilkoff.maxycamera.common.utils.Constants;
import com.vasilkoff.maxycamera.common.view.FilterLayoutUtilsYasa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import mehdi.sakout.fancybuttons.FancyButton;

public class YasaMainActivity extends YasaBaseActivity2 implements FiltersAdapterYasa.AdapterCallback,
        ShapesAdapterYasa.ShapesAdapterCallback {

    public Context appContext;
    //toolbar3
    private ImageView buttonGallery;//GALLERY
    private ImageView buttonShuffleFilter;
    private ImageView buttonShutter;
    private ImageView buttonShutter1;
    private ImageView buttonSuperYasa;
    private ImageView buttonFilters;
    private ImageView buttonGallery2;
    private ImageView buttonFilters2;
    private ImageView buttonMEffects;
    private ImageView button3DEffects;

    //WM
    private ImageView buttonWM;

    //toolbar4
    private ImageView buttonChangeCamera;
    private RelativeLayout buttonChangeCameraView;
    private ImageView buttonShapes;
    private ImageView buttonTimer;
    private ImageView buttonStickers;
    private ImageView buttonFlash;
    private RelativeLayout buttonFlashView;

    private ImageView buttonShare;//SHARE
    private ImageView buttonSave;//SAVE

    private ImageView buttonCloseFilters;
    private ImageView buttonCloseMEffects;
    private ImageView buttonClose3DEffects;

    //Timer View
    private RelativeLayout timerContent;
    private TextView timerView;
    //Timer View Related
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;
    String timerPreferenceName = "timer_state";
    int timerState;
    int[] timerStates = {   R.drawable.icn_timer,
                            R.drawable.icn_timer_2,
                            R.drawable.icn_timer_5,
                            R.drawable.icn_timer_10  };


    int flashState;
    int[] flashStates = {
            R.drawable.icn_flash,
            R.drawable.icn_flash_auto,
            R.drawable.icn_flash_off };

    //shapes toolbar
    int[] shapes ;
    RecyclerView mRecyclerView;
    RelativeLayout toolbar3shapes;

    //stickers toolbar | fills toolbar3+toolbar4 area
    RecyclerView mRecyclerViewStickers;
    RelativeLayout stickersToolbar;

    //filters toolbar
    RelativeLayout toolbarFilters;
    RelativeLayout toolbarMEffects;
    RelativeLayout toolbar3DEffects;

    RelativeLayout WaterMarkLogoView;

    SpinKitView spinKitView;
    Sprite drawable;
    View progressBarContainer;

    private boolean startShare = false;
    private boolean startPurchase = false;

    //billing
    static final String SKU_WOMANS_DAY_STICKERS = YasaConstants.SKU_WOMANS_DAY_STICKERS;
    static final String SKU_VALENTINE_DAY_STICKERS = YasaConstants.SKU_VALENTINE_DAY_STICKERS;
    static final String SKU_VINTAGE_STICKERS = YasaConstants.SKU_VINTAGE_STICKERS;
    static final String SKU_HAIRSTYLE_STICKERS = YasaConstants.SKU_HAIRSTYLE_STICKERS;
    //
    static final String SKU_MUSTACHE_STICKERS = YasaConstants.MUSTACHE_STICKERS;
    static final String SKU_LOVE_STICKERS = YasaConstants.LOVE_STICKERS;
    static final String SKU_LIPS_STICKERS = YasaConstants.LIPS_STICKERS;
    static final String SKU_HAIRSTYLE2_STICKERS = YasaConstants.HAIRSTYLE2_STICKERS;
    //
    static final String SKU_NEW_YEAR_STICKERS = YasaConstants.NEW_YEAR_STICKERS;
    static final String SKU_HAT_STICKERS = YasaConstants.HAT_STICKERS;
    static final String SKU_RIBON_STICKERS = YasaConstants.RIBON_STICKERS;

    static final String SKU_WOMAN_DAY2 = YasaConstants.STICKERS_WOMAN_DAY2;
    static final String SKU_WMARK = YasaConstants.STICKERS_WMARK;
    static final String SKU_GLASSES_STICKERS = YasaConstants._STICKERS_GLASSES;
    static final String _SKU_SHAPE = YasaConstants.SKU_PHOTO_SHAPE;//YasaConstants.SHAPES;

    /*static final String SKU_PHOTO_SHAPE = YasaConstants.SKU_PHOTO_SHAPE;*/
    static final String SKU_COLLAGE_CIRCULAR = YasaConstants.SKU_COLLAGE_CIRCULAR;

    boolean isWomenDayPremium = false;
    boolean isValentineDayPremium = false;
    boolean isHairstylesPremium = false;
    boolean isVintagePremium = false;
    boolean isWomenDayPremium2 = false;
    boolean isWMarkPremium = false;
    boolean isPhotoShapesPremium = false;
    boolean isCollageCircularPremium = false;
    static final int RC_REQUEST = 10001;
    String sticker1PreferenceName = "womans_day_sticker";
    String sticker2PreferenceName = "valentine_day_sticker";
    String sticker3PreferenceName = "vintage_day_sticker";
    String sticker4PreferenceName = "hairstyle_day_sticker";
    String sticker5PreferenceName = "woman_day2_sticker";
    String sticker6PreferenceName = "wmark_sticker";
    String sticker7PreferenceName = "photo_sticker";
    String sticker8PreferenceName = "collage_sticker";
    IabHelper mHelper;//most important shit

    private String lastSavedImage = "";
    private MyImageViewDrawableOverlay mImageView;
    //alternative stickers
    private ArrayList<View> mViews;
    private StickerView mCurrentView;
    private RelativeLayout mContentRootView;

    private GLSurfaceView glSurfaceView;
    private MagicCameraDisplay mMagicCameraDisplay;
    private boolean isShuttered=false;
    FilterLayoutUtilsYasa mFilterUtilsYasa;
    MirrorEffectsYasa mMEffectsYasa;
    Shapes3DYasa _3DEffectsYasa;

    private ImageView imageFromGaleryView;
    private boolean isLoadedFromGalery = false;

    private FontCache myFontCache;
    private boolean isPhotoShapeStarted=false;
    private boolean isPhotoShapeShuttered=false;
    private CircleSurface shapewView;
    private ShapeCameraDisplay ShapeCamera;

    private boolean isCollageStarted = false;
    private ImageView collageImageView;

    FaceDetectionBorder myborder;

    private ScaleGestureDetector mScaleDetector;
    private FancyButton buttonTakeAShot;

    private String tutorialPreferenceName = "tutorial_state";
    private boolean isTutoralWatched = false;

    RecyclerView mRecyclerViewPhotoStickers;
    RelativeLayout photoStickersToolbar;

    private int AMIMATION_TIME = 300;

    private int SURFACE_VIEW_WIDTH_3_4;
    private int SURFACE_VIEW_HEIGHT_3_4;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yasa);

        appContext = this;
        myFontCache = new FontCache();//custom font loader and keeper
        applyCustomTypefaceToCategoryTextView();

        initSurfaceViewSize();

        CameraEngine.current_mirror_type = 0;
        Constant.isMirrorApllying = false;
        Constant.isFilterApllying = true;
        Constant.is3DShapesApplying = false;
        initMagicPreview();
        Constant.BACK_CAMERA_IN_USE = checkBackCameraRunning();
        CameraEngine.BACK_CAMERA_IN_USE = Constant.BACK_CAMERA_IN_USE;
        //setSquare(0);//square frame
        WaterMarkLogoView = (RelativeLayout) findViewById(R.id.yasa_wm);
        startSquareCameraContent();

        //version1.0
        //mImageView = (MyImageViewDrawableOverlay) findViewById(R.id.myStickersOverlay);
        imageFromGaleryView = (ImageView) findViewById(R.id.imageFromGallery);
        applyNewSizeToAnyView(imageFromGaleryView);
        mViews = new ArrayList<>();
        mContentRootView = (RelativeLayout) findViewById(R.id.drawContainer3);
        applyNewSizeToAnyView(mContentRootView);

        shapewView = (CircleSurface)findViewById(R.id.surface_view2);
        /*move = (RelativeLayout)findViewById(R.id.moveMagic);
        move.setOnTouchListener(new MultiTouchListener(false,false));*/

        collageImageView = (ImageView) findViewById(R.id.col_icon);

        isWomenDayPremium = DataUtils.getBooleanPreferences(getApplicationContext(), sticker1PreferenceName);
        isValentineDayPremium = DataUtils.getBooleanPreferences(getApplicationContext(), sticker2PreferenceName);
        isVintagePremium = DataUtils.getBooleanPreferences(getApplicationContext(), sticker3PreferenceName);
        isHairstylesPremium = DataUtils.getBooleanPreferences(getApplicationContext(), sticker4PreferenceName);
        isWomenDayPremium2 = DataUtils.getBooleanPreferences(getApplicationContext(), sticker5PreferenceName);
        isWMarkPremium = DataUtils.getBooleanPreferences(getApplicationContext(), sticker6PreferenceName);
        isPhotoShapesPremium = DataUtils.getBooleanPreferences(appContext, sticker7PreferenceName);
        isCollageCircularPremium = DataUtils.getBooleanPreferences(appContext, sticker8PreferenceName);

        timerView = (TextView)findViewById(R.id.textView_timer);
        timerContent = (RelativeLayout)findViewById(R.id.timerContent);

        buttonGallery = (ImageView)findViewById(R.id.button_gallery);
        buttonGallery.setOnTouchListener(openGallery);

        buttonGallery2 = (ImageView)findViewById(R.id.button_gallery2);
        buttonGallery2.setOnTouchListener(openGallery);

        buttonSuperYasa = (ImageView)findViewById(R.id.button_super_yasa);
        buttonSuperYasa.setOnTouchListener(openGallery);

        mTimer = new Timer();
        buttonTimer = (ImageView)findViewById(R.id.button_timer);
        buttonTimer.setOnTouchListener(openGallery);
        timerState = DataUtils.getIntPreferences(this, timerPreferenceName);
        buttonTimer.setImageResource(timerStates[timerState]);

        initFlashButton(!Constant.BACK_CAMERA_IN_USE);

        buttonShutter = (ImageView) findViewById(R.id.button_shutter);
        buttonShutter.setOnTouchListener(openGallery);

        buttonShutter1 = (ImageView) findViewById(R.id.button_shutter1);
        buttonShutter1.setOnTouchListener(openGallery);

        initCameraButton(!isTwoCameras() && !Constant.BACK_CAMERA_IN_USE);

        buttonWM = (ImageView) findViewById(R.id.WM_Button);
        buttonWM.setOnTouchListener(openGallery);

        //buttonShuffleFilter = (ImageView)findViewById(R.id.button_shuffle_filter);
        //buttonShuffleFilter.setOnTouchListener(openGallery);

        shapes = YasaStickers.getShapes();
        mRecyclerView = (RecyclerView) findViewById(R.id.toolbar3recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setAdapter(new FiltersAdapterYasaCollage(
                shapes,
                myOnCollageCallback,
                isCollageCircularPremium,
                SKU_COLLAGE_CIRCULAR));

        toolbar3shapes = (RelativeLayout)findViewById(R.id.toolbar3shapes);
        buttonShapes = (ImageView)findViewById(R.id.button_shapes);
        buttonShapes.setOnTouchListener(openGallery);

        photoStickersToolbar = (RelativeLayout)findViewById(R.id.photo_stickersToolbar);
        mRecyclerViewPhotoStickers = (RecyclerView)findViewById(R.id.toolbarPhotoStickersRecyclerView);
        mRecyclerViewPhotoStickers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewPhotoStickers.setAdapter(
                new ShapesAdapterYasa(
                        YasaStickers.getShapes2(),
                        isPhotoShapesPremium,
                        this,
                        _SKU_SHAPE)
        );
        stickersToolbar = (RelativeLayout)findViewById(R.id.stickersToolbar);
        mRecyclerViewStickers = (RecyclerView)findViewById(R.id.toolbarStickersRecyclerView);
        mRecyclerViewStickers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewStickers.setAdapter(new FiltersAdapterYasa(
                        YasaStickers.getStickersMustachePreview(),
                        stickerAddHandler,
                        true,
                        this,
                        SKU_MUSTACHE_STICKERS)
                /*new ShapesAdapterYasa(
                        YasaStickers.getShapes2(),
                        true,
                        this,
                        _SKU_SHAPE)*/
        );

        buttonStickers = (ImageView)findViewById(R.id.button_stickers);
        buttonStickers.setOnTouchListener(openGallery);

        buttonShare = (ImageView)findViewById(R.id.button_share);
        buttonShare.setOnTouchListener(openGallery);

        buttonSave = (ImageView)findViewById(R.id.button_save);
        buttonSave.setOnTouchListener(openGallery);

        buttonTakeAShot = (FancyButton)findViewById(R.id.btnTakeAShot);
        buttonTakeAShot.setOnClickListener(onShareListener);
        findViewById(R.id.wellDone).setOnTouchListener(onWellDoneClick);

        isTutoralWatched = DataUtils.getBooleanPreferences(this, tutorialPreferenceName);
        initTutorial();

        initFaceDetectionView();

        initFilterToolbar();
        initMEffectsToolbar();
        init3DShapesToolabr();

        spinKitView = (SpinKitView)findViewById(R.id.spin_kit);
        drawable = new CubeGrid();
        progressBarContainer = findViewById(R.id.pb_container);

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvLATunlVMXQD6BpSn/0JTLhM47E8X+/jcvWwzcez3B12e1DuHcDH3WJN143ncAaTbdVvRc82FlHy1vr0r9xRoZ2b12TueX8R0e9l4hmq5N8C2miSmdtM3E1BZIHrrJmDZBwOIsY6+3XZZ8FZ5Oe3gpXRQZD7ZGIu+xL5hwXT1+RtcJe3WOJv4ixFobjdi+uy8G/l4IVi/bkqXfZbMc/a4nArnRMxi79a694XHajXN2KN6S8YmNac4b3tObbE2VAqrQyUYAyvzyRCdY8x/+JindtvVSllNSSnQK+1Em6KGjvuDRGyiX9a5nCTuNDjXGKnH/6Kwc5CLOL9SE01CW00vQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(false);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    toast("Problem setting up in-app billing: " + result, 1);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                //skip it, save to device is enough
                //mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });

        initAdAndListener();
        /*startWaldenFilter();
        startFaceProcessing();*/
        mTracker = App.getApp().getDefaultTracker();
    }

    @Override
    protected void onResume() {
        //System.out.println("activity_onresume");
        super.onResume();
        hideOnPauseScreen();
        if (!startShare && !startPurchase) {
            if ( checkCameraStartConditions() ) {

                if (mMagicCameraDisplay != null) {
                    mMagicCameraDisplay.onResume();
                    startFaceProcessing(1);
                    //toast("FACEDETECTION_RESUME", 0);
                }
            }
        }
        else if (startShare) startShare = false;
        else if (startPurchase) startPurchase = false;
    }

    private boolean isVideoAdsReady=false;
    private boolean isVideoAdsStarted=false;
    private boolean isVideoAdsClosed=false;
    public void initAdAndListener() {

        initAd(0);
        AdToApp.setInterstitialListener(new DefaultInterstitialListener() {
            @Override
            public void onInterstitialStarted(String adType, String provider) {
                //
                isVideoAdsStarted = true;
                //toast("start",0);
            }

            @Override
            public void onInterstitialClosed(String adType, String provider) {
                //
                isVideoAdsClosed = true;
                //toast("closd",0);
                if (premiumItemType == 2)
                    addPremiumItem(premiumItemType, premiumItemStickerType, premiumItemPosition);
                else if (premiumItemType == 12) {
                    buttonFlash.setImageResource(flashStates[2]);
                } else addPremiumItem(premiumItemType, 0, premiumItemPosition);
            }

            @Override
            public void onInterstitialClicked(String adType, String provider) {
                //
            }

            @Override
            public void onFirstInterstitialLoad(String adType, String provider) {
                //
                //df();
                isVideoAdsReady = true;
            }

            @Override
            public boolean onInterstitialFailedToShow(String adType) {
                //
                toast( getString(R.string.ads_not_available) , 0);
                return false;
            }
        });
    }
    public void sb1(){
        buttonChangeCameraView.setVisibility(View.GONE);
        buttonFlashView.setVisibility(View.GONE);

        buttonMEffects.setVisibility(View.GONE);
        button3DEffects.setVisibility(View.GONE);

        buttonSave.setVisibility(View.VISIBLE);
        buttonShare.setVisibility(View.VISIBLE);

        findViewById(R.id.toolbar3actions).setVisibility(View.VISIBLE);
        findViewById(R.id.toolbar3actions1).setVisibility(View.GONE);
    }
    public void sb2(){
        buttonChangeCameraView.setVisibility(View.VISIBLE);
        if (Constant.BACK_CAMERA_IN_USE) buttonFlashView.setVisibility(View.VISIBLE);

        buttonMEffects.setVisibility(View.VISIBLE);
        button3DEffects.setVisibility(View.VISIBLE);

        buttonSave.setVisibility(View.GONE);
        buttonShare.setVisibility(View.GONE);

        findViewById(R.id.toolbar3actions).setVisibility(View.GONE);
        findViewById(R.id.toolbar3actions1).setVisibility(View.VISIBLE);
    }
    public void showOnShutter(boolean shuttered) {
        if (shuttered) {
            findViewById(R.id.toolbar4_1).setVisibility(View.VISIBLE);
            findViewById(R.id.toolbar4).setVisibility(View.GONE);
            sb1();
        }
        else {
            findViewById(R.id.toolbar4).setVisibility(View.VISIBLE);
            findViewById(R.id.toolbar4_1).setVisibility(View.GONE);
            sb2();
        }
    }

    private void initMagicPreview(){
        /*GLSurfaceView*/ glSurfaceView = (GLSurfaceView)findViewById(R.id.glsurfaceviewYasa);
        //RelativeLayout.LayoutParams params = new LayoutParams(Constants.mScreenWidth, Constants.mScreenHeight);
        RelativeLayout.LayoutParams params = new LayoutParams(SURFACE_VIEW_WIDTH_3_4, SURFACE_VIEW_HEIGHT_3_4);
        glSurfaceView.setLayoutParams(params);
        mMagicCameraDisplay = new MagicCameraDisplay(this, glSurfaceView);

        initScaleDetector();
        glSurfaceView.setOnTouchListener(onZoomListener);
        glSurfaceView.setOnLongClickListener(onCameraLongPressed);

        //APPLY WALDEN FILTER
        startWaldenFilter();
        //BEGIN FACE DETECTION AND AUTOFOCUS
        startFaceProcessing(0);
        //toast("FACEDETECTION_INIT",0);
    }

    public void initSurfaceViewSize() {
        /*toast(Constants.mScreenSoftButtonsHeight + "", 1);*/
        int toolbar_DP_size = 138;
        int toolbar_height = App.getApp().dp2px( toolbar_DP_size );
        int toolbar_new_height = toolbar_height;
        SURFACE_VIEW_WIDTH_3_4 = Constants.mScreenWidth;
        SURFACE_VIEW_HEIGHT_3_4 = Constants.mScreenWidth/3*4;

        if (SURFACE_VIEW_HEIGHT_3_4 < (Constants.mScreenHeight-toolbar_height-Constants.mScreenSoftButtonsHeight) ) {
            toolbar_new_height = Constants.mScreenHeight - SURFACE_VIEW_HEIGHT_3_4-Constants.mScreenSoftButtonsHeight;
            if (toolbar_new_height > toolbar_height) {
                View v = findViewById(R.id.toolbar_background);
                applyNewSizeToToolbarBackground(v, toolbar_new_height);
            }
        }
        /*toast(SURFACE_VIEW_WIDTH_3_4+" "+SURFACE_VIEW_HEIGHT_3_4,1);*/
    }

    public void applyNewSizeToAnyView(View v) {
        RelativeLayout.LayoutParams params = new LayoutParams(SURFACE_VIEW_WIDTH_3_4, SURFACE_VIEW_HEIGHT_3_4);
        v.setLayoutParams(params);
    }

    public void applyNewSizeToToolbarBackground(View v, int width) {
        RelativeLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, width);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        v.setLayoutParams(params);
    }

    float scaleNumber = 0;
    int maxZoom = 10;
    int scaleSpeed = 5;
    private boolean isScaling = false;
    public void initScaleDetector() {
        mScaleDetector = new ScaleGestureDetector(appContext, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                isScaling = false;

            }
            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                isScaling = true;
                return true;
            }
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                //System.out.println("myScale " + detector.getScaleFactor());
                if (detector.getScaleFactor() >= 1) { scaleNumber += ( detector.getScaleFactor()/scaleSpeed ); }
                else { scaleNumber -= ( detector.getScaleFactor()/scaleSpeed ); }
                applyScale();
                return false;
            }
        });
    }
    private View.OnTouchListener onZoomListener = new View.OnTouchListener(){
        @Override
        public boolean onTouch(View arg0, MotionEvent arg1) {
            mScaleDetector.onTouchEvent(arg1);
            return /*false*/true;
        }

    };

    private View.OnLongClickListener onCameraLongPressed = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (!isScaling) {
                //toast("PRESSED", 0);
                /*CameraEngine.cancelAutoFocus();
                CameraEngine.setAutoFocus();
                CameraEngine.startAutoFocus();*/

                /*if (checkFaceDetectionAvailable()) {
                    if (!isFaceDetecting) { initFaceDetection(); isFaceDetecting = true; }
                    else { pauseFaceDetection(0); isFaceDetecting = false; }
                }*/
            }
            return false;
        }
    };

    public void initFaceDetectionView() {
        myborder = (FaceDetectionBorder)findViewById(R.id.fdbrdr);
        applyNewSizeToAnyView(myborder);
    }
    private boolean isFaceDetecting = false;
    public void initFaceDetection() {
        isFaceDetecting = true;
        myborder.setVisibility(View.VISIBLE);
        myborder.setmDisplayOrientation(mMagicCameraDisplay.getCameraDisplayOrientation(this));
        myborder.setmOrientation(CameraEngine.getOrientation());
        myborder.setFacing_(mMagicCameraDisplay.getCameraFacing());
        myborder.setIsClear(false);
        CameraEngine.setFaceDetectionListener(onFaceListener);
        CameraEngine.startFaceDetection();
    }
    public void pauseFaceDetection(int stop_detection) {
        isFaceDetecting = false;
        if (myborder!=null) {
            myborder.setFace(null);
            myborder.setIsClear(true);
            myborder.setVisibility(View.GONE);
            if (stop_detection == 0) {
                CameraEngine.stopFaceDetection();
            }
        }
    }

    public boolean checkFaceDetectionAvailable() {
        return CameraEngine.checkFaceDetection();
    }
    private Camera.FaceDetectionListener onFaceListener = new Camera.FaceDetectionListener() {

        @Override
        public void onFaceDetection(Camera.Face[] faces, Camera camera) {
            if (faces.length>0 && !Constant.isMirrorApllying && !Constant.is3DShapesApplying) {
                myborder.setFace(faces[0].rect);
                myborder.setIsClear(false);
            }
            else {
                myborder.setFace(null);
                myborder.setIsClear(true);
            }
        }
    };

    public void hideFaceDetectionBorder(){if (myborder!=null) myborder.setVisibility(View.GONE);}

    /*public void restartFaceDetection() {
        toast("yes",1);
        //CameraEngine.stopFaceDetection();
        initFaceDetection();
    }*/

    public void applyScale() {
        if (scaleNumber<0) scaleNumber = 0;
        else if (scaleNumber>maxZoom) scaleNumber= maxZoom;
        CameraEngine.addZoom( Math.round(scaleNumber) );
    }


    private void initFilterToolbar(){
        //YasaToolbars = (RelativeLayout)findViewById(R.id.yasaToolbars);
        buttonFilters = (ImageView)findViewById(R.id.button_filters);
        buttonFilters2 = (ImageView)findViewById(R.id.button_filters2);

        buttonCloseFilters = (ImageView)findViewById(R.id.button_close_filter);
        toolbarFilters = (RelativeLayout)findViewById(R.id.yasa_filters_toolbar);

        buttonFilters.setOnClickListener(startFiltersListener);
        buttonFilters2.setOnClickListener(startFiltersListener);

        buttonCloseFilters.setOnClickListener(closeFiltersListener);

        /*FilterLayoutUtilsYasa*/
        mFilterUtilsYasa = new FilterLayoutUtilsYasa(this, mMagicCameraDisplay, onSelectFilter_CB);
        mFilterUtilsYasa.init();
    }

    private void initMEffectsToolbar(){
        buttonMEffects = (ImageView)findViewById(R.id.button_mEffects);
        buttonCloseMEffects = (ImageView)findViewById(R.id.button_close_filter_m);
        toolbarMEffects = (RelativeLayout)findViewById(R.id.yasa_mirror_toolbar);

        buttonMEffects.setOnClickListener(startMEffectListener);
        buttonCloseMEffects.setOnClickListener(closeMEffectListener);

        mMEffectsYasa = new MirrorEffectsYasa(this, mMagicCameraDisplay, onmirror_cb);
        mMEffectsYasa.init();
    }

    private void init3DShapesToolabr(){
        button3DEffects = (ImageView)findViewById(R.id.button_3dshapes);
        buttonClose3DEffects = (ImageView)findViewById(R.id.button_close_filter_3dshape);
        toolbar3DEffects = (RelativeLayout)findViewById(R.id.yasa_3dshapes_toolbar);

        button3DEffects.setOnClickListener(start3DEffectListener);
        buttonClose3DEffects.setOnClickListener(close3DEffectListener);

        _3DEffectsYasa = new Shapes3DYasa(this, mMagicCameraDisplay, cb_shapes);
        _3DEffectsYasa.init();
    }

    private void initCameraButton(boolean isInit) {
        buttonChangeCamera = (ImageView)findViewById(R.id.button_change_camera);
        buttonChangeCameraView = (RelativeLayout)findViewById(R.id.button_change_cameraView);
        buttonChangeCameraView.setOnTouchListener(openGallery);
        if (isInit) buttonChangeCameraView.setVisibility(View.GONE);
    }

    private void initFlashButton(boolean isInit) {
        buttonFlash = (ImageView)findViewById(R.id.button_flash);
        buttonFlashView = (RelativeLayout)findViewById(R.id.button_flashView);
        buttonFlashView.setOnTouchListener(openGallery);
        flashState = 2;//DataUtils.getIntPreferences(this, flashPreferenceName);
        buttonFlash.setImageResource(flashStates[flashState]);
        if (isInit) buttonFlashView.setVisibility(View.GONE);
    }

    private int CUMirrorType = 0;
    MirrorEffectsYasa.OnNewMirrorEffectCallback onmirror_cb = new MirrorEffectsYasa.OnNewMirrorEffectCallback() {
        @Override
        public void onMirrorCallback(String t, int position) {
            //toast(t + " " + position, 1);
            CUMirrorType = position;
            if (position>0) {
                //
            }
            else {
                //startWaldenFilter();
                //mMagicCameraDisplay.setFilter(CUrrentFilterType);
            }
            //hideAnyBarWithAnimation(toolbarMEffects,300);
        }
    };


    private int _3DShapeType = 0;
    Shapes3DYasa.OnShape3DSelectedCallback cb_shapes = new Shapes3DYasa.OnShape3DSelectedCallback() {
        @Override
        public void onShapes3DCallback(String t, int position) {
            _3DShapeType = position;
            if (position>0) {
                lock_Filter_Mirror2(true);
            }
            else {
                //mMagicCameraDisplay.setFilter(CUrrentFilterType);
                reset_Filter_Mirror();
                lock_Filter_Mirror2(false);
            }
        }
    };

    public void lock_Filter_Mirror2(boolean isEnable) {
        float trnsprnt = 0.23f;
        float dflt = 1.0f;
        if (isEnable) {
            if (buttonFilters.getAlpha()!=trnsprnt) buttonFilters.setAlpha(trnsprnt);
            if (buttonMEffects.getAlpha()!=trnsprnt) buttonMEffects.setAlpha(trnsprnt);
        }
        else {
            if (buttonFilters.getAlpha()!=dflt) buttonFilters.setAlpha(dflt);
            if (buttonMEffects.getAlpha()!=dflt) buttonMEffects.setAlpha(dflt);
        }
    }

    public void _Skip_Mirror_when_Photo_is_Taken() {
        if (Constant.isFilterApllying && Constant.isMirrorApllying) {
            mMagicCameraDisplay.setMEffect(0);//there is no mirror when photo is taken
            /*mMagicCameraDisplay.setFilter(CUrrentFilterType);*/
        }
        else if (Constant.isMirrorApllying) {mMagicCameraDisplay.setMEffect2(0);}
    }

    public void reset_Filter_Mirror() {
        if (Constant.isFilterApllying && Constant.isMirrorApllying) {mMagicCameraDisplay.setFilter(CUrrentFilterType);}
        else if (Constant.isFilterApllying) {mMagicCameraDisplay.setFilter(CUrrentFilterType);}
        else if (Constant.isMirrorApllying) {mMagicCameraDisplay.setMEffect2(CUMirrorType);}
        //mMagicCameraDisplay.setFilter(CUrrentFilterType);
    }

    public void reset_Filter_Mirror2() {
        reset_Filter_Mirror();
        if (Constant.is3DShapesApplying) {mMagicCameraDisplay.set3DEffect_(_3DShapeType);}
    }

    public void skip_Filter_Mirror() {
        if (Constant.isFilterApllying && Constant.isMirrorApllying) {
            mMagicCameraDisplay.setMEffect(0);
            mMagicCameraDisplay.setFilter(0);}
        else if (Constant.isFilterApllying) {mMagicCameraDisplay.setFilter(0);}
        else if (Constant.isMirrorApllying) {mMagicCameraDisplay.setMEffect2(0);}
        else if (Constant.is3DShapesApplying) {mMagicCameraDisplay.set3DEffect_(0);}
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.arg1==1) {
                if (checkFaceDetectionAvailable() && !isFaceDetecting) {
                    //System.out.println("whos_is_owner_"+msg.arg2);
                    initFaceDetection();
                    isFaceDetectionCommandFinished = true;
                }
            }
            else if (msg.arg1==12) {addShape(msg.arg2);}
            else if (msg.arg1==13) {setSquare(0);/*//square frame}*/}
            else if (msg.arg1==14) {
                if (msg.arg2==1) {getFilterToImage(Constant.bitmap);}
                else if(msg.arg2==2) {getFilterToImage(CameraEngine.nonfiltered);}
            }
            else if (msg.arg1==15) {getFilterToImage2(cu.getCollage());}
            else if (msg.arg1==17) {changeCamera();}
            else if (msg.arg1==18) {startProcessingAnimation(false);}
            else if (msg.arg1==19) {startProcessingAnimation(true);}
            else mMagicCameraDisplay.setFilter(CUrrentFilterType/*MagicFilterType.WALDEN*/);
            return false;
        }
    });
    private void startWaldenFilter() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                handler.sendEmptyMessage(0);
            }
        }, 800);
    }

    private void startSquareCameraContent(){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 13;
                m.arg2 = 3;
                handler.sendMessage(m);
            }
        }, 150);
    }

    private boolean isFaceDetectionCommandFinished = true;
    private void startFaceProcessing(final int d) {
        if (isFaceDetectionCommandFinished) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    Message m = new Message();
                    m.arg1 = 1;
                    m.arg2 = d;
                    handler.sendMessage(m);
                }
            }, 1200);

            isFaceDetectionCommandFinished = false;
        }
    }

    private View.OnClickListener onShareListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showOnShare_(false);
        }
    };

    //we do not want to click through current view
    private View.OnTouchListener onWellDoneClick = new View.OnTouchListener(){
        @Override
        public boolean onTouch(View arg0, MotionEvent art1){
            //nothing happens here
            return true;
        }
    };

    //we do want to click through current view
    private View.OnTouchListener onWellDoneClick2 = new View.OnTouchListener(){
        @Override
        public boolean onTouch(View arg0, MotionEvent art1){
            //nothing happens here
            return false;
        }
    };

    public void showOnShare_(boolean isShow) {
        if (isShow) findViewById(R.id.wellDone).setVisibility(View.VISIBLE);
        else {
            takeANewCreation();
            findViewById(R.id.wellDone).setVisibility(View.GONE);
            premiumItemType = 12;
            AdToApp.showInterstitialAd();//ads
        }
    }

    public void initTutorial() {
        if (!isTutoralWatched) {
            //holder
            final RelativeLayout tutorialContainerMain = (RelativeLayout)findViewById(R.id.tutorialContainer);
            tutorialContainerMain.setOnTouchListener(onWellDoneClick);

            //inflate view
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            View tutorial = LayoutInflater.from(YasaMainActivity.this).inflate(
                    R.layout.activity_yasa_tutorial,
                    null);
            tutorial.setLayoutParams(params);

            tutorialContainerMain.addView(tutorial);
            tutorialContainerMain.setVisibility(View.VISIBLE);

            findViewById(R.id.btnGotItNext).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.tutorial1Content).setVisibility(View.GONE);
                    findViewById(R.id.tutorial2Content).setVisibility(View.VISIBLE);
                }
            });
            findViewById(R.id.btnOkIAmReady).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.tutorial2Content).setVisibility(View.GONE);
                    findViewById(R.id.tutorial1_1Content).setVisibility(View.VISIBLE);
                }
            });
            findViewById(R.id.btnGotItNext2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.tutorial1_1Content).setVisibility(View.GONE);
                    tutorialContainerMain.setVisibility(View.GONE);
                    DataUtils.setBooleanPreferences(appContext, tutorialPreferenceName, true);
                    //remove children
                    tutorialContainerMain.removeAllViews();
                }
            });

        }
    }

    public void addPremiumItem(int type, int usp, int pos){
        if (type==0) {
            mMagicCameraDisplay.onResume();
            startPhotoShapeDelayed(pos);
        }
        else if (type==1) {
            if (!isShuttered && !isLoadedFromGalery) {
                addCollage(Constants.mScreenWidth, pos);
            }
            else if (isLoadedFromGalery && isCollageStarted)
                if (pos>1)showYasaDialog5(Constants.mScreenWidth, pos);
        }
        else if (type==2) {
            Addon selectedSticker;
            switch (usp) {
                case 0:
                    selectedSticker = new Addon( YasaStickers.getStickersValentineDay()[pos] );
                    addSticker( selectedSticker );
                    break;
                case 1:
                    selectedSticker = new Addon( YasaStickers.getStickersWomenDay()[pos] );
                    addSticker( selectedSticker );
                    break;
                case 2:
                    selectedSticker = new Addon( YasaStickers.getStickersWomenDay2()[pos] );
                    addSticker( selectedSticker );
                    break;
                case 3:
                    selectedSticker = new Addon( YasaStickers.getStickersVintage()[pos] );
                    addSticker( selectedSticker );
                    break;
                case 4:
                    selectedSticker = new Addon( YasaStickers.getStickersHairstyles()[pos] );
                    addSticker( selectedSticker );
                    break;
                //case 5: break;
            }

        }
    }

    private int premiumItemType;
    private int premiumItemPosition;
    private int premiumItemStickerType;
    public void initAdsView(final int type, int pos) {initAdsView(type,0, pos);}

    public void initAdsView(final int type, final int ustype, int position_) {
        premiumItemType = type;
        premiumItemPosition = position_;
        premiumItemStickerType = ustype;
        //if (!isTutoralWatched) {
            //holder
            final RelativeLayout tutorialContainerMain = (RelativeLayout)findViewById(R.id.tutorialContainer);

            //inflate view
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(0, 0, 0, getActionPanelHeight());
            View tutorial = LayoutInflater.from(YasaMainActivity.this).inflate(
                    R.layout.activity_yasa_video_ads,
                    null);
            tutorial.setLayoutParams(params);

            tutorialContainerMain.addView(tutorial);
            tutorialContainerMain.setVisibility(View.VISIBLE);
            FancyButton bbutton = (FancyButton)findViewById(R.id.btnBuy);
            if (type==0) {bbutton.setText(getString(R.string.ads_bps));}
            if (type==1) {bbutton.setText(getString(R.string.ads_bcv));}

            findViewById(R.id.btnWatch).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if (AdToApp.isAvailableAd(AdToApp.VIDEO))
                    //if (isVideoAdsReady) {
                    AdToApp.showInterstitialAd();
                    //AdToApp.showInterstitialAd(AdToApp.VIDEO);
                        //if (!isVideoAdsReady) {toast("Ads is not available, try later", 1);}

                        closeAdsView();
                        isVideoAdsReady=false;
                        isVideoAdsStarted=false;
                        isVideoAdsClosed=false;
                    //}
                    //else {toast("Ads is not available, try later", 1);}
                }
            });
            findViewById(R.id.btnBuy).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type==0) {onBuyItemPhotoSstickers_();startPurchase=true;closeAdsView();}
                    if (type==1) {onBuyItemCollageCircular();startPurchase=true;closeAdsView();}
                    if (type==2) {startStickersBuying(ustype);closeAdsView();}
                }
            });
            findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeAdsView();
                }
            });

        //}
    }

    public void closeAdsView() {
        final RelativeLayout tutorialContainerMain = (RelativeLayout)findViewById(R.id.tutorialContainer);
        tutorialContainerMain.removeAllViews();
        tutorialContainerMain.setVisibility(View.GONE);
    }
    SaveTask.onPictureSaveListener myOnImageProcessingListener = new SaveTask.onPictureSaveListener() {

        @Override
        public void onSaved(String result) {

            //showProgressDialog(getString(R.string.activity_yasa_processing_image));
            captureBitmap2(new BitmapReadyCallbacks() {
                @Override
                public void onBitmapReady() {
                    boolean one_=true;
                    if (one_) {
                    //if (imageFromGaleryView.getVisibility() == View.GONE) {
                        imageFromGaleryView.setVisibility(View.VISIBLE);
                        detachMultiTouchListener(imageFromGaleryView);
                        imageFromGaleryView.setOnTouchListener(null);
                        imageFromGaleryView.setOnTouchListener(new MultiTouchListener());
                        isLoadedFromGalery = true;
                        imageFromGaleryView.setScaleType(ImageView.ScaleType.FIT_XY);

                        //imageFromGaleryView.setImageResource(android.R.color.transparent);
                        imageFromGaleryView.setImageBitmap(CameraEngine.nonfiltered);
                        //imageFromGaleryView.setImageBitmap(CameraEngine.captured);
                    }

                    //mMagicCameraDisplay.onResume();

                    //restartFaceDetection();

                    showDarkCover();
                    //startProcessingAnimation(false);
                    dismissProgressDialog();

                    //startFaceProcessing();
                }
            });

            //if (result.equals("")) {loadBitmapFromUri("");}
        }
    };

    SaveTaskLight.onPictureSaveListenerLight myOnImageProcessingListenerLight = new SaveTaskLight.onPictureSaveListenerLight() {
        @Override
        public void onSaved(boolean saved) {
            if (saved) {

                /*captureBitmap(new BitmapReadyCallbacks() {
                    @Override
                    public void onBitmapReady() {
                        if (imageFromGaleryView.getVisibility() == View.GONE) {
                            imageFromGaleryView.setVisibility(View.INVISIBLE);
                            detachMultiTouchListener(imageFromGaleryView);
                            imageFromGaleryView.setOnTouchListener(null);
                            imageFromGaleryView.setOnTouchListener(new MultiTouchListener());
                            isLoadedFromGalery = true;

                            //imageFromGaleryView.setImageResource(android.R.color.transparent);
                            imageFromGaleryView.setImageBitmap(CameraEngine.bitmap*//*captured*//*);
                            imageFromGaleryView.setVisibility(View.VISIBLE);
                        }

                        dismissProgressDialog();
                    }
                });*/

                if (imageFromGaleryView.getVisibility() == View.GONE) {
                    imageFromGaleryView.setVisibility(View.INVISIBLE);
                    detachMultiTouchListener(imageFromGaleryView);
                    imageFromGaleryView.setOnTouchListener(null);
                    imageFromGaleryView.setOnTouchListener(new MultiTouchListener());
                    isLoadedFromGalery = true;
                    imageFromGaleryView.setScaleType(ImageView.ScaleType.FIT_XY);

                    //imageFromGaleryView.setImageResource(android.R.color.transparent);
                    imageFromGaleryView.setImageBitmap(CameraEngine.bitmap/*captured*/);
                    imageFromGaleryView.setVisibility(View.VISIBLE);
                }

                showDarkCover();
                //startProcessingAnimation(false);
                dismissProgressDialog();
                //dismissProgressDialog();
            }
        }
    };

    private int CUrrentFilterType = MagicFilterType.WALDEN;
    FilterLayoutUtilsYasa.OnMirrorEffectCallback onSelectFilter_CB = new FilterLayoutUtilsYasa.OnMirrorEffectCallback() {
        @Override
        public void onMirrorCallback(String t, int position, int tp_) {
            CUrrentFilterType = tp_;
            //toast(position+"",0);
            //applyMirrorEffect(position);
            if (/*!isPhotoShapeStarted &&*/ !isCollageStarted && isShuttered) {
                if (position==0) imageFromGaleryView.setImageBitmap(CameraEngine.nonfiltered);
                //else getFilterToImage(CameraEngine.nonfiltered);
                else getFilterToImageDelayed(2);
            }
            else if (/*!isPhotoShapeStarted &&*/ !isCollageStarted && !isShuttered && isLoadedFromGalery) {
                if (position==0) imageFromGaleryView.setImageBitmap(Constant.bitmap);
                //else getFilterToImage(Constant.bitmap);
                else getFilterToImageDelayed(1);
            }
            else if (isCollageStarted && cu.checkFilled() && isLoadedFromGalery) {
                if (position==0) {collageImageView.setImageBitmap(cu.getCollage());}
                else getFilterToImage2Delayed();//getFilterToImage2(cu.getCollage());
            }
        }
    };

    SaveTask.onPictureSaveListener myOnImageListener2 = new SaveTask.onPictureSaveListener() {

        @Override
        public void onSaved(String result) {
            dismissProgressDialog();
            if (!result.equals("")) {lastSavedImage = result;}
            //toast("saved__"+lastSavedImage,1);
            startTrackingAction2Save();
        }
    };

    SaveTask.onPictureSaveListener myOnImageListener3 = new SaveTask.onPictureSaveListener() {

        @Override
        public void onSaved(String result) {
            dismissProgressDialog();
            if (!result.equals("")) {
                lastSavedImage = result;
                shareYasa();
            }
            //toast("saved_"+lastSavedImage,1);
        }
    };

    SaveTask.onPictureSaveListener myOnImageProcessingListener4 = new SaveTask.onPictureSaveListener() {

        @Override
        public void onSaved(String result) {
            //dismissProgressDialog();
            isPhotoShapeStarted = false;
            sb1();
            //startAnyBarWithAnimation(photoStickersToolbar, 300);//open stickers bar
            //startAnyBarWithAnimation(stickersToolbar, 300);//open stickers bar

            captureBitmap(new BitmapReadyCallbacks() {
                @Override
                public void onBitmapReady() {
                    View myview = findViewById(R.id.moveMagic);
                    copyRotationTransaltionScale(myview);
                    shapewView.setScaleType(ImageView.ScaleType.MATRIX);
                    shapewView.setVisibility(View.INVISIBLE);
                    shapewView.setImageBitmap(cropBitmap(myview, CameraEngine.captured));
                    //mMagicCameraDisplay.onPause();
                    //glSurfaceView.setVisibility(View.INVISIBLE);
                    addStickerView(0, loadBitmapFromView_(shapewView, shapewView.getWidth(), shapewView.getHeight()));
                    shapewView.setVisibility(View.GONE);

                    //mMagicCameraDisplay.onResume();
                    //restartFaceDetection();

                    showDarkCover();
                    //dismissProgressDialog();

                    //startFaceProcessing();
                }
            });

            //Bitmap btm = getBitmapFromView(shapewView);
            //applySCreenshot(btm);
            //new SaveTask(appContext, Constants.getOutputMediaFile(), null).execute( loadBitmapFromView(shapewView, shapewView.getWidth(), shapewView.getHeight()) );
        }
    };

    SaveTask.onPictureSaveListener myOnImageProcessingListener6 = new SaveTask.onPictureSaveListener() {

        @Override
        public void onSaved(String result) {

            captureBitmap(new BitmapReadyCallbacks() {
                @Override
                public void onBitmapReady() {
                    receiveCollageStateWhenShuttered();
                    mMagicCameraDisplay.onResume();
                }
            });

        }
    };
    public void takePictureForCollage() {
        captureBitmap(new BitmapReadyCallbacks() {
            @Override
            public void onBitmapReady() {
                receiveCollageStateWhenShuttered();
                //mMagicCameraDisplay.onResume();
            }
        });
    }
    /*SaveTask.onPictureSaveListener myOnImageProcessingListener5 = new SaveTask.onPictureSaveListener() {

        @Override
        public void onSaved(String result) {

            //openShapesBar(true);
            captureBitmap(new BitmapReadyCallbacks() {
                @Override
                public void onBitmapReady() {
                    receiveCollageStateWhenShuttered();
                    mMagicCameraDisplay.onResume();
                    //restartFaceDetection();

                    //showDarkCover();
                    //dismissProgressDialog();

                    //startFaceProcessing();
                }
            });

        }
    };*/



    public Bitmap cropBitmap(View v, Bitmap b) {
        Bitmap cropped = Bitmap.createBitmap(b, (int)v.getX(), (int)v.getY(), v.getWidth(), v.getHeight());
        return cropped;
    }
    public Bitmap cropBitmap2(View v, int x, int y, Bitmap b) {
        int startX = x;//(Constants.mScreenWidth - v.getWidth())/2 + (int)v.getX();
        int startY = y;//findViewById(R.id.collageDivider1).getHeight() + (int)v.getY();
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, v.getWidth(), v.getHeight());
        return cropped;
    }

    public Bitmap cropBitmap3(Bitmap b, int startX, int startY, int w, int h) {
        Bitmap cropped = Bitmap.createBitmap(b, startX, startY, w, h);
        return cropped;
    }
    public static Bitmap loadBitmapFromView_(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width , height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }

    /*public Bitmap drawBitmap() {
        Bitmap bitmap = Bitmap.createBitmap(shapewView.getWidth(), shapewView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        shapewView.draw(canvas);
        return bitmap;
    }*/

    public long LastShutterTime = 0;
    public void showDarkCover() {findViewById(R.id.surfaceCover).setVisibility(View.VISIBLE);}
    public void hideDarkCover() {findViewById(R.id.surfaceCover).setVisibility(View.GONE);}
    //add onshutter callback
    private void onShutter() {
        int timeLimit = isCollageStarted  ? 1000 : 3000;
        long shutterTime = System.currentTimeMillis();
        if ( (shutterTime- LastShutterTime) < timeLimit ) {
            toast( getString(R.string.taking_photo) ,0);
            return;
        }

        LastShutterTime = System.currentTimeMillis();
        if (isCollageStarted) {
            if (isPhotoShapeStarted) {
                PhotoShapeStarted();
            }
            else {
                mMagicCameraDisplay.onTakePictureSimple(myOnImageProcessingListener6);
                //showProgressDialog(getString(R.string.activity_yasa_processing_image));
                //mMagicCameraDisplay.onTakePicture(Constants.getOutputMediaFile(), myOnImageProcessingListener5, null, false);
            }

        }
        else if (isPhotoShapeStarted) {
            PhotoShapeStarted();
            /*View myview = findViewById(R.id.moveMagic);
            if ( !checkShapeBounds((int)myview.getX(), (int)myview.getY(), myview ) ) {showYasaDialog4();}
            else {
                showProgressDialog(getString(R.string.activity_yasa_processing_image));
                mMagicCameraDisplay.onTakePicture(Constants.getOutputMediaFile(), myOnImageProcessingListener4, null, false);
                //ShapeCamera.onTakePicture(Constants.getOutputMediaFile(), null, null, false);
                isPhotoShapeShuttered = true;
            }*/

        }
        else {
            //startProcessingAnimation(true);
            showProgressDialog(getString(R.string.activity_yasa_processing_image));
            //mMagicCameraDisplay.onTakePicture(Constants.getOutputMediaFile(), myOnImageProcessingListener, null, false);
            if (Constant.isMirrorApllying || Constant.is3DShapesApplying || !Constant.BACK_CAMERA_IN_USE)
                mMagicCameraDisplay.onTakePicture(Constants.getOutputMediaFile(), myOnImageProcessingListener, null, false);
            else
                mMagicCameraDisplay.onTakePicture(null, myOnImageProcessingListenerLight, null, false);
            //startProcessingAnimation(true);
            isShuttered = true;
            showOnShutter(true);
            /*showProgressDialog(getString(R.string.activity_yasa_processing_image));*/

        }
    }

    public void PhotoShapeStarted() {
        View myview = findViewById(R.id.moveMagic);
        if ( !checkShapeBounds((int)myview.getX(), (int)myview.getY(), myview ) ) {showYasaDialog4();}
        else {
            //showProgressDialog(getString(R.string.activity_yasa_processing_image));
            mMagicCameraDisplay.onTakePicture(Constants.getOutputMediaFile(), myOnImageProcessingListener4, null, false);
            //ShapeCamera.onTakePicture(Constants.getOutputMediaFile(), null, null, false);
            isPhotoShapeShuttered = true;
        }
    }
    //public void clearStickersOverlay() {mImageView.clearOverlays();}

    public void detachMultiTouchListener(View view) {

        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
        view.setRotation(0.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        view.setPivotX(0.0f);
        view.setPivotY(0.0f);

    }
    public void onCollageRequireImage(Bitmap b) {
        Bitmap tmp = BitmapProcessor.cropBitmapSquare(b, b.getWidth(), b.getHeight());;
        /*if (cu.getCurrentItemCollage().getTypeName().equals("square")) {
            tmp = BitmapProcessor.cropBitmapSquare(
                    b,
                    b.getWidth(),
                    b.getHeight());
        }*/
        if (cu.getCurrentItemCollage().getTypeName().equals("rectV")) {
            tmp = BitmapProcessor.cropBitmapRectV(b, b.getWidth(), b.getHeight());
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("rectH")) {
            tmp = BitmapProcessor.cropBitmapRectH(b, b.getWidth(), b.getHeight());
        }
        applyCollagePiece(tmp, 0, 0, 0, 0, false);
        //toast(b.getWidth()+" "+b.getHeight(),1);
    }
    public void hideCameraSurface(Bitmap b) {
        isShuttered = false;

        isLoadedFromGalery=true;
        //mMagicCameraDisplay.onPause();
        showDarkCover();
        //glSurfaceView.setVisibility(View.INVISIBLE);
        imageFromGaleryView.setVisibility(View.VISIBLE);
        imageFromGaleryView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        detachMultiTouchListener(imageFromGaleryView);
        imageFromGaleryView.setOnTouchListener(null);
        imageFromGaleryView.setOnTouchListener(new MultiTouchListener());
        imageFromGaleryView.setImageResource(android.R.color.transparent);

        if (CUrrentFilterType>0) {getFilterToImageDelayed(1);}
        else imageFromGaleryView.setImageBitmap(Constant.bitmap);

        clearStickers();//clear selected stickers
        if(isPhotoShapeStarted || isPhotoShapeShuttered) removeShapeView();
    }

    public void skipCollageLight() {
        isCollageStarted = false;
        clearStickers();
        if(isPhotoShapeStarted || isPhotoShapeShuttered) removeShapeView();

        hideDarkCover();
    }
    public void skipCollage(boolean startFaceProcessing) {
        skipCollage();
        if (startFaceProcessing) { startFaceProcessing(2);
            //toast("FACEDETECTION_SKIPCOLLAGE",1);
        }
    }
    public void skipCollage() {
        skipCollageLight();
        skipCollageLight2();
        setCollageBarVisible(false);

        /*ImageView iv = (ImageView) findViewById(R.id.col_icon);
        iv.setImageResource(android.R.color.transparent);
        isLoadedFromGalery = false;*/
    }
    public void startCameraSurface() {
        /*showOnShutter(false);*/
        //hide galery image
        if (imageFromGaleryView.getVisibility()==View.VISIBLE) {
            imageFromGaleryView.setImageResource(android.R.color.transparent);
            imageFromGaleryView.setVisibility(View.GONE);
        }
        //show surface
        //hide stickers overlay
        mMagicCameraDisplay.onResume();
        //restartFaceDetection();

        hideDarkCover();
        //if (glSurfaceView.getVisibility()==View.INVISIBLE) glSurfaceView.setVisibility(View.VISIBLE);

        clearStickers();
        lastSavedImage = "";
        isShuttered = false;
        isLoadedFromGalery = false;
        if(isPhotoShapeStarted || isPhotoShapeShuttered) removeShapeView();
        /*isPhotoShapeShuttered = false;//this is also in removeShapeView
        isPhotoShapeStarted = false;*/
        startFaceProcessing(3);
        showOnShutter(false);

        /*if (Constant.isMirrorApllying) {
            Constant.isMirrorApllying = false;
            mMagicCameraDisplay.setFilter(CUrrentFilterType);
            //mMEffectsYasa.to_def_();
        }*/
        if(Constant.BACK_CAMERA_IN_USE) turnLight2(flashState);
        //skip 3d_shape
        if (Constant.is3DShapesApplying) { Constant.is3DShapesApplying = false; _3DEffectsYasa.setSelection(0);}
        reset_Filter_Mirror();
        lock_Filter_Mirror2(false);
    }

    public void addSticker(Addon add) {
        if(isShuttered || isLoadedFromGalery
                /*|| (isCollageStarted && checkReady())*/ ) addStickerView(add.getId(), null);
        //if (isShuttered) {
        /*EffectUtil.addStickerImage(mImageView, YasaMainActivity.this, add,
                new EffectUtil.StickerCallback() {
                    @Override
                    public void onRemoveSticker(Addon add) {

                    }
                });
        mImageView.autoDeselect();*/
        //}
    }

    public void addShape(int pos) {
        if ( (isShuttered || isLoadedFromGalery
                /*|| (isCollageStarted && checkReady())*/ ) /*&& !isPhotoShapeStarted*/) {
            double sc_default = 0.7;
            float mScale = 1.42f;
            //we give start shape larger scale
            if (pos==5) { sc_default = 0.8; mScale = 1.25f; }
            /*if (pos!=2)*/ addShapeView( (int)(Constants.mScreenWidth*sc_default), pos, mScale );
        }
        /*else if (isPhotoShapeStarted) {showYasaDialog4();}*/

    }

    public void addShapeView(int w, int id, float s) {
        RelativeLayout move = (RelativeLayout)findViewById(R.id.moveMagic);
        if (isPhotoShapeShuttered) {
            isPhotoShapeShuttered=!isPhotoShapeShuttered;
            shapewView.setImageResource(android.R.color.transparent);
            detachMultiTouchListener(move);}
        isPhotoShapeStarted = true;
        if(shapewView.getVisibility()==View.GONE) shapewView.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w, w);
        shapewView.setLayoutParams(params);
        /*shapewView.setScaleType(ImageView.ScaleType.MATRIX);
        shapewView.setImageBitmap(CameraEngine.captured);*/
        ShapeCamera = new ShapeCameraDisplay(w, id, this, shapewView);
        //add move listener
        //detachMultiTouchListener(move);
        move.setOnClickListener(null);
        move.setOnTouchListener(new MultiTouchListener(false, true, s));
        //Open Action Panel
        hideAnyBarWithAnimation(photoStickersToolbar, AMIMATION_TIME);//open action panel
        //hideAnyBarWithAnimation(stickersToolbar, 300);//open action panel

        sb2();
    }

    public void removeShapeView(){
        shapewView.setVisibility(View.GONE);
        ShapeCamera.onPause();
        ShapeCamera = null;
        isPhotoShapeStarted = false;
        isPhotoShapeShuttered = false;
        detachMultiTouchListener(findViewById(R.id.moveMagic));
        shapewView.setImageResource(android.R.color.transparent);
    }

    private boolean isSquareType = false;
    public void setSquare(int id) {
        RelativeLayout.LayoutParams params;
        RelativeLayout.LayoutParams params2;
        if (id==0) {
            if (!isSquareType) {

                params = new RelativeLayout.LayoutParams(Constants.mScreenWidth, getSquareCoverTopHeight());
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                params2 = new RelativeLayout.LayoutParams(Constants.mScreenWidth, getSquareCoverBottomHeight());
                params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                RelativeLayout v = (RelativeLayout) findViewById(R.id.squareCoverTop);
                RelativeLayout v2 = (RelativeLayout) findViewById(R.id.squareCoverBottom);
                v.setLayoutParams(params);
                v2.setLayoutParams(params2);
                v.setVisibility(View.VISIBLE);
                v2.setVisibility(View.VISIBLE);
                isSquareType = true;

                setWMView(1);//add watermark
            }
        }
        else if (id==1) {
            findViewById(R.id.squareCoverTop).setVisibility(View.GONE);
            findViewById(R.id.squareCoverBottom).setVisibility(View.GONE);
            isSquareType = false;

            setWMView(2);//add watermark
        }
        if (isCollageStarted) {skipCollage(true);}
    }

    public boolean checkSquareFrameEnabled() {
        return ( findViewById(R.id.squareCoverTop).getVisibility()==View.VISIBLE ) &&
                ( findViewById(R.id.squareCoverBottom).getVisibility()==View.VISIBLE );
    }

    public void setWMView(int state) {
        //RelativeLayout WM = (RelativeLayout) findViewById(R.id.yasa_wm);
        if (state==1) {WaterMarkLogoView.setPadding(0, 0, 0, getSquareCoverBottomHeight());}
        else if (state==2) {WaterMarkLogoView.setPadding(0, 0, 0, getActionPanelBackgroundColorHeight());}
        if (WaterMarkLogoView.getVisibility()==View.GONE) WaterMarkLogoView.setVisibility(View.VISIBLE);
    }
    public void hideWMView() {
        WaterMarkLogoView.setVisibility(View.GONE);}

    public void addCollage(int w, int id) {
        if (id <= 1 /*&& !isCollageStarted*/) {setSquare(id);}
        else if (id > 1) {
            setSquare(1);
            skipCollage();
            hideFaceDetectionBorder();
            /*skipCollageLight2();
            hideDarkCover();*/

            int type = id-2;
            setCollageBarVisible(true);
            ImageView iv = (ImageView) findViewById(R.id.col_icon);
            if (cu!=null) cu.destroyCollage();
            cu = new CollageUsual(w, w, type, iv);
            handleCollageState(w);

            isCollageStarted = true;
            hideWMView();//Hide WM
            openShapesBar(false);
        }
    }

    private CollageUsual cu;

    public void receiveCollageStateWhenShuttered() {
        int startX = 0;
        int startY = 0;
        int startWidth = Constants.mScreenWidth;
        int startHeight = Constants.mScreenWidth;
        if (cu.getCurrentItemCollage().getTypeName().equals("rectV")) {
            startX = findViewById(R.id.squareCoverLeft2).getWidth();
            startWidth = Constants.mScreenWidth -
                    (findViewById(R.id.squareCoverLeft2).getWidth() + findViewById(R.id.squareCoverRight2).getWidth());
            startHeight = getScreenContentHeight() - getActionPanelHeight();
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("rectH")) {
            startY = findViewById(R.id.squareCoverTop2).getHeight();
            startHeight = getScreenContentHeight() -
                    (findViewById(R.id.squareCoverTop2).getHeight() + findViewById(R.id.squareCoverBottom2).getHeight());
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("circle")) {
            startY = findViewById(R.id.squareCoverTop2).getHeight();
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("square")) {
            startY = findViewById(R.id.squareCoverTop2).getHeight();
            /*cu.fillNext( cropBitmap3(CameraEngine.captured, startX, startY, startWidth, startHeight) );*/
        }
        applyCollagePiece(CameraEngine.captured, startX, startY, startWidth, startHeight, true);
    }

    public void applyCollagePiece(Bitmap b, int startX, int startY, int startWidth, int startHeight, boolean isCrop) {
        if (isCrop) cu.fillNext( BitmapProcessor.cropBitmap3(b/*CameraEngine.captured*/, startX, startY, startWidth, startHeight) );
        else cu.fillNext(b);
        if (cu.checkFilled()) {
            setSquareCollage(0,0);
            setRectCollage(1, 0);
            //setTheImageAsPicture
            ImageView iv = (ImageView) findViewById(R.id.col_icon);
            iv.setImageResource(android.R.color.transparent);
            iv.setImageBitmap(cu.getCollage());

            isLoadedFromGalery = true;
            showDarkCover();
            showOnShutter(true);

            setWMView(1);//apply yasa logo view
        }
        else {handleCollageState(Constants.mScreenWidth);}
    }
    public void handleCollageState(int w) {
        setSquareCollage(1,0);
        setRectCollage(1,0);
        if (cu.getCurrentItemCollage().getTypeName().equals("rectV")) {
            float scaleF = (float)(getScreenContentHeight() - getActionPanelHeight()) / ( (float)Constants.mScreenWidth/2f );
            float vertical_frame_size = ( (float)Constants.mScreenWidth - (float)Constants.mScreenWidth/3f * scaleF )/2f;
            setRectCollage(0, (int) vertical_frame_size);

            RelativeLayout cl_ = (RelativeLayout)findViewById(R.id.collage_type1);
            RelativeLayout.LayoutParams params;
            params = new RelativeLayout.LayoutParams(w, getScreenContentHeight() - getActionPanelHeight());
            cl_.setLayoutParams(params);
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("rectH")) {
            float scaleF = (float)Constants.mScreenWidth / ((float)Constants.mScreenWidth/3f*2f);
            float horizontal_frame_size = ((float)Constants.mScreenWidth - (float)Constants.mScreenWidth/2f * scaleF)/2f;
            //horizontal_frame_size = (int)((float)horizontal_frame_size / scaleF);
            setSquareCollage(0,(int)horizontal_frame_size);

            RelativeLayout cl_ = (RelativeLayout)findViewById(R.id.collage_type1);
            RelativeLayout.LayoutParams params;
            params = new RelativeLayout.LayoutParams(w, (int) ((float)w/2*scaleF) );
            params.addRule(RelativeLayout.BELOW, R.id.squareCoverTop2);
            cl_.setLayoutParams(params);
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("circle")) {
            setSquareCollage(0,0);

            RelativeLayout cl_ = (RelativeLayout)findViewById(R.id.collage_type1);
            RelativeLayout.LayoutParams params;
            params = new RelativeLayout.LayoutParams(w, w);
            params.addRule(RelativeLayout.BELOW, R.id.squareCoverTop2);
            cl_.setLayoutParams(params);
        }
        else if (cu.getCurrentItemCollage().getTypeName().equals("square")) {
            setSquareCollage(0,0);

            RelativeLayout cl_ = (RelativeLayout)findViewById(R.id.collage_type1);
            RelativeLayout.LayoutParams params;
            params = new RelativeLayout.LayoutParams(w, w);
            params.addRule(RelativeLayout.BELOW, R.id.squareCoverTop2);
            cl_.setLayoutParams(params);
        }
    }

    public void setSquareCollage(int id, int rect_size) {
        RelativeLayout.LayoutParams params;
        RelativeLayout.LayoutParams params2;
        if (id==0) {
            //if (!isSquareType) {
                params = new RelativeLayout.LayoutParams(Constants.mScreenWidth, getSquareCoverTopHeight() + rect_size);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                params2 = new RelativeLayout.LayoutParams(Constants.mScreenWidth, getSquareCoverBottomHeight() + rect_size);
                params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                RelativeLayout v = (RelativeLayout) findViewById(R.id.squareCoverTop2);
                RelativeLayout v2 = (RelativeLayout) findViewById(R.id.squareCoverBottom2);
                v.setLayoutParams(params);
                v2.setLayoutParams(params2);
                v.setVisibility(View.VISIBLE);
                v2.setVisibility(View.VISIBLE);
                //isSquareType = true;
            //}
        }
        else if (id==1) {
            findViewById(R.id.squareCoverTop2).setVisibility(View.GONE);
            findViewById(R.id.squareCoverBottom2).setVisibility(View.GONE);
            //isSquareType = false;
        }
    }

    public void setRectCollage(int id, int rect_size) {
        RelativeLayout.LayoutParams params;
        RelativeLayout.LayoutParams params2;
        if (id==0) {
            //if (!isSquareType) {
                params = new RelativeLayout.LayoutParams(rect_size, getScreenContentHeight()- getActionPanelHeight());
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                params2 = new RelativeLayout.LayoutParams(rect_size, getScreenContentHeight()- getActionPanelHeight());
                params2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                RelativeLayout v = (RelativeLayout) findViewById(R.id.squareCoverLeft2);
                RelativeLayout v2 = (RelativeLayout) findViewById(R.id.squareCoverRight2);
                v.setLayoutParams(params);
                v2.setLayoutParams(params2);
                v.setVisibility(View.VISIBLE);
                v2.setVisibility(View.VISIBLE);
                //isSquareType = true;
            //}
        }
        else if (id==1) {
            findViewById(R.id.squareCoverLeft2).setVisibility(View.GONE);
            findViewById(R.id.squareCoverRight2).setVisibility(View.GONE);
            //isSquareType = false;
        }
    }

    public void skipCollageLight2() {
        setSquareCollage(1,0);
        setRectCollage(1,0);
        setCollageBarVisible(false);

        ImageView iv = (ImageView) findViewById(R.id.col_icon);
        iv.setImageResource(android.R.color.transparent);
        isLoadedFromGalery = false;
    }

    public void setCollageBarVisible(boolean visible) {
        if (visible) findViewById(R.id.collageViewer).setVisibility(View.VISIBLE);
        else findViewById(R.id.collageViewer).setVisibility(View.GONE);
    }

    private FiltersAdapterYasaCollage.AdapterCollageCallback myOnCollageCallback = new FiltersAdapterYasaCollage.AdapterCollageCallback() {
        @Override
        public void onMethodCallbackCollage(String t, int position) {
            if (t.equals(SKU_COLLAGE_CIRCULAR)) {
                if (!isCollageCircularPremium && position==3) {
                    initAdsView(1,position);
                    //onBuyItemCollageCircular();startPurchase=true;
                }
                else {//if we own
                    if (!isShuttered && !isLoadedFromGalery) {
                        addCollage(Constants.mScreenWidth, position);
                        /*if (!isLoadedFromGalery && isCollageStarted)
                            addCollage(Constants.mScreenWidth, position);
                        else if (isLoadedFromGalery && isCollageStarted)
                            showYasaDialog5(Constants.mScreenWidth, position);*/
                    }
                    else if (isLoadedFromGalery && isCollageStarted)
                        if (position>1)showYasaDialog5(Constants.mScreenWidth, position);
                }
            }
        }
    };
    private Bitmap applyChangesBitmap(Bitmap myBitmap, int mywidth, int myheight) {
        final Bitmap newBitmap = Bitmap.createBitmap(mywidth, myheight,
        //final Bitmap newBitmap = Bitmap.createBitmap(mContentRootView.getWidth(), mContentRootView.getHeight(),
        //final Bitmap newBitmap = Bitmap.createBitmap(mImageView.getWidth(), mImageView.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas cv = new Canvas(newBitmap);
        RectF dst = new RectF(0, 0, mywidth, myheight);
        //RectF dst = new RectF(0, 0, mContentRootView.getWidth(), mContentRootView.getHeight());
        //RectF dst = new RectF(0, 0, mImageView.getWidth(), mImageView.getHeight());
        cv.drawBitmap(
                myBitmap,
                //CameraEngine.captured,
                null,
                dst,
                null);

        //EffectUtil.applyOnSave(cv, mImageView);
        return newBitmap;
    }

    private Bitmap applySCreenshot(Bitmap myBitmap){
        final Bitmap newBitmap = Bitmap.createBitmap(shapewView.getWidth(), shapewView.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas cv = new Canvas(newBitmap);
        RectF dst = new RectF(0, 0, shapewView.getWidth(), shapewView.getHeight());
        cv.drawBitmap(
                myBitmap,
                null,
                dst,
                null);

        return newBitmap;

    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap bitmap;
        v.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public void showOnPauseScreen() {
        findViewById(R.id.onpause).setVisibility(View.VISIBLE);
        //showProgressDialog("Refreshing...");
    }
    public void hideOnPauseScreen() {
        findViewById(R.id.onpause).setVisibility(View.GONE);
    }
    @Override
    protected void onPause() {
        super.onPause();

        /*if (isFaceDetecting)*/ //pauseFaceDetection(0);

        //showOnPauseScreen();
        if (!startShare && !startPurchase) {
            showOnPauseScreen();
            pauseFaceDetection(1);

            /*isShuttered = false;
            lastSavedImage = "";

            //hide view
            if (isLoadedFromGalery) {
                imageFromGaleryView.setImageResource(android.R.color.transparent);
                imageFromGaleryView.setVisibility(View.GONE);
                isLoadedFromGalery = false;
            }*/

            //hide cover on top of glsurface
            //hideDarkCover();

            //if (glSurfaceView.getVisibility()==View.INVISIBLE) glSurfaceView.setVisibility(View.VISIBLE);

            //clearStickers();
            if (mMagicCameraDisplay != null)
                mMagicCameraDisplay.onPause();
            /*if(isPhotoShapeStarted || isPhotoShapeShuttered) removeShapeView();
            isPhotoShapeStarted = false;
            isPhotoShapeShuttered = false;

            if (isCollageStarted) { isCollageStarted = false;skipCollageLight2(); }    */
        }
        //else startShare = false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mMagicCameraDisplay != null)
            mMagicCameraDisplay.onDestroy();

        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    private BitmapFactory.Options downScaledImageOptions(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, options);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = options.outWidth, height_tmp = options.outHeight;

        //we do not need to downscale, return
        /*if (width_tmp <= REQUIRED_SIZE && height_tmp <= height_tmp)
            return null;*/

        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return o2;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        System.out.println("activity_onresult");
        if (requestCode == YasaConstants.REQUEST_PICK && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            InputStream imageStream = null;
            BitmapFactory.Options ds_options = null;
            try {
                ds_options = downScaledImageOptions(selectedImage);
                imageStream = getContentResolver().openInputStream(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
            if (imageStream!=null) {
                //Constant.bitmap = BitmapFactory.decodeStream(imageStream);
                Constant.bitmap = BitmapFactory.decodeStream(imageStream, null, ds_options);

                if (!isCollageStarted) {
                    hideCameraSurface(null);
                    showOnShutter(true);
                } else {
                    onCollageRequireImage(Constant.bitmap);
                }
            }
        }
        if ((requestCode == YasaConstants.REQUEST_PICK && resultCode == RESULT_CANCELED)) {
            //startCameraSurface();
        }
        if (requestCode == YasaConstants.REQUEST_SHARE /*&& resultCode == RESULT_OK*/) {
            //startCameraSurface();
            /*SCS*/
            showOnShare_(true);
            //hideOnPauseScreen();

            //if (isCollageStarted) skipCollage();
        }

        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            //Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == YasaConstants.REQUEST_PERMISSION_SAVE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            { saveYasa(myOnImageListener2); }
            else { toast( getString(R.string.message_no_save_permission) ,0); }
        }
        else if (requestCode == YasaConstants.REQUEST_PERMISSION_SAVE_BEFORE_SHARE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            { saveYasa(myOnImageListener3); }
            else { toast( getString(R.string.message_no_save_permission2) ,0); }
        }
    }

    public boolean checkCameraStartConditions() {
        return ( (isCollageStarted ) || (isPhotoShapeStarted && isLoadedFromGalery) || (!isLoadedFromGalery) );
    }

    public void copyRotationTransaltionScale(View v) {
        ShapePosition.setPositionX(v.getX());
        ShapePosition.setPositionY(v.getY());
        //v1.setScaleX(v2.getScaleX());
        //v1.setScaleY(v2.getScaleY());
        //v1.setRotation(v2.getRotation());
        //v1.setTranslationX(v2.getTranslationX());
        //v1.setTranslationY(v2.getTranslationY());
        //v1.setPivotX(v2.getPivotX());
        //v1.setPivotY(v2.getPivotY());
        //v1.setX(v2.getX());
        //v1.setY(v2.getY());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_yasa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void addStickerView(int sticker, Bitmap b) {
        final StickerView stickerView = new StickerView(this);
        if (b!=null) {
            stickerView.setBitmap2WithPosition(b, ShapePosition.getPositionX(), ShapePosition.getPositionY());
        }
        else
            stickerView.setImageResource(sticker);
        stickerView.setOperationListener(new StickerView.OperationListener() {
            @Override
            public void onDeleteClick() {
                mViews.remove(stickerView);
                mContentRootView.removeView(stickerView);
            }

            @Override
            public void onEdit(StickerView stickerView) {
                /*if (mCurrentEditTextView != null) {
                    mCurrentEditTextView.setInEdit(false);
                }*/
                mCurrentView.setInEdit(false);
                mCurrentView = stickerView;
                mCurrentView.setInEdit(true);
            }

            @Override
            public void onTop(StickerView stickerView) {
                int position = mViews.indexOf(stickerView);
                if (position == mViews.size() - 1) {
                    return;
                }
                StickerView stickerTemp = (StickerView) mViews.remove(position);
                mViews.add(mViews.size(), stickerTemp);
            }
        });
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        mContentRootView.addView(stickerView, lp);
        mViews.add(stickerView);
        setCurrentEdit(stickerView);
    }
    public void clearStickers(){
        for (View sticker : mViews) {
            mContentRootView.removeView(sticker);
        }
        mViews.clear();
    }

    public void HideStickersEditMode(){
        for (View sticker : mViews) {
            StickerView t = (StickerView)sticker;
            t.setInEdit(false);
        }
    }

    private void setCurrentEdit(StickerView stickerView) {
        if (mCurrentView != null) {
            mCurrentView.setInEdit(false);
        }
        /*if (mCurrentEditTextView != null) {
            mCurrentEditTextView.setInEdit(false);
        }*/
        mCurrentView = stickerView;
        stickerView.setInEdit(true);
    }

    private View.OnTouchListener openGallery = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View arg0, MotionEvent arg1) {
            switch (arg1.getAction()) {
                    /*case MotionEvent.ACTION_DOWN: {
                        break;
                    }*/
                case MotionEvent.ACTION_UP:{

                    //DIALOG_CS
                    if (arg0==buttonSuperYasa) {
                        //initFaceDetection();
                        openPhotoStickersBar(true);/*showYasaDialog();*/
                    }
                    //PICK IMAGE FROM GALLERY
                    if ( arg0==buttonGallery ) {
                        if (isLoadedFromGalery) showYasaDialog3();
                        else loadPictureFromGallery();
                    }
                    if ( arg0==buttonGallery2 ) {
                        if (isLoadedFromGalery) showYasaDialog3();
                        else loadPictureFromGallery();
                    }
                    //SET TIMER
                    if ( arg0==buttonTimer ) { setTimerButtonImage(); }
                    //SET FLASH
                    if ( arg0==buttonFlashView ) { setFlashButtonImage();}
                    //DO SHUTTER
                    if ( arg0==buttonShutter || arg0==buttonShutter1)  {
                        //initAdsView();
                        if (isPhotoShapeStarted && !isPhotoShapeShuttered) onShutterHelper();
                        else if (isShuttered || isLoadedFromGalery ||
                                isPhotoShapeShuttered /*||
                                (isCollageStarted && checkReady())*/ ) {showYasaDialog2();}
                        else {
                            onShutterHelper();
                        }
                    }
                    //CHANGE CAMERA
                    if ( arg0==buttonChangeCameraView ) {
                        startProcessingAnimationDelay2();
                        //startProcessingAnimation(true);
                        changeCameraDelay();
                        stopProcessingAnimationDelay();/*changeCamera();*/ }
                    //OPEN SHAPES BAR
                    if ( arg0==buttonShapes ) {
                        if (toolbar3shapes.getVisibility()==View.GONE) openShapesBar(true);
                        else openShapesBar(false);
                    }
                    //SHOW STICKERS BAR
                    if ( arg0==buttonStickers) { openStickersBar(true); }
                    //SHARE
                    if ( arg0==buttonShare) { if (isShuttered || isLoadedFromGalery || isCollageStarted) {
                        askWritePermission(YasaConstants.REQUEST_PERMISSION_SAVE_BEFORE_SHARE);
                        //saveYasa(myOnImageListener3);
                    }
                        /*saveYasaBeforeShare(myOnImageListener3);*/
                        /*shareYasa();*/}
                    }
                    //SAVE
                    if ( arg0==buttonSave) { if (isShuttered || isLoadedFromGalery || isCollageStarted) {
                        askWritePermission(YasaConstants.REQUEST_PERMISSION_SAVE);
                        //saveYasa(myOnImageListener2);}
                    }
                    //RANDOM_FILTER
                    /*if (arg0==buttonShuffleFilter) { applyShuffleFilter(); }
                    break;*/
                    /*WATER MARK*/
                    if (arg0==buttonWM) { if (!isWMarkPremium) {onBuyWMark(SKU_WMARK);startPurchase=true;} }
                }
            }
            return true;
        }
    };

    public void onShutterHelper() {
        pauseFaceDetection(1);

        if (timerState == 0) { onShutter(); }
        if (timerState == 1) startTimerUpdate(0, 1, 2);
        if (timerState == 2) startTimerUpdate(0, 1, 5);
        if (timerState == 3) startTimerUpdate(0, 1, 10);

        startTrackingAction5TakeAPhoto();
    }

    public void takeANewCreation(){
        if (isCollageStarted) {
            setSquare(0);
            skipCollage(true);
            startCameraSurface();
        } else {
            startCameraSurface();
        }
    }

    public void startProcessingAnimation(boolean isStart) {
        int padingTop = getSquareCoverTopHeight()+(Constants.mScreenWidth/2)-25;
        //toast(progressBarContainer.getHeight()+" "+padingTop,1);
        progressBarContainer.setPadding(0, padingTop, 0, 0);
        if (isStart ) {
            spinKitView.setIndeterminateDrawable(drawable);
            findViewById(R.id.onprocessing).setVisibility(View.VISIBLE);
        }
        else {
            spinKitView.unscheduleDrawable(drawable);
            findViewById(R.id.onprocessing).setVisibility(View.GONE);
        }
    }
    private void showYasaDialog2() {
        final NiftyDialogBuilder newDialogBuilder=NiftyDialogBuilder.getInstance(this);
        newDialogBuilder
                .withTitle("Yasa 2")
                .withMessage( getString(R.string.dialog_message) )
                .withButton1Text( getString(R.string.dialog_message_ok) )
                .withButton2Text( getString(R.string.dialog_message_cancel) )
                .withDialogColor("#ff1d1d1d")
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        takeANewCreation();
                        newDialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newDialogBuilder.dismiss();
                    }
                })
                .show();
    }

    private void showYasaDialog3() {
        final NiftyDialogBuilder newDialogBuilder=NiftyDialogBuilder.getInstance(this);
        newDialogBuilder
                .withTitle("Yasa 2")
                .withMessage( getString(R.string.dialog_message2) )
                .withButton1Text( getString(R.string.dialog_message_ok) )
                .withButton2Text( getString(R.string.dialog_message_cancel) )
                .withDialogColor("#ff1d1d1d")
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadPictureFromGallery();
                        newDialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newDialogBuilder.dismiss();
                    }
                })
                .show();
    }

    private void showYasaDialog5(final int w, final int pos) {
        final NiftyDialogBuilder newDialogBuilder=NiftyDialogBuilder.getInstance(this);
        newDialogBuilder
                .withTitle("Yasa 2")
                .withMessage( getString(R.string.dialog_message4) )
                .withButton1Text( getString(R.string.dialog_message_ok) )
                .withButton2Text( getString(R.string.dialog_message_cancel) )
                .withDialogColor("#ff1d1d1d")
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*skipCollage();*/
                        isLoadedFromGalery = false;
                        addCollage(w, pos);
                        newDialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newDialogBuilder.dismiss();
                    }
                })
                .show();
    }

    private void showYasaDialog4() {
        final NiftyDialogBuilder dialogBuilder=NiftyDialogBuilder.getInstance(this);
        dialogBuilder
                .withTitle("Yasa 2")
                .withMessage( getString(R.string.dialog_message3) )
                .withButton1Text( getString(R.string.dialog_message_ok) )
                .withDialogColor("#ff1d1d1d")
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                })
                .show();
    }

    public boolean checkShapeBounds(int x, int y, View myView) {
        System.out.println(x + " " + y + " " + myView.getWidth() + " " + myView.getHeight() + " infos");
        return ( (x >= 0) &&
                 (y >= 0) &&
                 //(x+myView.getWidth()<=Constants.mScreenWidth) &&
                 (x+myView.getWidth()<=SURFACE_VIEW_WIDTH_3_4) &&
                 //(y+myView.getHeight()<=Constants.mScreenHeight) );
                 (y+myView.getHeight()<=SURFACE_VIEW_HEIGHT_3_4) );
    }



    public Path addBorderPath(int x, int y, int w, int h) {
        Path border = new Path();
        border.moveTo(x, y);
        border.lineTo(x + w, y);
        border.lineTo(x + w, y + h);
        border.lineTo(x, y + h);
        border.lineTo(x, y);
        border.lineTo(x + w, y);
        return border;
    }



    private Bitmap addWM(Bitmap b){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.watermark, options);
        Bitmap bmOverlay = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Matrix moveMatrix = new Matrix();
        float newX = b.getWidth() - bm.getWidth();
        float newY = b.getHeight() - bm.getHeight();
        moveMatrix.postTranslate(newX - 5, newY - 5);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(b, new Matrix(), paint);
        canvas.drawBitmap(bm, moveMatrix, paint);
        return bmOverlay;
    }
    private Bitmap mergeStickersWithCollage(Bitmap b1, Bitmap b2) {
        Bitmap bmOverlay = Bitmap.createBitmap(b1.getWidth(), b1.getHeight(), b1.getConfig());
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(b1, new Matrix(), paint);
        canvas.drawBitmap(b2, new Matrix(), paint);
        return bmOverlay;
    }
    //private void hideWM(){findViewById(R.id.yasa_wm).setVisibility(View.GONE);}

    public int getSquareCoverTopHeight() { return (getScreenContentHeight() - getActionPanelHeight() - Constants.mScreenWidth) / 2; }
    public int getSquareCoverBottomHeight() { return getSquareCoverTopHeight() + getActionPanelHeight(); }

    public int getActionPanelHeight() { return findViewById(R.id.yasaToolbars).getHeight(); }
    public int getActionPanelBackgroundColorHeight() { return findViewById(R.id.toolbar_background).getHeight(); }
    public int getScreenContentHeight() {return findViewById(R.id.cameraContent).getHeight();}


    public void SaveCollage(SaveTask.onPictureSaveListener listener) {
        //dismissProgressDialog();
        View v = findViewById(R.id.collage_type1);
        //toast(v.getY() + " " + v.getWidth() +" " + v.getHeight(), 1);
        if ( cu.checkFilled() ) {
            Bitmap b = cu.getCollage();
            Bitmap stickers = loadBitmapFromView(mContentRootView, mContentRootView.getWidth(), mContentRootView.getHeight());
            stickers = cropBitmap3(
                    stickers,
                    0,
                    (int)v.getY(),
                    v.getWidth(),
                    v.getHeight());
            b = mergeStickersWithCollage(b, stickers);
            if (!isWMarkPremium) {b = addWM(b);}
            new SaveTask(appContext,
                    Constants.getOutputMediaFile(), listener).execute(applyChangesBitmap(b, b.getWidth(), b.getHeight()));
        }
        else {dismissProgressDialog();}
    }
    public void saveYasa(SaveTask.onPictureSaveListener listener) {
        showProgressDialog(getString(R.string.save_picture));
        //if (isWMarkPremium) {hideWM();}
        HideStickersEditMode();
        if (isCollageStarted) {
            SaveCollage(listener);
        }
        else if (isLoadedFromGalery || isShuttered) {
            View v = findViewById(R.id.drawContainer);
            Bitmap b = loadBitmapFromView(v, v.getWidth(), v.getHeight());

            if (checkSquareFrameEnabled()) {
                b = cropBitmap3(
                        b,
                        0,
                        getSquareCoverTopHeight(),
                        Constants.mScreenWidth,
                        Constants.mScreenWidth);
                //toast(b.getWidth() + " " + b.getHeight(), 0);
            }
            else {
                b = cropBitmap3(b, 0, 0, Constants.mScreenWidth,
                        //getScreenContentHeight() - getActionPanelHeight());
                        getScreenContentHeight() - getActionPanelBackgroundColorHeight());

            }
            if (!isWMarkPremium) {b = addWM(b);}
            new SaveTask(appContext,
                    Constants.getOutputMediaFile(), listener).execute(applyChangesBitmap(b, b.getWidth(), b.getHeight()));
        }


    }

    public void shareYasa() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

        startShare = true;
        if (!lastSavedImage.equals("")) {
            Uri imageFile = Uri.fromFile(new File(lastSavedImage));
            Intent share = createShareIntent(imageFile);
            /*startActivity(Intent.createChooser(share, "Yasa Share"));*/
            startActivityForResult(Intent.createChooser(share, "Yasa Share"), YasaConstants.REQUEST_SHARE);
            showOnPauseScreen();
        }
        else {toast( getString(R.string.nothing_to_share) , 1);}
    }

    private Intent createShareIntent(Uri uri) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        return shareIntent;
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    private void applyShuffleFilter() {
        //0 - none; added
        //1 is divider
        int shuffeFilter = randInt(2, /*38*/MagicFilterType.FILTER_COUNT-1);
        int type = mFilterUtilsYasa.getFilters().get( shuffeFilter ).getFilterType();
        //get item, than find filtertype, than set selected
        mMagicCameraDisplay.setFilter(type);
        mFilterUtilsYasa.changeSelected(shuffeFilter-1);
        //System.out.println("wasselected " + shuffeFilter);
        if (!isPhotoShapeStarted && !isCollageStarted && isShuttered) {
            getFilterToImage(CameraEngine.nonfiltered);
        }
        else if (!isPhotoShapeStarted && !isCollageStarted && !isShuttered && isLoadedFromGalery) {
            getFilterToImage(Constant.bitmap);
        }
    }

    public void getFilterToImageDelayed(final int pos) {

        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 14;
                m.arg2 = pos;
                handler.sendMessage(m);
            }
        }, 500);
    }

    public void getFilterToImage(Bitmap b){
        _Skip_Mirror_when_Photo_is_Taken();
        Bitmap tmp = b.copy(b.getConfig(), true);
        //Bitmap tmp = CameraEngine.bitmap.copy(CameraEngine.bitmap.getConfig(), true);
        mMagicCameraDisplay.applyFilterToImageShuttered(
                tmp,
                new MagicCameraDisplay.onFilterReadyListener() {

                    @Override
                    public void onFilterReady(final Bitmap saved) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageFromGaleryView.setImageBitmap(saved);
                                //toast((saved == null) + "", 1);
                            }
                        });
                    }

                }
        );
    }

    public void getFilterToImage2Delayed() {

        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 15;
                //m.arg2 = pos;
                handler.sendMessage(m);
            }
        }, 500);
    }

    public void getFilterToImage2(Bitmap b){
        _Skip_Mirror_when_Photo_is_Taken();
        Bitmap tmp = b.copy(b.getConfig(), true);
        mMagicCameraDisplay.applyFilterToImageShuttered(
                tmp,
                new MagicCameraDisplay.onFilterReadyListener() {

                    @Override
                    public void onFilterReady(final Bitmap saved) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                collageImageView.setImageBitmap(saved);
                            }
                        });
                    }

                }
        );
    }

    public void loadPictureFromGallery() {
        startTrackingAction6OpenGallery();
        /*Intent customAlbumActivity = new Intent(Intent.ACTION_GET_CONTENT);
        //Intent customAlbumActivity = new Intent(YasaMainActivity.this, AlbumActivity.class);
        customAlbumActivity.setType("image");
        startActivityForResult(customAlbumActivity, YasaConstants.REQUEST_PICK);*/

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, YasaConstants.REQUEST_PICK);
    }

    private void changeTimerText(String text) {
        timerView.setText(text);}
    private void changeTimerViewVisibility(boolean visible) {
        if (visible) timerContent.setVisibility(View.VISIBLE);
        else timerContent.setVisibility(View.GONE);
    }

    public void openStickersBar(boolean open) {
        if (open) {
            startTrackingAction11OpenStickerBar();
            startAnyBarWithAnimation(stickersToolbar, AMIMATION_TIME);}
        if (!open) {
            hideAnyBarWithAnimation(stickersToolbar, AMIMATION_TIME);}
    }
    public void openPhotoStickersBar(boolean open) {
        if (open) {
            startTrackingAction10OpenPhotoStickerBar();
            startAnyBarWithAnimation(photoStickersToolbar, AMIMATION_TIME);}
        if (!open) {
            hideAnyBarWithAnimation(photoStickersToolbar, AMIMATION_TIME);}
    }
    public void openShapesBar(boolean open) {
        if (open) {
            startTrackingAction7OpenCollageBar();
            startAnyBarWithAnimation(toolbar3shapes, AMIMATION_TIME);}
        if (!open) {
            hideAnyBarWithAnimation(toolbar3shapes, AMIMATION_TIME);}
    }

    private void setTimerButtonImage() {
        startTrackingAction8SetTimer();

        timerState++;
        if (timerState>timerStates.length-1) timerState = 0;
        buttonTimer.setImageResource(timerStates[timerState]);
        DataUtils.setIntPreferences(getApplicationContext(), timerPreferenceName, timerState);
    }

    public void cancelTimerUpdate() {
        mMyTimerTask.cancel();
    }
    public void startTimerUpdate(int start, int repeat, int seconds) {
        counter = seconds;
        mMyTimerTask = new MyTimerTask();
        changeTimerViewVisibility(true);
        //set timer text to the start
        changeTimerText(counter + " sec");
        mTimer.schedule(mMyTimerTask, 0, repeat*1000);
    }

    int counter;
    class MyTimerTask extends TimerTask {


        String timer_text = " sec";
        @Override
        public void run() {

            if (counter == 0) {

                cancelTimerUpdate();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        changeTimerViewVisibility(false);
                        //do onShutter
                        onShutter();
                    }
                });

            }
            else {
                //System.out.println(counter);
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //System.out.println(counter);
                        int data = counter +1;//because it starts in separate thread with already counter--
                        changeTimerText(data+timer_text);
                    }
                });
            }
            counter--;
        }
    }

    private void setFlashButtonImage() {
        if (isHasFlash()) {
            startTrackingAction13SetFlash();
            turnLight();
        }
        else {System.out.println("Here");}
    }

    boolean isHasFlash() {
        Camera mCamera = mMagicCameraDisplay.onGetCamera();
        if (mCamera == null || mCamera.getParameters() == null
                || mCamera.getParameters().getSupportedFlashModes() == null) return false;
        List<String> supportedModes = mCamera.getParameters().getSupportedFlashModes();
        return supportedModes.size()>1;
    }

    private void turnLight() {
        Camera mCamera = mMagicCameraDisplay.onGetCamera();
        if (mCamera == null || mCamera.getParameters() == null
                || mCamera.getParameters().getSupportedFlashModes() == null) {
            return;
        }
        Camera.Parameters parameters = mCamera.getParameters();
        String flashMode = mCamera.getParameters().getFlashMode();
        List<String> supportedModes = mCamera.getParameters().getSupportedFlashModes();
        if (Camera.Parameters.FLASH_MODE_OFF.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            mCamera.setParameters(parameters);
            flashState = 0;
            buttonFlash.setImageResource(R.drawable.icn_flash);
        } else if (Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) {
            if (supportedModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                flashState = 1;
                buttonFlash.setImageResource(R.drawable.icn_flash_auto);
                mCamera.setParameters(parameters);
            } else if (supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                flashState = 2;
                buttonFlash.setImageResource(R.drawable.icn_flash_off);
                mCamera.setParameters(parameters);
            }
        } else if (Camera.Parameters.FLASH_MODE_AUTO.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(parameters);
            flashState = 2;
            buttonFlash.setImageResource(R.drawable.icn_flash_off);
        }
    }

    private void turnLight2(int type) {
        Camera mCamera = mMagicCameraDisplay.onGetCamera();
        if (mCamera == null || mCamera.getParameters() == null
                || mCamera.getParameters().getSupportedFlashModes() == null) {
            return;
        }
        Camera.Parameters parameters = mCamera.getParameters();
        String flashMode = mCamera.getParameters().getFlashMode();
        List<String> supportedModes = mCamera.getParameters().getSupportedFlashModes();
        /*System.out.println("modes___ " +supportedModes);*/
        if (type==0 && Camera.Parameters.FLASH_MODE_OFF.equals(flashMode)

                && supportedModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            mCamera.setParameters(parameters);
            flashState = 0;
            buttonFlash.setImageResource(R.drawable.icn_flash);
        }
        if (type==1 && Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) {
            if (supportedModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                flashState = 1;
                buttonFlash.setImageResource(R.drawable.icn_flash_auto);
                mCamera.setParameters(parameters);
            }
            else if (supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                flashState = 2;
                buttonFlash.setImageResource(R.drawable.icn_flash_off);
                mCamera.setParameters(parameters);
            }
        }
        if (type==2 && Camera.Parameters.FLASH_MODE_AUTO.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(parameters);
            flashState = 2;
            buttonFlash.setImageResource(R.drawable.icn_flash_off);
        }
    }

    public void changeCameraDelay() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 17;
                handler.sendMessage(m);
            }
        }, 300);
    }
    public void stopProcessingAnimationDelay() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 18;
                handler.sendMessage(m);
            }
        }, 2000);
    }
    public void startProcessingAnimationDelay2() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 19;
                handler.sendMessage(m);
            }
        }, 0);
    }
    private void changeCamera() {
        int numberOfCameras = Camera.getNumberOfCameras();
        if (numberOfCameras > 1) {
            /*Pause Face Detection*/
            pauseFaceDetection(1);

            if (Constant.BACK_CAMERA_IN_USE) {
                skip_Filter_Mirror();
                showFrontCamera();
                reset_Filter_Mirror2();}
            else {
                skip_Filter_Mirror();
                showBackCamera();
                reset_Filter_Mirror2();}

            startFaceProcessing(4);
            //toast("FACEDETECTION_CC", 1);

            startTrackingAction12ChangeCamera();
        }
    }

    private void showFrontCamera() {
        int numberOfCameras = Camera.getNumberOfCameras();

        if (numberOfCameras > 1) {
            mMagicCameraDisplay.onReleaseCamera();
            mMagicCameraDisplay.onOpenCameraById(Camera.CameraInfo.CAMERA_FACING_FRONT);
            Constant.BACK_CAMERA_IN_USE = false;
            CameraEngine.BACK_CAMERA_IN_USE = false;
            //mMagicCameraDisplay.onStopPreview();

            //removePreview();
            mMagicCameraDisplay.onChangeCamera();
            //attachCameraToPreview();
            buttonFlashView.setVisibility(View.GONE);//just show the no flash available
        } else {
            Toast.makeText(this, "Front camera not available", Toast.LENGTH_SHORT).show();
            /*toast("Front camera not available", 0);*/
        }
    }

    private void showBackCamera() {
        mMagicCameraDisplay.onReleaseCamera();
        //releaseCamera();
        mMagicCameraDisplay.onOpenCameraById(Camera.CameraInfo.CAMERA_FACING_BACK);
        //camera = getCameraInstance(Camera.CameraInfo.CAMERA_FACING_BACK);
        if (mMagicCameraDisplay.onGetCamera() != null) {

            Constant.BACK_CAMERA_IN_USE = true;
            CameraEngine.BACK_CAMERA_IN_USE = true;
            mMagicCameraDisplay.onChangeCamera();
            //attachCameraToPreview();

            buttonFlashView.setVisibility(View.VISIBLE);
            turnLight2(flashState);//set back camera flash state
        }
    }

    private boolean checkBackCameraRunning() {
        return (mMagicCameraDisplay.getCameraFacing() == Camera.CameraInfo.CAMERA_FACING_BACK);
    }

    private boolean isTwoCameras() {
        return mMagicCameraDisplay.getCameraNumber()==2;
    }

    private Handler stickerAddHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {


            return false;
        }
    });

    @Override
    public void onBackPressed() {
        //close the Sticker Bar when backpressed
        if (stickersToolbar.getVisibility()==View.VISIBLE) {openStickersBar(false);}
        //close the Filters Bar when backpressed
        else if (toolbarFilters.getVisibility() == View.VISIBLE) {hideAnyBarWithAnimation(toolbarFilters, AMIMATION_TIME);}
        else if (photoStickersToolbar.getVisibility() == View.VISIBLE) {openPhotoStickersBar(false);}
        else if (toolbarMEffects.getVisibility() == View.VISIBLE) {hideAnyBarWithAnimation(toolbarMEffects, AMIMATION_TIME);}
        else if (toolbar3DEffects.getVisibility() == View.VISIBLE) {hideAnyBarWithAnimation(toolbar3DEffects, AMIMATION_TIME);}
        //close the Collage Bar when backpressed
        else if (toolbar3shapes.getVisibility()==View.VISIBLE) { openShapesBar(false); }
        else super.onBackPressed();
    }

    private void startAnyBarWithAnimation(final RelativeLayout bar, int dur) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(bar, "translationY", bar.getHeight(), 0);
        animator.setDuration(dur);
        animator.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                bar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
        animator.start();
    }

    private void hideAnyBarWithAnimation(final RelativeLayout bar, int dur) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(bar, "translationY", 0, bar.getHeight());
        animator.setDuration(dur);
        animator.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                bar.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                bar.setVisibility(View.GONE);

            }
        });
        animator.start();
    }

    private View.OnClickListener startFiltersListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v.getAlpha()==1.0f) startAnyBarWithAnimation(toolbarFilters, AMIMATION_TIME);
            startTrackingAction9OpenFilterBar();
        }
    };

    private View.OnClickListener closeFiltersListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            hideAnyBarWithAnimation(toolbarFilters, AMIMATION_TIME);
        }
    };

    private View.OnClickListener startMEffectListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v.getAlpha()==1.0f) startAnyBarWithAnimation(toolbarMEffects, AMIMATION_TIME);
            startTrackingAction3Mirror();
        }
    };

    private View.OnClickListener closeMEffectListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            hideAnyBarWithAnimation(toolbarMEffects, AMIMATION_TIME);
        }
    };

    private View.OnClickListener start3DEffectListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            startAnyBarWithAnimation(toolbar3DEffects, AMIMATION_TIME);
            startTrackingAction4_3DShape();
        }
    };

    private View.OnClickListener close3DEffectListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            hideAnyBarWithAnimation(toolbar3DEffects, AMIMATION_TIME);
        }
    };

    public void addStickersToRecyclerView(int[] mystickers, boolean isAvailable, String type) {
        FiltersAdapterYasa adapter3 = new FiltersAdapterYasa(
                mystickers,
                stickerAddHandler, //remove this parameter
                isAvailable,
                this,
                type);
        if (mRecyclerViewStickers.getAdapter() instanceof FiltersAdapterYasa) {
            mRecyclerViewStickers.swapAdapter(adapter3, true);
        }
        else {mRecyclerViewStickers.setAdapter(adapter3);}
    }
    public void addShapesToRecyclerView(int[] myshapes, boolean isAvailable, String type) {
        ShapesAdapterYasa adapter3 = new ShapesAdapterYasa(
                myshapes,
                isAvailable,
                this,
                type);
        //mRecyclerViewStickers.swapAdapter(adapter3, true);
        mRecyclerViewStickers.setAdapter(adapter3);
    }

    int lastSelectedCategory=R.id.stickerCat4;//first category in the list
    int lastSelectedCategoryHighlight=R.id.stRedovCat4;//first category in the list
    public void applyCustomTypefaceToCategoryTextView(){
        Typeface tf = myFontCache.get("BebasNeueRegular.ttf", this);
        TextView myTextvView1 = (TextView)findViewById(R.id.stickerCat1);
        myTextvView1.setTypeface( tf );
        /*myTextvView1 = (TextView)findViewById(R.id.stickerCat2);
        myTextvView1.setTypeface( tf );*/
        myTextvView1 = (TextView)findViewById(R.id.stickerCat3);
        myTextvView1.setTypeface( tf );
        myTextvView1 = (TextView)findViewById(R.id.stickerCat4);
        myTextvView1.setTypeface( tf );
        myTextvView1 = (TextView)findViewById(R.id.stickerCat5);
        myTextvView1.setTypeface( tf );
        myTextvView1 = (TextView)findViewById(R.id.stickerCat6);
        myTextvView1.setTypeface( tf );
        myTextvView1 = (TextView)findViewById(R.id.stickerCat7);
        myTextvView1.setTypeface( tf );
        myTextvView1 = (TextView)findViewById(R.id.stickerCat8);
        myTextvView1.setTypeface( tf );
        /*myTextvView1 = (TextView)findViewById(R.id.stickerCat9);
        myTextvView1.setTypeface( tf );*/
        myTextvView1 = (TextView)findViewById(R.id.stickerCat10);
        myTextvView1.setTypeface( tf );
        myTextvView1 = (TextView)findViewById(R.id.stickerCat11);
        myTextvView1.setTypeface( tf );
        /*myTextvView1 = (TextView)findViewById(R.id.stickerCat12);
        myTextvView1.setTypeface( tf );*/
        myTextvView1 = (TextView)findViewById(R.id.stickerCat13);
        myTextvView1.setTypeface( tf );
        /*myTextvView1 = (TextView)findViewById(R.id.stickerCat14);
        myTextvView1.setTypeface( tf );*/

        myTextvView1 = (TextView)findViewById(R.id.photoStickerCat14);
        myTextvView1.setTypeface( tf );

        myTextvView1 = (TextView)findViewById(R.id.stickerCat15);
        myTextvView1.setTypeface( tf );
    }
    public void onSelectCategoryHelper(View myView, int[] cat, int highlightId, String type, boolean premium) {//it does dirty job
        setCategorySelected(lastSelectedCategory, false);
        setCategorySelected(myView.getId(), true);
        /*if (myView.getId()==R.id.stickerCat14) {addShapesToRecyclerView(cat, premium, type);}
        else {*/ addStickersToRecyclerView(cat, premium, type); //}
        lastSelectedCategory = myView.getId();
        setCategoryHighlightSelected(highlightId, true);
        setCategoryHighlightSelected(lastSelectedCategoryHighlight, false);
        lastSelectedCategoryHighlight = highlightId;
    }
    public void onSelectStickersCategory(View myView) {
        //Vintage
        if (myView.getId()==R.id.stickerCat1 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersVintagePreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat1, SKU_VINTAGE_STICKERS, isVintagePremium);
            return;
        }
        //Women Day
        /*if (myView.getId()==R.id.stickerCat2 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersWomenDayPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat2, SKU_WOMANS_DAY_STICKERS, isWomenDayPremium);
            return;
        }*/
        //Women Day2
        /*if (myView.getId()==R.id.stickerCat12 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersWomenDayPreview2();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat12, SKU_WOMAN_DAY2, isWomenDayPremium2);
            return;
        }*/
        //Hairstyle
        if (myView.getId()==R.id.stickerCat3 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersHairstylesPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat3, SKU_HAIRSTYLE_STICKERS, isHairstylesPremium);
            return;
        }
        //Mustache
        if (myView.getId()==R.id.stickerCat4 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersMustachePreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat4, SKU_MUSTACHE_STICKERS, true);
            return;
        }
        //Valentine Day
        if (myView.getId()==R.id.stickerCat5 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersValentineDayPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat5, SKU_VALENTINE_DAY_STICKERS, isValentineDayPremium);
            return;
        }
        //Love
        if (myView.getId()==R.id.stickerCat6 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersLovePreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat6, SKU_LOVE_STICKERS, true);
            return;
        }
        //Lips
        if (myView.getId()==R.id.stickerCat7 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersLipsPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat7, SKU_LIPS_STICKERS, true);
            return;
        }
        //Hairstyle2
        if (myView.getId()==R.id.stickerCat8 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickerHairstyles2Preview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat8, SKU_HAIRSTYLE2_STICKERS, true);
            return;
        }
        //New Year
        /*if (myView.getId()==R.id.stickerCat9 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersNewYearPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat9, SKU_NEW_YEAR_STICKERS, true);
            return;
        }*/
        //Ribbon
        if (myView.getId()==R.id.stickerCat10 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersRibbonPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat10, SKU_RIBON_STICKERS, true);
            return;
        }
        //Hat
        if (myView.getId()==R.id.stickerCat11 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersHatPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat11, SKU_HAT_STICKERS, true);
            return;
        }
        //Watermark
        if (myView.getId()==R.id.stickerCat13 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersWMarkPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat13, SKU_WMARK, isWMarkPremium);
            return;
        }
        //Glasses
        if (myView.getId()==R.id.stickerCat15 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getStickersGlassesPreview();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat15, SKU_GLASSES_STICKERS, true);
            return;
        }


        //Shape
        /*if (myView.getId()==R.id.stickerCat14 && lastSelectedCategory!=myView.getId()) {
            int[] cat = YasaStickers.getShapes2();
            onSelectCategoryHelper(myView, cat, R.id.stRedovCat14, _SKU_SHAPE, true);
            return;
        }*/

    }
    public void setCategorySelected(int id, boolean pressed) {
        TextView category = (TextView)findViewById(id);
        if (id!=-1) {
            if (pressed)  category.setTextColor(Color.parseColor("#7f837f"));
            else category.setTextColor(Color.parseColor("#bdc3c7"));
        }
    }
    public void setCategoryHighlightSelected(int id, boolean pressed) {
        if(pressed) findViewById(id).setVisibility(View.VISIBLE);
        else findViewById(id).setVisibility(View.GONE);
    }

    /**
     * The code bellow related to Google Biling Service
     */
    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    public void startStickersBuying(int t) {
        switch (t) {
            case 0:
                onBuyItemValentineDay();startPurchase=true;
                break;
            case 1:
                onBuyItemWomansDay();startPurchase=true;
                break;
            case 2:
                onBuyItemWomansDay2();startPurchase=true;
                break;
            case 3:
                onBuyItemVintage();startPurchase=true;
                break;
            case 4:
                onBuyItemHairstyle();startPurchase=true;
                break;
            case 5:
                onBuyWMark(SKU_WMARK);startPurchase=true;
                break;
            default:
                return;
        }
    }
    @Override
    public void onMethodCallback(String t, int position) {
        /*System.out.println("myselected_item " +t+" " +position);*/
        //valentine day
        if (t.equals(SKU_VALENTINE_DAY_STICKERS) ) {
            if (!isValentineDayPremium) {initAdsView(2,0,position);/*onBuyItemValentineDay();startPurchase=true;*/}
            else {//if we own
                Addon selectedSticker = new Addon( YasaStickers.getStickersValentineDay()[position] );
                addSticker( selectedSticker );
            }
        }
        //women day
        if (t.equals(SKU_WOMANS_DAY_STICKERS)) {
            if (!isWomenDayPremium) {initAdsView(2,1,position);/*onBuyItemWomansDay();startPurchase=true;*/}
            else {//if we own
                Addon selectedSticker = new Addon( YasaStickers.getStickersWomenDay()[position] );
                addSticker( selectedSticker );
            }
        }
        //women day2
        if (t.equals(SKU_WOMAN_DAY2)) {
            if (!isWomenDayPremium2) {initAdsView(2,2,position);/*onBuyItemWomansDay2();startPurchase=true;*/}
            else {//if we own
                Addon selectedSticker = new Addon( YasaStickers.getStickersWomenDay2()[position] );
                addSticker( selectedSticker );
            }
        }

        if (t.equals(SKU_VINTAGE_STICKERS)) {
            if (!isVintagePremium) {initAdsView(2,3,position);/*onBuyItemVintage();startPurchase=true;*/}
            else {//if we own
                Addon selectedSticker = new Addon( YasaStickers.getStickersVintage()[position] );
                addSticker( selectedSticker );
            }
        }
        if (t.equals(SKU_HAIRSTYLE_STICKERS)) {
            if (!isHairstylesPremium) {initAdsView(2,4,position);/*onBuyItemHairstyle();startPurchase=true;*/}
            else {//if we own
                Addon selectedSticker = new Addon( YasaStickers.getStickersHairstyles()[position] );
                addSticker( selectedSticker );
            }
        }
        //mustache
        if (t.equals(SKU_MUSTACHE_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersMustache()[position] );
            addSticker( selectedSticker );
        }
        //love
        if (t.equals(SKU_LOVE_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersLove()[position] );
            addSticker( selectedSticker );
        }
        //lips
        if (t.equals(SKU_LIPS_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersLips()[position] );
            addSticker( selectedSticker );
        }
        //hairstyle
        if (t.equals(SKU_HAIRSTYLE2_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersHairstyles2()[position] );
            addSticker( selectedSticker );
        }
        //new year
        if (t.equals(SKU_NEW_YEAR_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersNewYear()[position] );
            addSticker( selectedSticker );
        }
        //hat
        if (t.equals(SKU_HAT_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersHat()[position] );
            addSticker( selectedSticker );
        }
        //ribbon
        if (t.equals(SKU_RIBON_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersRibbon()[position] );
            addSticker( selectedSticker );
        }
        //watermark
        if (t.equals(SKU_WMARK)) {
            if (!isWMarkPremium) {initAdsView(2,5,position);/*onBuyWMark(SKU_WMARK);startPurchase=true;*/}
            else {
                /*Addon selectedSticker = new Addon(YasaStickers.getStickersWMark()[position]);
                addSticker(selectedSticker);*/
            }
        }
        //glasses
        if (t.equals(SKU_GLASSES_STICKERS)) {
            Addon selectedSticker = new Addon( YasaStickers.getStickersGlasses()[position] );
            addSticker( selectedSticker );
        }

    }

    @Override
    public void onShapesCallback(String t, int position) {
        if (t.equals(_SKU_SHAPE)) {
            if (!isPhotoShapesPremium && position!=0) {
                initAdsView(0,position);
                /*onBuyItemPhotoSstickers_();
                startPurchase=true;*/
            }
            else {//if we own
                mMagicCameraDisplay.onResume();
                startPhotoShapeDelayed(position);
                //addShape(position);
            }
        }

    }

    public void startPhotoShapeDelayed(final int pos) {
        mMagicCameraDisplay.setFilter(CUrrentFilterType);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                Message m = new Message();
                m.arg1 = 12;
                m.arg2 = pos;
                handler.sendMessage(m);
            }
        }, 500);
    }

    //different methods just in case
    public void onBuyItemWomansDay2() {
        String payload = "payload21";
        mHelper.launchPurchaseFlow(this, SKU_WOMAN_DAY2, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }
    //different methods just in case
    public void onBuyItemVintage() {
        String payload = "payload22";
        mHelper.launchPurchaseFlow(this, SKU_VINTAGE_STICKERS, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }
    //different methods just in case
    public void onBuyItemHairstyle() {
        String payload = "payload23";
        mHelper.launchPurchaseFlow(this, SKU_HAIRSTYLE_STICKERS, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    public void onBuyItemPhotoSstickers_() {
        String payload = "payload23";
        mHelper.launchPurchaseFlow(this, _SKU_SHAPE, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }


    public void onBuyItemCollageCircular() {
        String payload = "payload23";
        mHelper.launchPurchaseFlow(this, SKU_COLLAGE_CIRCULAR, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    public void onBuyItemWomansDay() {
        /*if (!mHelper.subscriptionsSupported()) {
            toast("Subscriptions not supported on your device yet. Sorry!", 1);
            return;
        }*/
        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = "payload24";
        mHelper.launchPurchaseFlow(this, SKU_WOMANS_DAY_STICKERS, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    public void onBuyItemValentineDay() {
        String payload = "payload25";
        mHelper.launchPurchaseFlow(this, SKU_VALENTINE_DAY_STICKERS, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    public void onBuyWMark(String SKU) {
        String payload = "payload26";
        mHelper.launchPurchaseFlow(this, SKU, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                System.out.println("Error purchasing: " + result);
                //toast("Error purchasing: " + result, 1);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                toast("Error purchasing. Authenticity verification failed.", 1);
                return;
            }

            if (purchase.getSku().equals(SKU_WOMANS_DAY_STICKERS)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_VALENTINE_DAY_STICKERS)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                //alert("Thank you for upgrading to premium!");
                //mIsPremium = true;
                //updateUi();
                //setWaitScreen(false);
            }
            else if (purchase.getSku().equals(SKU_VINTAGE_STICKERS)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_WOMAN_DAY2)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_HAIRSTYLE_STICKERS)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_WMARK)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(_SKU_SHAPE)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_COLLAGE_CIRCULAR)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                if (purchase.getSku().equals(SKU_WOMANS_DAY_STICKERS)) {
                    isWomenDayPremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker1PreferenceName, isWomenDayPremium);
                    //save to database
                    addStickersToRecyclerView(
                            YasaStickers.getStickersWomenDayPreview(),
                            isWomenDayPremium,
                            SKU_WOMANS_DAY_STICKERS);
                }
                if (purchase.getSku().equals(SKU_VALENTINE_DAY_STICKERS)) {
                    isValentineDayPremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker2PreferenceName, isValentineDayPremium);
                    //save to database
                    addStickersToRecyclerView(
                            YasaStickers.getStickersValentineDayPreview(),
                            isValentineDayPremium,
                            SKU_VALENTINE_DAY_STICKERS);
                }
                if (purchase.getSku().equals(SKU_VINTAGE_STICKERS)) {
                    isVintagePremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker3PreferenceName, isVintagePremium);
                    addStickersToRecyclerView(
                            YasaStickers.getStickersVintagePreview(),
                            isVintagePremium,
                            SKU_VINTAGE_STICKERS);
                }
                if (purchase.getSku().equals(SKU_WOMAN_DAY2)) {
                    isWomenDayPremium2 = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker5PreferenceName, isWomenDayPremium2);
                    addStickersToRecyclerView(
                            YasaStickers.getStickersWomenDayPreview2(),
                            isWomenDayPremium2,
                            SKU_WOMAN_DAY2);
                }
                if (purchase.getSku().equals(SKU_HAIRSTYLE_STICKERS)) {
                    isHairstylesPremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker4PreferenceName, isHairstylesPremium);
                    addStickersToRecyclerView(
                            YasaStickers.getStickersHairstylesPreview(),
                            isHairstylesPremium,
                            SKU_HAIRSTYLE_STICKERS);
                }
                if (purchase.getSku().equals(SKU_WMARK)) {
                    isWMarkPremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker6PreferenceName, isWMarkPremium);
                    //save to database
                    addStickersToRecyclerView(
                            YasaStickers.getStickersWMarkPreview(),
                            isWMarkPremium,
                            SKU_WMARK);
                }
                if (purchase.getSku().equals(_SKU_SHAPE)) {
                    isPhotoShapesPremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker7PreferenceName, isPhotoShapesPremium);
                    //save to database
                    addShapesToRecyclerView(
                            YasaStickers.getShapes2(),
                            isPhotoShapesPremium,
                            _SKU_SHAPE);
                }
                if (purchase.getSku().equals(SKU_COLLAGE_CIRCULAR)) {
                    isCollageCircularPremium = true;
                    DataUtils.setBooleanPreferences(getApplicationContext(), sticker8PreferenceName, isCollageCircularPremium);
                    //save to database

                    FiltersAdapterYasaCollage adapter3 = new FiltersAdapterYasaCollage(
                            YasaStickers.getShapes(),
                            myOnCollageCallback,
                            isCollageCircularPremium,
                            SKU_COLLAGE_CIRCULAR);
                    mRecyclerView.swapAdapter(adapter3, true);

                }
                //mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                //saveData();
                //alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            }
            else {
                toast("Error while consuming: " + result, 1);
            }
        }
    };

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                toast("Failed to query inventory: " + result, 1);
                return;
            }

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the womans day stickers subscribe?
            Purchase womans_day = inventory.getPurchase(SKU_WOMANS_DAY_STICKERS);
            isWomenDayPremium = (womans_day != null && verifyDeveloperPayload(womans_day));
            DataUtils.setBooleanPreferences(getApplicationContext(), sticker1PreferenceName, isWomenDayPremium);
            //boolean data = (womans_day != null && verifyDeveloperPayload(womans_day));
            //System.out.println("items we have isWomenDayPremium " + data);
            //Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));

            // Do we have the valentine day subscribe?
            Purchase valentine_day_purchase = inventory.getPurchase(SKU_VALENTINE_DAY_STICKERS);
            isValentineDayPremium = (valentine_day_purchase != null && verifyDeveloperPayload(valentine_day_purchase));
            DataUtils.setBooleanPreferences(getApplicationContext(), sticker2PreferenceName, isValentineDayPremium);
            //boolean data2 = (valentine_day_purchase != null && verifyDeveloperPayload(valentine_day_purchase));
            //System.out.println("items we have isValentineDayPremium " + data2);

            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
            /*Purchase gasPurchase = inventory.getPurchase(SKU_GAS);
            if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                //Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_GAS), mConsumeFinishedListener);
                return;
            }*/
        }
    };

    //CAPTURE FROM GLSURFACEVIEW
    private interface BitmapReadyCallbacks {
        void onBitmapReady(/*Bitmap bitmap*/);
    }
    private void captureBitmap(final BitmapReadyCallbacks bitmapReadyCallbacks) {
        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                EGL10 egl = (EGL10) EGLContext.getEGL();
                GL10 gl = (GL10)egl.eglGetCurrentContext().getGL();
                CameraEngine.captured = createBitmapFromGLSurface(0, 0, glSurfaceView.getWidth(), glSurfaceView.getHeight(), gl);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bitmapReadyCallbacks.onBitmapReady(/*snapshotBitmap*/);
                    }
                });

            }
        });

    }

    private void captureBitmap2(final BitmapReadyCallbacks bitmapReadyCallbacks) {
        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                EGL10 egl = (EGL10) EGLContext.getEGL();
                GL10 gl = (GL10) egl.eglGetCurrentContext().getGL();
                CameraEngine.nonfiltered = createBitmapFromGLSurface(0, 0, glSurfaceView.getWidth(), glSurfaceView.getHeight(), gl);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bitmapReadyCallbacks.onBitmapReady();
                    }
                });

            }
        });

    }
    private Bitmap createBitmapFromGLSurface(int x, int y, int w, int h, GL10 gl) {

        int bitmapBuffer[] = new int[w * h];
        int bitmapSource[] = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            Log.e("onGLSave", "createBitmapFromGLSurface: " + e.getMessage(), e);
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

    public void startTrackingAction2Save() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Save")
                .build());
    }
    public void startTrackingAction3Mirror() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Mirror")
                .build());
    }
    public void startTrackingAction4_3DShape() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("3D Shape")
                .build());
    }
    public void startTrackingAction5TakeAPhoto() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Take a Photo")
                .build());
    }
    public void startTrackingAction6OpenGallery() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Open Gallery")
                .build());
    }
    public void startTrackingAction7OpenCollageBar() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Open Collage Bar")
                .build());
    }
    public void startTrackingAction8SetTimer() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Set Timer")
                .build());
    }
    public void startTrackingAction9OpenFilterBar() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Open Filter Bar")
                .build());
    }
    public void startTrackingAction10OpenPhotoStickerBar() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Open Photo Sticker Bar")
                .build());
    }

    public void startTrackingAction11OpenStickerBar() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Open Sticker Bar")
                .build());
    }
    public void startTrackingAction12ChangeCamera() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Change Camera")
                .build());
    }
    public void startTrackingAction13SetFlash() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Set Flash")
                .build());
    }

    public void askWritePermission(int request_type) {
        int permissionCheck = ContextCompat.checkSelfPermission(appContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    request_type);
        }
        else {
            if (request_type==YasaConstants.REQUEST_PERMISSION_SAVE_BEFORE_SHARE) saveYasa(myOnImageListener3);
            if (request_type==YasaConstants.REQUEST_PERMISSION_SAVE) saveYasa(myOnImageListener2);
        }
    }
}
