package com.getyasa.activities;

import com.getyasa.R;

/**
 * Created by Vasilkoff on 15.02.2016.
 */
public class YasaStickers {

    static int[] mustache_stickers_preview = {
            R.drawable.m1_preview,
            R.drawable.m2_preview,
            R.drawable.m3_preview,
            R.drawable.m4_preview,
            R.drawable.m5_preview,
            R.drawable.m6_preview,
            R.drawable.m7_preview,
            R.drawable.m8_preview,
            R.drawable.m9_preview,
            R.drawable.m10_preview,
            R.drawable.m11_preview,
            R.drawable.m12_preview
    };

    static int[] mustache_stickers = {
            R.drawable.m1,
            R.drawable.m2,
            R.drawable.m3,
            R.drawable.m4,
            R.drawable.m5,
            R.drawable.m6,
            R.drawable.m7,
            R.drawable.m8,
            R.drawable.m9,
            R.drawable.m10,
            R.drawable.m11,
            R.drawable.m12
    };
    public static int[] getStickersMustache() {return mustache_stickers;}
    public static int[] getStickersMustachePreview() {return mustache_stickers_preview;}

    static int[] vintage_stickers = {
            R.drawable.sticker1,
            R.drawable.sticker2,
            R.drawable.sticker3,
            R.drawable.sticker4,
            R.drawable.sticker6,
            R.drawable.sticker7,
            R.drawable.sticker8,
            R.drawable.sticker9,
            R.drawable.sticker11,
            R.drawable.sticker12
    };

    static int[] vintage_stickers_preview = {
            R.drawable.sticker1_preview,
            R.drawable.sticker2_preview,
            R.drawable.sticker3_preview,
            R.drawable.sticker4_preview,
            R.drawable.sticker6_preview,
            R.drawable.sticker7_preview,
            R.drawable.sticker8_preview,
            R.drawable.sticker9_preview,
            R.drawable.sticker11_preview,
            R.drawable.sticker12_preview
    };
    public static int[] getStickersVintage() {return vintage_stickers;}
    public static int[] getStickersVintagePreview() {return vintage_stickers_preview;}

    static int[] stickers_women_day = {
            R.drawable.woman_sticker1,
            R.drawable.woman_sticker2,
            R.drawable.woman_sticker3,
            R.drawable.woman_sticker4
    };
    static int[] stickers_women_day_preview = {
            R.drawable.woman_sticker1_preview,
            R.drawable.woman_sticker2_preview,
            R.drawable.woman_sticker3_preview,
            R.drawable.woman_sticker4_preview
    };
    static int[] stickers_women_day2 = {
            R.drawable.woman_sticker5,
            R.drawable.woman_sticker6,
            R.drawable.woman_sticker7,
            R.drawable.woman_sticker8
    };
    static int[] stickers_women_day_preview2 = {
            R.drawable.woman_sticker5_preview,
            R.drawable.woman_sticker6_preview,
            R.drawable.woman_sticker7_preview,
            R.drawable.woman_sticker8_preview
    };
    public static int[] getStickersWomenDay() {return stickers_women_day;}
    public static int[] getStickersWomenDayPreview() {return stickers_women_day_preview;}
    public static int[] getStickersWomenDay2() {return stickers_women_day2;}
    public static int[] getStickersWomenDayPreview2() {return stickers_women_day_preview2;}

    static int[] stickers_valentine_day = {
            R.drawable.valentine_sticker1,
            R.drawable.valentine_sticker2,
            R.drawable.valentine_sticker3,
            R.drawable.valentine_sticker4,
            R.drawable.valentine_sticker5,
            R.drawable.valentine_sticker6,
            R.drawable.valentine_sticker7,
            R.drawable.valentine_sticker8,
            R.drawable.valentine_sticker9,
            R.drawable.valentine_sticker10,
            R.drawable.valentine_sticker11,
            R.drawable.valentine_sticker12,
            R.drawable.valentine_sticker13,
            R.drawable.valentine_sticker14,
            R.drawable.valentine_sticker16,
            R.drawable.valentine_sticker17,
            R.drawable.valentine_sticker18,
    };
    static int[] stickers_valentine_day_preview = {
            R.drawable.valentine_sticker1_preview,
            R.drawable.valentine_sticker2_preview,
            R.drawable.valentine_sticker3_preview,
            R.drawable.valentine_sticker4_preview,
            R.drawable.valentine_sticker5_preview,
            R.drawable.valentine_sticker6_preview,
            R.drawable.valentine_sticker7_preview,
            R.drawable.valentine_sticker8_preview,
            R.drawable.valentine_sticker9_preview,
            R.drawable.valentine_sticker10_preview,
            R.drawable.valentine_sticker11_preview,
            R.drawable.valentine_sticker12_preview,
            R.drawable.valentine_sticker13_preview,
            R.drawable.valentine_sticker14_preview,
            R.drawable.valentine_sticker16_preview,
            R.drawable.valentine_sticker17_preview,
            R.drawable.valentine_sticker18_preview,
    };
    public static int[] getStickersValentineDay() {return stickers_valentine_day;}
    public static int[] getStickersValentineDayPreview() {return stickers_valentine_day_preview;}

    static int[] stickers_hairstyles = {
            R.drawable.p1,         R.drawable.p2,         R.drawable.p3,        R.drawable.p4,
            R.drawable.p5,         R.drawable.p6,         R.drawable.p7,        R.drawable.p8,
            R.drawable.p9,         R.drawable.p10,        R.drawable.p11,       R.drawable.p12,
            R.drawable.p13,        R.drawable.p14,        R.drawable.p15,       R.drawable.p16,
    };
    static int[] stickers_hairstyles_preview = {
            R.drawable.p1_preview,
            R.drawable.p2_preview,
            R.drawable.p3_preview,
            R.drawable.p4_preview,
            R.drawable.p5_preview,
            R.drawable.p6_preview,
            R.drawable.p7_preview,
            R.drawable.p8_preview,
            R.drawable.p9_preview,
            R.drawable.p10_preview,
            R.drawable.p11_preview,
            R.drawable.p12_preview,
            R.drawable.p13_preview,
            R.drawable.p14_preview,
            R.drawable.p15_preview,
            R.drawable.p16_preview
    };
    public static int[] getStickersHairstyles() {return stickers_hairstyles;}
    public static int[] getStickersHairstylesPreview() {return stickers_hairstyles_preview;}

    static int[] stickers_love = {
            R.drawable.love1,      R.drawable.love2,      R.drawable.love3,     R.drawable.love4,
            R.drawable.love5,      R.drawable.love6,      R.drawable.love7,     R.drawable.love8,
            R.drawable.love9,      R.drawable.love10,     R.drawable.love11,    R.drawable.love12
    };

    static int[] stickers_love_preview = {
            R.drawable.love1_preview,
            R.drawable.love2_preview,
            R.drawable.love3_preview,
            R.drawable.love4_preview,
            R.drawable.love5_preview,
            R.drawable.love6_preview,
            R.drawable.love7_preview,
            R.drawable.love8_preview,
            R.drawable.love9_preview,
            R.drawable.love10_preview,
            R.drawable.love11_preview,
            R.drawable.love12_preview
    };
    public static int[] getStickersLove() {return stickers_love;}
    public static int[] getStickersLovePreview() {return stickers_love_preview;}

    static int[] stickers_lips = {
            R.drawable.lips1,      R.drawable.lips2,      R.drawable.lips3,     R.drawable.lips4,
            R.drawable.lips5,      R.drawable.lips6,      R.drawable.lips7,     R.drawable.lips8,
            R.drawable.lips9,      R.drawable.lips10,     R.drawable.lips11
    };
    static int[] stickers_lips_preview = {
            R.drawable.lips1_preview,
            R.drawable.lips2_preview,
            R.drawable.lips3_preview,
            R.drawable.lips4_preview,
            R.drawable.lips5_preview,
            R.drawable.lips6_preview,
            R.drawable.lips7_preview,
            R.drawable.lips8_preview,
            R.drawable.lips9_preview,
            R.drawable.lips10_preview,
            R.drawable.lips11_preview
    };
    public static int[] getStickersLips() {return stickers_lips;}
    public static int[] getStickersLipsPreview() {return stickers_lips_preview;}

    static int[] stickers_new_year = {
            R.drawable.ny1,        R.drawable.ny2,        R.drawable.ny3,       R.drawable.ny4,
            R.drawable.ny5,        R.drawable.ny6,        R.drawable.ny7,       R.drawable.ny8,
            R.drawable.ny9,        R.drawable.ny10,       R.drawable.ny21,      R.drawable.ny12,
            R.drawable.ny13,       R.drawable.ny14,       R.drawable.ny15,      R.drawable.ny16,
            R.drawable.ny17,       R.drawable.ny18,       R.drawable.ny19,      R.drawable.ny20,
            R.drawable.ny21,       R.drawable.ny22,       R.drawable.ny23,      R.drawable.ny24,
            R.drawable.ny25
    };
    static int[] stickers_new_year_preview = {
            R.drawable.ny1_preview,
            R.drawable.ny2_preview,
            R.drawable.ny3_preview,
            R.drawable.ny4_preview,
            R.drawable.ny5_preview,
            R.drawable.ny6_preview,
            R.drawable.ny7_preview,
            R.drawable.ny8_preview,
            R.drawable.ny9_preview,
            R.drawable.ny10_preview,
            R.drawable.ny21_preview,
            R.drawable.ny12_preview,
            R.drawable.ny13_preview,
            R.drawable.ny14_preview,
            R.drawable.ny15_preview,
            R.drawable.ny16_preview,
            R.drawable.ny17_preview,
            R.drawable.ny18_preview,
            R.drawable.ny19_preview,
            R.drawable.ny20_preview,
            R.drawable.ny21_preview,
            R.drawable.ny22_preview,
            R.drawable.ny23_preview,
            R.drawable.ny24_preview,
            R.drawable.ny25_preview
    };

    public static int[] getStickersNewYear() {return stickers_new_year;}
    public static int[] getStickersNewYearPreview() {return stickers_new_year_preview;}

    static int[] stickers_hat = {
            R.drawable.h1,         R.drawable.h2,         R.drawable.h3,        R.drawable.h4,
            R.drawable.h5,         R.drawable.h6,         R.drawable.h7,        R.drawable.h8,
            R.drawable.h9,         R.drawable.h10,        R.drawable.h11,       R.drawable.h12,
            R.drawable.h13,        R.drawable.h14,        R.drawable.h15,       R.drawable.h16
    };
    static int[] stickers_hat_preview = {
            R.drawable.h1_preview,
            R.drawable.h2_preview,
            R.drawable.h3_preview,
            R.drawable.h4_preview,
            R.drawable.h5_preview,
            R.drawable.h6_preview,
            R.drawable.h7_preview,
            R.drawable.h8_preview,
            R.drawable.h9_preview,
            R.drawable.h10_preview,
            R.drawable.h11_preview,
            R.drawable.h12_preview,
            R.drawable.h13_preview,
            R.drawable.h14_preview,
            R.drawable.h15_preview,
            R.drawable.h16_preview
    };
    public static int[] getStickersHat() {return stickers_hat;}
    public static int[] getStickersHatPreview() {return stickers_hat_preview;}

    static int[] stickers_hairstyles2 = {
            R.drawable.p17,        R.drawable.p18,        R.drawable.p19,       R.drawable.p20,
            R.drawable.p21,        R.drawable.p22,        R.drawable.p23,       R.drawable.p24,
            R.drawable.p25,        R.drawable.p26
    };
    static int[] stickers_hairstyles2_preview = {
            R.drawable.p17_preview,
            R.drawable.p18_preview,
            R.drawable.p19_preview,
            R.drawable.p20_preview,
            R.drawable.p21_preview,
            R.drawable.p22_preview,
            R.drawable.p23_preview,
            R.drawable.p24_preview,
            R.drawable.p25_preview,
            R.drawable.p26_preview
    };
    public static int[] getStickersHairstyles2() {return stickers_hairstyles2;}
    public static int[] getStickerHairstyles2Preview() {return stickers_hairstyles2_preview;}

    static int[] stickers_ribbon = {
            R.drawable.sticker25,  R.drawable.sticker26,  R.drawable.sticker27, R.drawable.sticker29,
            R.drawable.sticker31,
            R.drawable.sticker32
    };
    static int[] stickers_ribbon_preview = {
            R.drawable.sticker25_preview,
            R.drawable.sticker26_preview,
            R.drawable.sticker27_preview,
            R.drawable.sticker29_preview,
            R.drawable.sticker31_preview,
            R.drawable.sticker32_preview
    };
    public static int[] getStickersRibbon() {return stickers_ribbon;}
    public static int[] getStickersRibbonPreview() {return stickers_ribbon_preview;}

    public static int[] getStickersGlasses() {
        return glasses_sticker;
    }

    static int[] glasses_sticker = {
            R.drawable.glasses_34,
            R.drawable.glasses_35,
            R.drawable.glasses_36,
            R.drawable.glasses_37,
            R.drawable.glasses_38,
            R.drawable.glasses_39,
            R.drawable.glasses_40,
            R.drawable.glasses_41,
            R.drawable.glasses_42,
            R.drawable.glasses_43,
            R.drawable.glasses_44,
            R.drawable.glasses_45,
            R.drawable.glasses_46,
            R.drawable.glasses_47,
            R.drawable.glasses_48,

    };

    public static int[] getStickersGlassesPreview() {
        return glasses_sticker_preview;
    }

    static int[] glasses_sticker_preview = {
            R.drawable.glasses_34_preview,
            R.drawable.glasses_35_preview,
            R.drawable.glasses_36_preview,
            R.drawable.glasses_37_preview,
            R.drawable.glasses_38_preview,
            R.drawable.glasses_39_preview,
            R.drawable.glasses_40_preview,
            R.drawable.glasses_41_preview,
            R.drawable.glasses_42_preview,
            R.drawable.glasses_43_preview,
            R.drawable.glasses_44_preview,
            R.drawable.glasses_45_preview,
            R.drawable.glasses_46_preview,
            R.drawable.glasses_47_preview,
            R.drawable.glasses_48_preview,

    };

    static int[] wmark = { R.drawable.watermark };
    static int[] wmark_preview= { R.drawable.watermark_preview};
    public static int[] getStickersWMark() {return wmark;}
    public static int[] getStickersWMarkPreview() {return wmark_preview;}

    static int[] stickers = {
            R.drawable.sticker1_preview,
            R.drawable.sticker2_preview,
            R.drawable.sticker3_preview,
            R.drawable.sticker4_preview,
            R.drawable.sticker6_preview,
            R.drawable.sticker7_preview,
            R.drawable.sticker8_preview,
            R.drawable.sticker9_preview,
            R.drawable.sticker11_preview,
            R.drawable.sticker12_preview,
            R.drawable.sticker14_preview,
            R.drawable.sticker15_preview,
            R.drawable.sticker25_preview,
            R.drawable.sticker26_preview,
            R.drawable.sticker27_preview,
            R.drawable.sticker29_preview,
            R.drawable.sticker31_preview,
            R.drawable.sticker32_preview,

            /*R.drawable.m1_preview,
            R.drawable.m2_preview,
            R.drawable.m3_preview,
            R.drawable.m4_preview,
            R.drawable.m5_preview,
            R.drawable.m6_preview,
            R.drawable.m7_preview,
            R.drawable.m8_preview,
            R.drawable.m9_preview,
            R.drawable.m10_preview,
            R.drawable.m11_preview,
            R.drawable.m12_preview,*/

            /*R.drawable.m1_preview_purchase,
            R.drawable.m2_preview_purchase,
            R.drawable.m3_preview_purchase,
            R.drawable.m4_preview_purchase,
            R.drawable.m5_preview_purchase,
            R.drawable.m6_preview_purchase,
            R.drawable.m7_preview_purchase,
            R.drawable.m8_preview_purchase,
            R.drawable.m9_preview_purchase,
            R.drawable.m10_preview_purchase,
            R.drawable.m11_preview_purchase,
            R.drawable.m12_preview_purchase,*/

            R.drawable.ny1_preview,
            R.drawable.ny2_preview,
            R.drawable.ny3_preview,
            R.drawable.ny4_preview,
            R.drawable.ny5_preview,
            R.drawable.ny6_preview,
            R.drawable.ny7_preview,
            R.drawable.ny8_preview,
            R.drawable.ny9_preview,
            R.drawable.ny10_preview,
            R.drawable.ny21_preview,
            R.drawable.ny12_preview,
            R.drawable.ny13_preview,
            R.drawable.ny14_preview,
            R.drawable.ny15_preview,
            R.drawable.ny16_preview,
            R.drawable.ny17_preview,
            R.drawable.ny18_preview,
            R.drawable.ny19_preview,
            R.drawable.ny20_preview,
            R.drawable.ny21_preview,
            R.drawable.ny22_preview,
            R.drawable.ny23_preview,
            R.drawable.ny24_preview,
            R.drawable.ny25_preview,

            R.drawable.p1_preview,
            R.drawable.p2_preview,
            R.drawable.p3_preview,
            R.drawable.p4_preview,
            R.drawable.p5_preview,
            R.drawable.p6_preview,
            R.drawable.p7_preview,
            R.drawable.p8_preview,
            R.drawable.p9_preview,
            R.drawable.p10_preview,
            R.drawable.p11_preview,
            R.drawable.p12_preview,
            R.drawable.p13_preview,
            R.drawable.p14_preview,
            R.drawable.p15_preview,
            R.drawable.p16_preview,
            R.drawable.p17_preview,
            R.drawable.p18_preview,
            R.drawable.p19_preview,
            R.drawable.p20_preview,
            R.drawable.p21_preview,
            R.drawable.p22_preview,
            R.drawable.p23_preview,
            R.drawable.p24_preview,
            R.drawable.p25_preview,
            R.drawable.p26_preview,

            R.drawable.love1_preview,
            R.drawable.love2_preview,
            R.drawable.love3_preview,
            R.drawable.love4_preview,
            R.drawable.love5_preview,
            R.drawable.love6_preview,
            R.drawable.love7_preview,
            R.drawable.love8_preview,
            R.drawable.love9_preview,
            R.drawable.love10_preview,
            R.drawable.love11_preview,
            R.drawable.love12_preview,

            R.drawable.lips1_preview,
            R.drawable.lips2_preview,
            R.drawable.lips3_preview,
            R.drawable.lips4_preview,
            R.drawable.lips5_preview,
            R.drawable.lips6_preview,
            R.drawable.lips7_preview,
            R.drawable.lips8_preview,
            R.drawable.lips9_preview,
            R.drawable.lips10_preview,
            R.drawable.lips11_preview,

            R.drawable.h1_preview,
            R.drawable.h2_preview,
            R.drawable.h3_preview,
            R.drawable.h4_preview,
            R.drawable.h5_preview,
            R.drawable.h6_preview,
            R.drawable.h7_preview,
            R.drawable.h8_preview,
            R.drawable.h9_preview,
            R.drawable.h10_preview,
            R.drawable.h11_preview,
            R.drawable.h12_preview,
            R.drawable.h13_preview,
            R.drawable.h14_preview,
            R.drawable.h15_preview,
            R.drawable.h16_preview

    };

    public static int[] getStickers() {
        return stickers;
    }

    static int[] collages = {
            R.drawable.shape_sq,
            R.drawable.shape_re,
            R.drawable.collage1_preview,
            R.drawable.collage2_preview,
            R.drawable.collage5_preview,
            R.drawable.collage3_preview,
            R.drawable.collage4_preview
    };


    static int[] shapes = {
            R.drawable.shape3_preview,
            R.drawable.shape4_preview,
            R.drawable.shape5_preview,
            R.drawable.shape6_preview,
            R.drawable.shape7_preview,
            R.drawable.shape8_preview
    };

    public static int[] getShapes() {return collages;}
    public static int[] getShapes2() {return shapes;}

    public static int[] m_effects = {
            R.drawable.mirror1_preview,
            R.drawable.mirror2_preview,
            R.drawable.mirror3_preview,
            R.drawable.mirror4_preview,
            //R.drawable.mirror5_preview,
            //R.drawable.mirror6_preview
    };

    public static int[] _3dshape_effects = {
            R.drawable.tshapelogo,
            R.drawable.tshapelogo,
            R.drawable.tshapelogo,
            R.drawable.tshapelogo,
            R.drawable.tshapelogo
    };

    public static int[] getM_effects() {return m_effects;}
    public static int[] get3DShape_effects() {return _3dshape_effects;}

    static int[] stickers_purchased = {
            R.drawable.sticker1_preview,
            R.drawable.sticker2_preview,
            R.drawable.sticker3_preview,
            R.drawable.sticker4_preview,
            R.drawable.sticker6_preview,
            R.drawable.sticker7_preview,
            R.drawable.sticker8_preview,
            R.drawable.sticker9_preview,
            R.drawable.sticker11_preview,
            R.drawable.sticker12_preview,
            R.drawable.sticker14_preview,
            R.drawable.sticker15_preview,
            R.drawable.sticker25_preview,
            R.drawable.sticker26_preview,
            R.drawable.sticker27_preview,
            R.drawable.sticker29_preview,
            R.drawable.sticker31_preview,
            R.drawable.sticker32_preview,

            R.drawable.m1_preview,
            R.drawable.m2_preview,
            R.drawable.m3_preview,
            R.drawable.m4_preview,
            R.drawable.m5_preview,
            R.drawable.m6_preview,
            R.drawable.m7_preview,
            R.drawable.m8_preview,
            R.drawable.m9_preview,
            R.drawable.m10_preview,
            R.drawable.m11_preview,
            R.drawable.m12_preview,

            R.drawable.ny1_preview,
            R.drawable.ny2_preview,
            R.drawable.ny3_preview,
            R.drawable.ny4_preview,
            R.drawable.ny5_preview,
            R.drawable.ny6_preview,
            R.drawable.ny7_preview,
            R.drawable.ny8_preview,
            R.drawable.ny9_preview,
            R.drawable.ny10_preview,
            R.drawable.ny21_preview,
            R.drawable.ny12_preview,
            R.drawable.ny13_preview,
            R.drawable.ny14_preview,
            R.drawable.ny15_preview,
            R.drawable.ny16_preview,
            R.drawable.ny17_preview,
            R.drawable.ny18_preview,
            R.drawable.ny19_preview,
            R.drawable.ny20_preview,
            R.drawable.ny21_preview,
            R.drawable.ny22_preview,
            R.drawable.ny23_preview,
            R.drawable.ny24_preview,
            R.drawable.ny25_preview,

            R.drawable.p1_preview,
            R.drawable.p2_preview,
            R.drawable.p3_preview,
            R.drawable.p4_preview,
            R.drawable.p5_preview,
            R.drawable.p6_preview,
            R.drawable.p7_preview,
            R.drawable.p8_preview,
            R.drawable.p9_preview,
            R.drawable.p10_preview,
            R.drawable.p11_preview,
            R.drawable.p12_preview,
            R.drawable.p13_preview,
            R.drawable.p14_preview,
            R.drawable.p15_preview,
            R.drawable.p16_preview,
            R.drawable.p17_preview,
            R.drawable.p18_preview,
            R.drawable.p19_preview,
            R.drawable.p20_preview,
            R.drawable.p21_preview,
            R.drawable.p22_preview,
            R.drawable.p23_preview,
            R.drawable.p24_preview,
            R.drawable.p25_preview,
            R.drawable.p26_preview,

            R.drawable.love1_preview,
            R.drawable.love2_preview,
            R.drawable.love3_preview,
            R.drawable.love4_preview,
            R.drawable.love5_preview,
            R.drawable.love6_preview,
            R.drawable.love7_preview,
            R.drawable.love8_preview,
            R.drawable.love9_preview,
            R.drawable.love10_preview,
            R.drawable.love11_preview,
            R.drawable.love12_preview,

            R.drawable.lips1_preview,
            R.drawable.lips2_preview,
            R.drawable.lips3_preview,
            R.drawable.lips4_preview,
            R.drawable.lips5_preview,
            R.drawable.lips6_preview,
            R.drawable.lips7_preview,
            R.drawable.lips8_preview,
            R.drawable.lips9_preview,
            R.drawable.lips10_preview,
            R.drawable.lips11_preview,

            R.drawable.h1_preview,
            R.drawable.h2_preview,
            R.drawable.h3_preview,
            R.drawable.h4_preview,
            R.drawable.h5_preview,
            R.drawable.h6_preview,
            R.drawable.h7_preview,
            R.drawable.h8_preview,
            R.drawable.h9_preview,
            R.drawable.h10_preview,
            R.drawable.h11_preview,
            R.drawable.h12_preview,
            R.drawable.h13_preview,
            R.drawable.h14_preview,
            R.drawable.h15_preview,
            R.drawable.h16_preview

    };

    /*public static int[] getStickersPurchased() {
        return stickers_purchased;
    }*/

    /*public static boolean checkIfNotPurchased(int id) {
        boolean result = false;
        for (int i=0; i<mustache_purchase.length; i++) {
            if (mustache_purchase[i]==id) {
                result = true;
                break;
            }
        }
        return result;
    }*/
}
