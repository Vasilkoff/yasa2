package com.getyasa.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.getyasa.App;
import com.getyasa.R;
import com.getyasa.YasaConstants;
import com.vasilkoff.maxycamera.common.utils.Constants;

/**
 * Created by maxim.vasilkov@gmail.com on 22/12/15.
 */
public class SplashActivity extends /*AppCompatActivity*/Activity {

    final int time = 1600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        //actionBar.hide();

        setContentView(R.layout.splash_activity);
        initConstants();
        initVersionContainer();

        /*ImageView img = (ImageView) findViewById(R.id.yaicon1);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 720,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(time);
        img.startAnimation(rotateAnimation);*/

        askCameraPermission();

        //openYasaActivity();
    }
    private void initConstants() {
        Point outSize = new Point();
        getWindowManager().getDefaultDisplay().getRealSize(outSize);
        Constants.mScreenWidth = outSize.x;
        Constants.mScreenHeight = outSize.y;
        Constants.mScreenSoftButtonsHeight = Constants.mScreenHeight -getSoftButtonsBarHeight();
        //System.out.println("mScreenWidth" + outSize.x);
        //System.out.println("mScreenWidth" + outSize.y);
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            //Intent intent = new Intent(SplashActivity.this, CameraActivity.class);
            Intent intent = new Intent(SplashActivity.this, YasaMainActivity.class);
            intent.putExtra("shape_id", "");
            intent.putExtra("front",false);
            overridePendingTransition(R.anim.appear_bottom_right_in, R.anim.disappear_top_left_out);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return false;
        }
    });

    private void initVersionContainer() {
        TextView tv = (TextView)findViewById(R.id.versionTextView);
        String version = ( (App)getApplication() ).packageInfo().versionName + "." + ( (App)getApplication() ).packageInfo().versionCode;
        tv.setText(version);
    }

    //@SuppressLint("NewApi")
    private int getSoftButtonsBarHeight() {
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            return usableHeight;
            /*getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;*/
    }

    public void startAnimation() {
        ImageView img = (ImageView) findViewById(R.id.yaicon1);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 720,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(time);
        img.startAnimation(rotateAnimation);
    }
    public void openYasaActivity() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                handler.sendEmptyMessage(0);
            }
        }, time);
    }
    public void askCameraPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            /*System.out.println("here" + 1);*/
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    YasaConstants.REQUEST_PERMISSION_CAMERA);
        }
        else {
            /*System.out.println("here" + 2);*/
            startAnimation();
            openYasaActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == YasaConstants.REQUEST_PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
                startAnimation();
                openYasaActivity();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                finish();
            }
        }
    }
}
