package com.getyasa.activities;

/**
 * Created by Vasilkoff on 04.03.2016.
 */
public class ShapePosition {

    private static float positionX = 0;
    private static float positionY = 0;

    static public float getPositionX() {
        return positionX;
    }

    static public void setPositionX(float pos) {
        positionX = pos;
    }

    static public float getPositionY() {
        return positionY;
    }

    static public void setPositionY(float pos) {
        positionY = pos;
    }

}
