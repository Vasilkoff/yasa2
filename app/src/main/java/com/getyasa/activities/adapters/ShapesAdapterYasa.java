package com.getyasa.activities.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.getyasa.R;

/**
 * Created by Vasilkoff on 25.02.2016.
 */
public class ShapesAdapterYasa extends RecyclerView.Adapter<ShapesAdapterYasa.ViewHolder> {
    private int[] mDataset;
    private boolean items_purchased;
    private ShapesAdapterCallback myCallBack;
    private String type;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mImageView;
        public ImageView premiumView;
        public ViewHolder(View v) {

            super(v);
            mImageView = (ImageView)v.findViewById(R.id.itemYasaShape);
            premiumView = (ImageView)v.findViewById(R.id.premium_photoshape);
        }
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageView iv = (ImageView)v;
            int resId = (int)iv.getTag(R.string.obj_id);
            int position = (int)iv.getTag(R.string.obj_position);

            myCallBack.onShapesCallback(type, position);
        }
    };

    // Provide a suitable constructor (depends on the kind of dataset)
    public ShapesAdapterYasa(int[] resourcesDataset,  boolean purchased, Context listener) {
        mDataset = resourcesDataset;
        this.items_purchased = purchased;
        this.myCallBack = (ShapesAdapterCallback)listener;
        type = "";
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public ShapesAdapterYasa(int[] resourcesDataset, boolean purchased, Context listener,
                              String type) {
        mDataset = resourcesDataset;
        this.items_purchased = purchased;
        this.myCallBack = (ShapesAdapterCallback)listener;
        this.type= type;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ShapesAdapterYasa.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shape_item_view, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mImageView.setImageResource(mDataset[position]);
        holder.mImageView.setTag(R.string.obj_id, mDataset[position]);
        holder.mImageView.setTag(R.string.obj_position, position);
        holder.mImageView.setOnClickListener(clickListener);
        if (!items_purchased && position!=0) holder.premiumView.setVisibility(View.VISIBLE);
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static interface ShapesAdapterCallback {
        void onShapesCallback(String t, int position);
    }
}
