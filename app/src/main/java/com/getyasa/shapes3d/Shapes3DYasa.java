package com.getyasa.shapes3d;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.getyasa.R;
import com.getyasa.activities.YasaStickers;
import com.getyasa.activities.camera.utils.Constant;
import com.vasilkoff.magicfilter.camera.CameraEngine;
import com.vasilkoff.magicfilter.display.MagicDisplay;
import com.vasilkoff.magicfilter.filter.helper.MagicFilterType;
//import com.vasilkoff.maxycamera.common.adapter.FilterAdapter;
import com.vasilkoff.maxycamera.common.bean.FilterInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasilkoff on 10.05.2016.
 */
public class Shapes3DYasa {
    private Context mContext;
    private MagicDisplay mMagicDisplay;
    private FilterAdapter3DShapes mAdapter;

    private int position;
    private List<FilterInfo> filterInfos;

    private int mFilterType = MagicFilterType.NONE;

    /**
     * we have to handle programmatically onClick
     */
    RecyclerView mFilterListView;
    OnShape3DSelectedCallback callback;

    public Shapes3DYasa(Context context,MagicDisplay magicDisplay, OnShape3DSelectedCallback c) {
        mContext = context;
        mMagicDisplay = magicDisplay;
        callback = c;
    }

    public void init(){

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFilterListView = (RecyclerView)((Activity) mContext).findViewById(R.id.filterRecyclerView_3dshapes);
        mFilterListView.setLayoutManager(linearLayoutManager);

        mAdapter = new FilterAdapter3DShapes(mContext, 0);
        //mAdapter = new FilterAdapter(mContext);
        mFilterListView.setAdapter(mAdapter);
        initFilterInfos();
        mAdapter.setFilterInfos(filterInfos);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);
    }

    /**
     * MirrorEffect
     * */
    public static interface OnShape3DSelectedCallback {
        void onShapes3DCallback(String t, int position);
    }

    private FilterAdapter3DShapes.onFilterChangeListener onFilterChangeListener = new FilterAdapter3DShapes.onFilterChangeListener(){

        @Override
        public void onFilterChanged(int filterType, int position) {
            Shapes3DYasa.this.position = position;
            if (filterType==-3) {
                //if (Constant.is3DShapesApplying) mMagicDisplay.setMEffect(position+1);
                //else mMagicDisplay.setMEffect2(position + 1);

                mMagicDisplay.set3DEffect_(position + 1);
                //CameraEngine.current_mirror_type = position + 1;
                Constant.is3DShapesApplying = true;
            }
            else {
                //if (Constant.isFilterApllying) mMagicDisplay.setMEffect(0);
                //else mMagicDisplay.setFilter(filterType);

                mMagicDisplay.set3DEffect_(0);
                //CameraEngine.current_mirror_type = 0;
                Constant.is3DShapesApplying = false;
            }
            mFilterType = filterType;

            callback.onShapes3DCallback("3D_Shapes", (filterType == -3) ? position + 1 : position);
        }

    };

    public void setSelection(int sel) {
        //mAdapter = new FilterAdapter3DShapes(mContext, sel);
        //mAdapter = new FilterAdapter3DShapes(mContext, 0);
        //mFilterListView.swapAdapter(mAdapter, true);
        mAdapter.setLastSelected(0);
    }
    private void initFilterInfos(){
        filterInfos = new ArrayList<FilterInfo>();
        //add original
        FilterInfo filterInfo = new FilterInfo();
        filterInfo.setFilterType(MagicFilterType.NONE);
        filterInfo.setSelected(true);
        filterInfos.add(filterInfo);

        //add Divider
        filterInfo = new FilterInfo();
        filterInfo.setFilterType(-1);
        filterInfos.add(filterInfo);


        for(int i = 0;i < YasaStickers.get3DShape_effects().length; i++){
            filterInfo = new FilterInfo();
            filterInfo.setFilterType(-3);
            filterInfo.setShape_type(i);
            filterInfos.add(filterInfo);
        }
    }


    public List<FilterInfo> getFilters() {
        return filterInfos;
    }

}
