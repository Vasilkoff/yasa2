package com.getyasa.shapes3d;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.getyasa.R;
import com.getyasa.activities.YasaStickers;
import com.vasilkoff.maxycamera.common.bean.FilterInfo;
import com.vasilkoff.maxycamera.common.utils.FilterTypeHelper;

import java.util.List;

/**
 * Created by Vasilkoff on 11.05.2016.
 */
public class FilterAdapter3DShapes extends RecyclerView.Adapter<FilterAdapter3DShapes.FilterHolder>{
    private LayoutInflater mInflater;
    private int lastSelected = 2;
    private Context context;
    private List<FilterInfo> filterInfos;

    public FilterAdapter3DShapes(Context context){
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public FilterAdapter3DShapes(Context context, int select){
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.lastSelected = select;
    }

    @Override
    public int getItemCount() {
        return filterInfos.size();
    }

    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return filterInfos.get(position).getFilterType();
    }

    @Override
    public void onBindViewHolder(FilterHolder arg0, final int arg1) {
        if(filterInfos.get(arg1).getFilterType() == -3){
            arg0.thumbImage.setImageResource(
                    YasaStickers.get3DShape_effects()[ filterInfos.get(arg1).getShape_type() ]
            );
            arg0.filterName.setText(
                    context.getString(R.string.dialog_message_cancel6) + (filterInfos.get(arg1).getShape_type() + 1)
            );
            arg0.filterName.setBackgroundColor(
                    context.getResources().getColor(FilterTypeHelper.FilterType2Color(35)));//blue cool

            if(filterInfos.get(arg1).isSelected()){
                arg0.thumbSelected.setVisibility(View.VISIBLE);
                arg0.thumbSelected_bg.setBackgroundColor(context.getResources().getColor(
                        FilterTypeHelper.FilterType2Color(filterInfos.get(arg1).getFilterType())));
                arg0.thumbSelected_bg.setAlpha(0.7f);
            }
            else
                arg0.thumbSelected.setVisibility(View.GONE);

            arg0.filterFavourite.setVisibility(View.GONE);
            arg0.filterRoot.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (onFilterChangeListener != null
                            && filterInfos.get(arg1).getFilterType() != -1
                            && arg1 != lastSelected
                            && !filterInfos.get(arg1).isSelected()) {

                        filterInfos.get(lastSelected).setSelected(false);
                        filterInfos.get(arg1).setSelected(true);
                        notifyItemChanged(lastSelected);
                        notifyItemChanged(arg1);
                        lastSelected = arg1;

                        onFilterChangeListener.onFilterChanged(
                                filterInfos.get(arg1).getFilterType(),
                                filterInfos.get(arg1).getShape_type()
                        );
                    }
                }
            });
        }

        else if(filterInfos.get(arg1).getFilterType() != -1){
            arg0.thumbImage.setImageResource(FilterTypeHelper.FilterType2Thumb(filterInfos.get(arg1).getFilterType()));
            arg0.filterName.setText(FilterTypeHelper.FilterType2Name(filterInfos.get(arg1).getFilterType()));
            arg0.filterName.setBackgroundColor(context.getResources().getColor(
                    FilterTypeHelper.FilterType2Color(filterInfos.get(arg1).getFilterType())));
            if(filterInfos.get(arg1).isSelected()){
                arg0.thumbSelected.setVisibility(View.VISIBLE);
                arg0.thumbSelected_bg.setBackgroundColor(context.getResources().getColor(
                        FilterTypeHelper.FilterType2Color(filterInfos.get(arg1).getFilterType())));
                arg0.thumbSelected_bg.setAlpha(0.7f);
            }
            else
                arg0.thumbSelected.setVisibility(View.GONE);

            if(!filterInfos.get(arg1).isFavourite() || arg1 == 0)
                arg0.filterFavourite.setVisibility(View.GONE);
            else
                arg0.filterFavourite.setVisibility(View.VISIBLE);

            arg0.filterRoot.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if(onFilterChangeListener!= null && filterInfos.get(arg1).getFilterType() != -1
                            && arg1!= lastSelected
                            && !filterInfos.get(arg1).isSelected()){
                        filterInfos.get(lastSelected).setSelected(false);
                        filterInfos.get(arg1).setSelected(true);
                        notifyItemChanged(lastSelected);
                        notifyItemChanged(arg1);
                        lastSelected = arg1;

                        onFilterChangeListener.onFilterChanged(filterInfos.get(arg1).getFilterType(), arg1);
                    }
                }
            });
        }

    }

    @Override
    public FilterHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        if(arg1 != -1){
            View view = mInflater.inflate(R.layout.filter_item_layout,
                    arg0, false);
            FilterHolder viewHolder = new FilterHolder(view);
            viewHolder.thumbImage = (ImageView) view
                    .findViewById(R.id.filter_thumb_image);
            viewHolder.filterName = (TextView) view
                    .findViewById(R.id.filter_thumb_name);
            viewHolder.filterRoot = (FrameLayout)view
                    .findViewById(R.id.filter_root);
            viewHolder.thumbSelected = (FrameLayout) view
                    .findViewById(R.id.filter_thumb_selected);
            viewHolder.filterFavourite = (FrameLayout)view.
                    findViewById(R.id.filter_thumb_favorite_layout);
            viewHolder.thumbSelected_bg = (View)view.
                    findViewById(R.id.filter_thumb_selected_bg);
            return viewHolder;
        }else{
            View view = mInflater.inflate(R.layout.filter_division_layout,
                    arg0, false);
            FilterHolder viewHolder = new FilterHolder(view);
            return viewHolder;
        }
    }

    public void setLastSelected(int arg){
        if ( (filterInfos.get(arg).getFilterType() != -1) && (arg != lastSelected) && (!filterInfos.get(arg).isSelected()) ) {

            filterInfos.get(lastSelected).setSelected(false);
            filterInfos.get(arg).setSelected(true);
            notifyItemChanged(lastSelected);
            notifyItemChanged(arg);
            lastSelected = arg;

            //onFilterChangeListener.onFilterChanged( filterInfos.get(arg).getFilterType(), filterInfos.get(arg).getShape_type() );
        }
        //notifyItemChanged(lastSelected);
        //notifyItemChanged(arg);
        //lastSelected = arg;
    }

    public int getLastSelected(){
        return lastSelected;
    }

    public void setFilterInfos(List<FilterInfo> filterInfos){
        this.filterInfos = filterInfos;
        notifyDataSetChanged();
    }

    class FilterHolder extends ViewHolder {
        ImageView thumbImage;
        TextView filterName;
        FrameLayout thumbSelected;
        FrameLayout filterRoot;
        FrameLayout filterFavourite;
        View thumbSelected_bg;

        public FilterHolder(View itemView) {
            super(itemView);
        }
    }

    public interface onFilterChangeListener{
        void onFilterChanged(int filterType, int position);
    }

    private onFilterChangeListener onFilterChangeListener;

    public void setOnFilterChangeListener(onFilterChangeListener onFilterChangeListener){
        this.onFilterChangeListener = onFilterChangeListener;
    }
}
