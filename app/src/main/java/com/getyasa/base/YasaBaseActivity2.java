package com.getyasa.base;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.appintop.adbanner.BannerAdContainer;
import com.appintop.init.AdToApp;
import com.getyasa.R;

/**
 * Created by Vasilkoff on 21.03.2016.
 */

public class YasaBaseActivity2 extends Activity implements ActivityResponsable {
    protected Bundle savedInstanceState;


    protected ActivityHelper  mActivityHelper;
    private BannerAdContainer banner;

    protected int transition_in = R.anim.slide_in;
    protected int transition_out = R.anim.slide_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(transition_in, transition_out);
        mActivityHelper = new ActivityHelper(this);
        this.savedInstanceState = savedInstanceState;
    }

    protected void initAd(int resId) {
        AdToApp.initializeSDK(this,
                "5c781406-6584-4d6e-a499-723500477f75:6d22c38e-ded0-4fc9-b6a8-09548b9c7db4",
                AdToApp.MASK_INTERSTITIAL
                //AdToApp.MASK_VIDEO
        );
        /*banner = (BannerAdContainer)findViewById(resId);
        if(banner!=null) {
            banner.setRefreshInterval(60);
            AdToApp.setTestMode(true);
        }*/
        //AdToApp.setTestMode(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(banner!=null) {
            AdToApp.onResume(this);
            banner.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(banner!=null) {
            banner.pause();
            AdToApp.onPause(this);
        }
    }

    @Override
    protected void onDestroy() {
        if(banner!=null) {
            banner.destroy();
            AdToApp.onDestroy(this);
        }
        super.onDestroy();
    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    /**
     * @param title
     * @param msg
     * @param positive
     * @param positiveListener
     * @param negative
     * @param negativeListener
     */
    @Override
    public void alert(String title, String msg, String positive,
                      DialogInterface.OnClickListener positiveListener, String negative,
                      DialogInterface.OnClickListener negativeListener) {
        mActivityHelper.alert(title, msg, positive, positiveListener, negative, negativeListener);
    }

    /**
     * @param title
     * @param msg
     * @param positive
     * @param positiveListener
     * @param negative
     * @param negativeListener
     * @param isCanceledOnTouchOutside
     */
    @Override
    public void alert(String title, String msg, String positive,
                      DialogInterface.OnClickListener positiveListener, String negative,
                      DialogInterface.OnClickListener negativeListener,
                      Boolean isCanceledOnTouchOutside) {
        mActivityHelper.alert(title, msg, positive, positiveListener, negative, negativeListener,
                isCanceledOnTouchOutside);
    }

    /**
     * TOAST
     *
     * @param msg
     * @param period
     */
    @Override
    public void toast(String msg, int period) {
        mActivityHelper.toast(msg, period);
    }

    /**
     * @param msg
     */
    @Override
    public void showProgressDialog(String msg) {
        System.out.println("DIALOG_STAARTED");
        mActivityHelper.showProgressDialog(msg);
    }

    /**
     * @param msg
     */
    public void showProgressDialog(final String msg, final boolean cancelable,
                                   final DialogInterface.OnCancelListener cancelListener) {
        mActivityHelper.showProgressDialog(msg, cancelable, cancelListener);
    }

    @Override
    public void dismissProgressDialog() {
        System.out.println("DIALOG_DISMISSED");
        mActivityHelper.dismissProgressDialog();
    }





    public void onForward(View v) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_back, R.anim.slide_in_back);
    }
}
