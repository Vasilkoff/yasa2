package com.getyasa.base;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

public interface ActivityResponsable {
    /**
     * Flexible Dialog
     * 
     * @param title
     *            title
     * @param msg
     *            message
     * @param positive
     *            positive title
     * @param positiveListener
     *            positive callback function
     * @param negative
     *            negative title
     * @param negativeListener
     *            negative callback function
     */
    public void alert(final String title, final String msg, final String positive,
                      final DialogInterface.OnClickListener positiveListener,
                      final String negative, final DialogInterface.OnClickListener negativeListener);

    /**
     * Flexible Dialog with touch outside event
     * 
     * @param title
     *            title
     * @param msg
     *            message
     * @param positive
     *            positive title
     * @param positiveListener
     *            positive callback function
     * @param negative
     *            negative title
     * @param negativeListener
     *            negative callback function
     * @param isCanceledOnTouchOutside
     *            if the outside touch cancel event
     */
    public void alert(final String title, final String msg, final String positive,
                      final DialogInterface.OnClickListener positiveListener,
                      final String negative,
                      final DialogInterface.OnClickListener negativeListener,
                      Boolean isCanceledOnTouchOutside);

    /**
     * TOAST
     * 
     * @param msg
     *            message
     * @param period
     *            time period
     */
    public void toast(final String msg, final int period);

    /**
     * Show progress dialog
     * 
     * @param msg
     *            message
     */
    public void showProgressDialog(final String msg);

    /**
     * The progress dialog is displayed to cancel
     * 
     * @param msg
     *            message
     */
    public void showProgressDialog(final String msg, final boolean cancelable,
                                   final OnCancelListener cancelListener);

    /**
     * Cancel progress dialog
     * 
     */
    public void dismissProgressDialog();

}
