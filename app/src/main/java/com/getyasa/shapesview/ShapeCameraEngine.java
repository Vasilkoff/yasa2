package com.getyasa.shapesview;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.view.SurfaceHolder;

import com.vasilkoff.magicfilter.camera.CameraEngine;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Vasilkoff on 26.02.2016.
 */
public class ShapeCameraEngine {
    private static Camera mCamera = null;
    private static int mCameraID = 0;

    public static Camera getCamera(){
        return mCamera;
    }

    public static boolean openCamera(int w, int h){
        if(mCamera == null){
            try{
                mCamera = Camera.open(mCameraID);
                setDefaultParameters2(w, h);
                return true;
            }catch(RuntimeException e){
                return false;
            }
        }
        return false;
    }

    public static boolean openCamera(int cameraId, int w, int h){
        mCameraID = cameraId;
        if(mCamera == null){
            try{
                mCamera = Camera.open(mCameraID);
                setDefaultParameters2(w,h);
                return true;
            }catch(RuntimeException e){
                return false;
            }
        }
        return false;
    }

    public static void releaseCamera(){
        if(mCamera != null){
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public void resumeCamera(){
        int w=240;
        int h=240;
        openCamera(w, h);
    }

    public void setParameters(Camera.Parameters parameters){
        mCamera.setParameters(parameters);
    }

    public Camera.Parameters getParameters(){
        if(mCamera != null)
            mCamera.getParameters();
        return null;
    }

    private static void setDefaultParameters(){
        Camera.Parameters parameters = mCamera.getParameters();
        if (parameters.getSupportedFocusModes().contains(
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        Camera.Size previewSize = getLargePreviewSize();

        parameters.setPreviewSize(previewSize.width, previewSize.height);
        Camera.Size pictureSize = getLargePictureSize(previewSize);
        parameters.setPictureSize(pictureSize.width, pictureSize.height);
        mCamera.setParameters(parameters);
    }

    private static void setDefaultParameters2(int w, int h){
        Camera.Parameters parameters = mCamera.getParameters();
        if (parameters.getSupportedFocusModes().contains(
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        parameters.setPreviewSize(w, h);
        parameters.setPictureSize(w, h);
        mCamera.setParameters(parameters);
    }

    private static Camera.Size getLargePreviewSize(){
        if(mCamera != null){
            List<Camera.Size> sizes = mCamera.getParameters().getSupportedPreviewSizes();
            Camera.Size temp = sizes.get(0);
            for(int i = 1;i < sizes.size();i ++){
                if(temp.width < sizes.get(i).width)
                    temp = sizes.get(i);
            }
            return temp;
        }
        return null;
    }

    private static Camera.Size getLargePictureSize(Camera.Size previewSize){
        if(mCamera != null) {
            float ratio = previewSize.width/(float)previewSize.height;
            List<Camera.Size> sizes = mCamera.getParameters().getSupportedPictureSizes();
            Collections.sort(sizes, new Comparator<Camera.Size>() {
                @Override
                public int compare(Camera.Size lhs, Camera.Size rhs) {
                    return -lhs.width * lhs.height + rhs.width * rhs.height;
                }
            });
            int index = 0;
            float difference = Float.MAX_VALUE;
            for(int i = 0;i < sizes.size();i ++) {
                Camera.Size s1 = sizes.get(i);
                float scale = s1.width/(float)s1.height;
                if(Math.abs(scale-ratio)<difference) {
                    difference = Math.abs(scale-ratio);
                    index = i;
                }

            }
            return sizes.get(index);
        }
        return null;
    }

    public static Camera.Size getPreviewSize(){
        return mCamera.getParameters().getPreviewSize();
    }

    public static void startPreview(SurfaceTexture surfaceTexture){
        try {
            mCamera.setPreviewTexture(surfaceTexture);
            mCamera.startPreview();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void beginCamera(SurfaceHolder mholder){
        if(mCamera != null) {
            try {
                mCamera.setPreviewDisplay(mholder);
                mCamera.startPreview();
            } catch (IOException e) {
                System.out.println("WE_ARE_HERE_2");
                e.printStackTrace();
            }
        }
    }

    public static void stopPreview(){
        mCamera.stopPreview();
    }

    public static Camera.CameraInfo getCameraInfo(){
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraID, cameraInfo);
        return cameraInfo;
    }

    public static int getOrientation(){
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraID, cameraInfo);
        return cameraInfo.orientation;
    }

    public static boolean isFlipHorizontal(){
        return CameraEngine.getCameraInfo().facing == Camera.CameraInfo.CAMERA_FACING_FRONT ? true : false;
    }

    public static void setCameraOrientstion(int degree){
        mCamera.setDisplayOrientation(degree);
    }
    public static void setRotation(int rotation){
        Camera.Parameters params = mCamera.getParameters();
        params.setRotation(rotation);
        mCamera.setParameters(params);
    }

    public static void takePicture(Camera.ShutterCallback shutterCallback, Camera.PictureCallback rawCallback,
                                   Camera.PictureCallback jpegCallback){
        mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
    }

    public static int getFacing(){
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraID, cameraInfo);
        return cameraInfo.facing;
    }

    public static final int FRONT_CAMERA_ROTATION = 270;
    public static final int BACK_CAMERA_ROTATION = 90;
    public static boolean BACK_CAMERA_IN_USE = true;

    //PHOTO
    public static Bitmap bitmap;
    //CAPTURE
    public static Bitmap captured;
}
