package com.getyasa.shapesview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.vasilkoff.magicfilter.BitmapHelper;
import com.vasilkoff.magicfilter.camera.CameraEngine;
import com.vasilkoff.magicfilter.utils.SaveTask;

import java.io.File;


/**
 * Created by Vasilkoff on 26.02.2016.
 */
public class ShapeCameraDisplay /*implements SurfaceHolder.Callback*/ {

    private int ShapeWidth;
    private int ShapeType;
    private Activity Appcon;

    public CircleSurface getShapeSurface() {
        return ShapeSurface;
    }

    protected final CircleSurface ShapeSurface;

    public SurfaceHolder getShapeHolder() {
        return ShapeHolder;
    }

    private SurfaceHolder ShapeHolder;
    SaveTask mSaveTask3;

    public ShapeCameraDisplay(int width, int type, Context con, CircleSurface surface) {
        ShapeWidth = width;
        ShapeType = type;
        Appcon = (Activity) con;
        ShapeSurface = surface;
        init(width, type);
        /*ShapeHolder = ShapeSurface.getHolder();
        ShapeHolder.addCallback(this);*/
    }

    /*@Override
    public void surfaceCreated(SurfaceHolder holder) {
        onChangeCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }*/

    public void onOpenCameraById(int id) {
        ShapeCameraEngine.openCamera(id, getShapeWidth(), getShapeWidth());
    }

    public void onOpenCamera() {
        ShapeCameraEngine.openCamera(getShapeWidth(), getShapeWidth());
    }

    public void setUpShapeCamera() {
        ShapeCameraEngine.beginCamera(ShapeHolder);

    }

    public void onResume() {
        //onChangeCamera();
    }

    public void onChangeCamera() {
        /*if (ShapeCameraEngine.getCamera() == null)
            ShapeCameraEngine.openCamera(getShapeWidth(), getShapeWidth());
        if (ShapeCameraEngine.getCamera() != null) {
            boolean flipHorizontal = ShapeCameraEngine.isFlipHorizontal();
            int betterOrientation = getCameraDisplayOrientation(Appcon);
            ShapeCameraEngine.setCameraOrientstion(betterOrientation);
            //adjustPosition(betterOrientation, flipHorizontal, !flipHorizontal);
        }
        setUpShapeCamera();*/
    }

    public void onPause() {
        //ShapeCameraEngine.releaseCamera();
    }

    public void onDestroy() {

    }

    public void init(int width, int type) {
        if (type == 0) ShapeSurface.initSquare(width, width);
        else if (type == 3) ShapeSurface.initCircle(width, width);
        else if (type == 5) ShapeSurface.initStar(width, width);
        else if (type == 1) ShapeSurface.initHexagon(width, width);
        else if (type == 4) ShapeSurface.initOctagon(width, width);
        else if (type == 2) ShapeSurface.initHeart(width, width);
    }

    public Camera onGetCamera() { return ShapeCameraEngine.getCamera();}
    public void onReleaseCamera() { ShapeCameraEngine.releaseCamera(); }
    public void onStopPreview() { ShapeCameraEngine.stopPreview(); }

    public void onTakePicture(File file, SaveTask.onPictureSaveListener listener, Camera.ShutterCallback shutterCallback, boolean save){
        mSaveTask3 = new SaveTask(Appcon, file, null, save);
        ShapeCameraEngine.takePicture(shutterCallback, null, mPictureCallback);
    }

    public void onTakePicture(File file, SaveTask.onPictureSaveListener listener, Camera.ShutterCallback shutterCallback){
        mSaveTask3 = new SaveTask(Appcon, file, null);
        ShapeCameraEngine.takePicture(shutterCallback, null, mPictureCallback);
    }

    private Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(final byte[] data,Camera camera) {
            /*Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            if(ShapeCameraEngine.BACK_CAMERA_IN_USE) {
                Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(bitmap, ShapeCameraEngine.BACK_CAMERA_ROTATION);
                bitmap = null;
                bitmap = rotatedBitmap;
            }
            else {
                Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(bitmap, ShapeCameraEngine.FRONT_CAMERA_ROTATION);
                rotatedBitmap = BitmapHelper.flip(rotatedBitmap);
                bitmap = null;
                bitmap = rotatedBitmap;
            }
            mSaveTask3.execute(bitmap);*/
        }
    };

    public int getCameraDisplayOrientation(final Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        int result;
        int orientation = ShapeCameraEngine.getOrientation();
        int facing = ShapeCameraEngine.getFacing();
        if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (orientation + degrees) % 360;
        } else { // back-facing
            result = (orientation - degrees + 360) % 360;
        }
        return result;
    }
    public int getShapeWidth() {
        return ShapeWidth;
    }

    public void setShapeWidth(int shapeWidth) {
        ShapeWidth = shapeWidth;
    }

    public int getShapeType() {
        return ShapeType;
    }

    public void setShapeType(int shapeType) {
        ShapeType = shapeType;
    }
}
