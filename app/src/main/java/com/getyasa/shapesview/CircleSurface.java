package com.getyasa.shapesview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Vasilkoff on 25.02.2016.
 */
public class CircleSurface extends ImageView/*View*//*SurfaceView*/  {

    private Path clipPath;
    private Paint mpaint;
    private Path mBorder;

    public CircleSurface(Context context) {
        super(context);
        this.setDrawingCacheEnabled(true);
    }

    public CircleSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setDrawingCacheEnabled(true);
    }

    public CircleSurface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setDrawingCacheEnabled(true);
    }

    public void initCircle(int w, int h) {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
        initPaint();
        float radius = w/2;
        float centerX = w/2;
        float centerY = w/2;
        clipPath.addCircle(centerX, centerY, radius, Path.Direction.CW);
    }
    public void initPaint(){
        if (mpaint!=null) mpaint.reset();
        mpaint = new Paint();
        mpaint.setAntiAlias(true);
        //mpaint.setStrokeWidth(1);
        mpaint.setColor(Color.WHITE);
        //mpaint.setStyle(Paint.Style.STROKE);
        mpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    public void initPath() {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
    }

    public void initStar(int w, int h) {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
        initPaint();
        float mid = w/2;
        float half = w;
        clipPath.moveTo(half * 0.5f - mid, half * 0.84f - mid);
        // top right
        clipPath.lineTo(half * 1.5f - mid, half * 0.84f - mid);
        //bottom left
        clipPath.lineTo(half * 0.68f - mid, half * 1.45f - mid);
        //top tip
        clipPath.lineTo(half * 1.0f - mid, half * 0.5f - mid);
        //bottom right
        clipPath.lineTo(half * 1.32f - mid, half * 1.45f - mid);
        //top left
        clipPath.lineTo(half * 0.5f - mid, half * 0.84f - mid);
    }
    public void initSquare(int w, int h) {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
        //clipPath.setFillType(Path.FillType.WINDING);
        initPaint();

        float lrg = w;
        clipPath.moveTo(lrg / 2, 0);
        clipPath.lineTo(lrg,lrg/2);
        clipPath.lineTo(lrg/2,lrg);
        clipPath.lineTo(0,lrg/2);
    }

    public void initHexagon(int w, int h) {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
        initPaint();
        float radius = w/2;
        float centerX = w/2;
        float centerY = w/2;
        float triangleHeight = (float) (Math.sqrt(3) * radius / 2);
        clipPath.moveTo(centerX, centerY + radius);
        clipPath.lineTo(centerX - triangleHeight, centerY + radius / 2);
        clipPath.lineTo(centerX - triangleHeight, centerY - radius / 2);
        clipPath.lineTo(centerX, centerY - radius);
        clipPath.lineTo(centerX + triangleHeight, centerY - radius/2);
        clipPath.lineTo(centerX + triangleHeight, centerY + radius / 2);
    }

    public void initOctagon(int w, int h) {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
        initPaint();
        float radius = w/2;
        float centerX = w/2;
        float centerY = w/2;
        float triangleHeight = (float) (Math.sqrt(3) * radius / 2);
        clipPath.moveTo(centerX + triangleHeight * (float) Math.cos(0), centerY + triangleHeight * (float) Math.sin(0));
        for (int i = 1; i <= 8; i++)
        {
            clipPath.lineTo (centerX + triangleHeight * (float)Math.cos(i * 2 * Math.PI / 8), centerY + triangleHeight * (float)Math.sin(i * 2 * Math.PI / 8));
        }

    }

    Matrix matrix = new Matrix();
    public void initHeart(int w, int h) {
        if (clipPath!=null) clipPath.reset();
        clipPath = new Path();
        initPaint();
        //float lrg = w;
        /*float lrg = (float)w*1.066f;
        float height = h;
        clipPath.moveTo(lrg / 2, lrg / 4);
        clipPath.cubicTo(
                0, 0,
                0, 4 * height / 5,
                lrg / 2, 11*lrg/12);
        clipPath.cubicTo(
                lrg, 4 * height / 5,
                lrg, 0,
                lrg / 2, lrg / 4);*/
        float lrg = 7*w/12;
        float x = w;
        float y = w;
        matrix.reset();
        matrix.postRotate(45, x / 2, y / 2);
        matrix.postTranslate(0, -lrg/2+lrg/10);
        clipPath.moveTo(x, y);
        clipPath.lineTo(x - lrg, y);
        clipPath.arcTo(new RectF(x - lrg - (lrg / 2), y - lrg, x - (lrg / 2), y), 90, 180);
        clipPath.arcTo(new RectF(x - lrg, y - lrg - (lrg / 2), x, y - (lrg / 2)), 180, 180);
        clipPath.lineTo(x, y);
        clipPath.transform(matrix, clipPath);
    }

    /*@Override
    protected void dispatchDraw(Canvas canvas) {
        if (clipPath!=null) {
            canvas.drawPath(clipPath, mpaint);
            canvas.clipPath(clipPath);}
        super.dispatchDraw(canvas);
    }*/

    public Bitmap get(){
        return this.getDrawingCache();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (clipPath!=null) {
            canvas.drawPath(clipPath, mpaint);
            canvas.clipPath(clipPath);}
        super.onDraw(canvas);
    }
}