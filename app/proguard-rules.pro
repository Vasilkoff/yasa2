# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Program Files\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keepattributes Exceptions, InnerClasses, Signature, Deprecated, SourceFile, LineNumberTable, *Annotation*, EnclosingMethod
-dontwarn android.webkit.**
#adtoapp
-keep class com.appintop.** {*;}
-dontwarn com.appintop.**
#adcolony
-keep class com.immersion.** {*;}
-dontwarn com.immersion.**
-dontnote com.immersion.**
-keep class com.jirbo.** {*;}
-dontwarn com.jirbo.**
-dontnote com.jirbo.**
#applovin
-keep class com.applovin.** {*;}
-dontwarn com.applovin.**
#startapp
-keep class com.startapp.** {*;}
-dontwarn com.startapp.**
#inmobi
-dontwarn com.inmobi.**
-keep class com.inmobi.** {*;}
#mytarget
-keep class ru.mail.android.mytarget.** {*;}
-dontwarn ru.mail.android.mytarget.**
#mopub
-keep class com.mopub.** {*;}
-dontwarn com.mopub.**
#smaato
-keep class com.smaato.soma.** {*;}
-dontwarn com.smaato.soma.**
-dontwarn org.fmod.**
-keep class org.fmod.** { *; }
-keepclassmembers  class com.tms.rarus.videoserver.* { *; }
-keepclassmembers  class com.unity3d.player.** { *; }
-keepclassmembers  class org.fmod.** { *; }
#chartboost
-keep class com.chartboost.** {*;}
-dontwarn com.chartboost.**
#unity3d
-keep class com.applifier.** {*;}
-keep class com.unity3d.** {*;}
-dontwarn com.unity3d.**
#personagraph
-keep class com.personagraph.** {*;}
-dontwarn com.personagraph.**
#yandex
-keep class com.yandex.metrica.** { *; }
-dontwarn com.yandex.metrica.**
-keep class com.yandex.mobile.ads.** { *; }
-dontwarn com.yandex.mobile.ads.**
#tapsense
-dontwarn **CompatHoneycomb
-dontwarn com.tapsense.android.publisher.**
-keep class android.support.v4.**
-keep class com.tapsense.android.publisher.TS**
-keep class com.tapsense.mobileads.**
#amazon
-dontwarn com.amazon.**
-keep class com.amazon.** {*;}
#vungle
-keep class com.vungle.** { public *; }
-keep class javax.inject.*
-keepattributes *Annotation*
-keepattributes Signature
-keep class dagger.*
#supersonic
-keepclassmembers class com.supersonicads.sdk.controller.SupersonicWebView$JSInterface {
    public *;
}
-keepclassmembers class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}
-keep class com.supersonic.mediationsdk.** { *;}
-keep class com.supersonic.adapters.** { *;}
#woobi
-keep class com.woobi.** { *; }

-dontwarn org.apache.**
-dontwarn com.google.**
-dontwarn android.**